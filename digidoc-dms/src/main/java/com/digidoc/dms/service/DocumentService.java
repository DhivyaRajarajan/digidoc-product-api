package com.digidoc.dms.service;

import java.util.List;
import java.util.Set;

import org.springframework.web.multipart.MultipartFile;

import com.digidoc.admin.dto.DocumentDto;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.dataaccess.es.model.EsDocument;
import com.digidoc.dataaccess.model.Document;

public interface DocumentService {
	
	String saveDocument(MultipartFile file,DocumentDto documentDto);

	String updateDocument(MultipartFile scanFile, DocumentDto documentDto);
	
	List<DocumentDto> getDocumentsByFolderId(Long folderId);

	String isValidInput(DocumentDto documentDto);

	DocumentService findById(Long id);

	List<DocumentDto> getDocumentList();

	List<DocumentDto> getVersionsByDoId(Long docId);
	
	String findByDocId(DocumentDto documentDto);

	List<Document> searchContentInElasticSearch(String searchContent);

	Set<Long> findDocumentIdsIfContentAvail(List<DocumentMetaDataTagInfoDto> documentsListUsingMetaDataTagValue,
			List<DocumentLabelsDto> documentsListUsingLabels, List<Document> allDocumentObjectWithIdWhereSearchTextAvail);

	List<DocumentDto> getDocumentNameUsingDocumentIds(Set<Long> documentIdsSet);
	
	StatusResponseDTO isDownloaded(Long docId, Long userId);
	
	boolean isReverted(Long docId);
	
	List<DocumentMetaDataTagInfoDto> getDocMataAndValues(Long documentId);

	//Boolean saveNetworkInfo(NetworkDto networkDto);

}
