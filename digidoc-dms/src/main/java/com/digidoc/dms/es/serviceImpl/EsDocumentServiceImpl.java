package com.digidoc.dms.es.serviceImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.dataaccess.es.model.EsDocument;
import com.digidoc.dataaccess.es.repository.EsDocumentRepository;
import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.repository.DocumentRepository;
import com.digidoc.dms.es.service.EsDocumentService;

@Service
public class EsDocumentServiceImpl implements EsDocumentService {

	@Autowired
	EsDocumentRepository esDocumentRepository;

	@Autowired
	DocumentRepository documentRepository;

	@Override
	public EsDocument save(EsDocument esdocument) {
		// TODO Auto-generated method stub
		esDocumentRepository.save(esdocument);
		return null;
	}

	@Override
	public EsDocument getDocumentContent(Long documentId) {
		// TODO Auto-generated method stub
		EsDocument esDocument = esDocumentRepository.findByDocumentMappingId(documentId);
		return esDocument;
	}

	@Override
	public List<Document> findAllContentFromElasticSearch(String content) {
		// TODO Auto-generated method stub
		List<Document> allDocumentObjectWithIdWhereSearchTextAvail = new ArrayList<Document>();
		Iterable<EsDocument> allContentOfElasticSearchh = esDocumentRepository.findAll();
		Iterator<EsDocument> esDocIterator = allContentOfElasticSearchh.iterator();
		while (esDocIterator.hasNext()) {
			EsDocument esDocument = esDocIterator.next();
			if (esDocument.getSearchText() != null && esDocument.getSearchText().contains(content)) {
				Document document = documentRepository.findById(esDocument.getDocumentMappingId());
				allDocumentObjectWithIdWhereSearchTextAvail.add(document);
			}
		}
		return allDocumentObjectWithIdWhereSearchTextAvail;
	}

}
