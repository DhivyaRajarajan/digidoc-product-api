package com.digidoc.dms.es.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digidoc.dataaccess.es.model.EsDocument;
import com.digidoc.dataaccess.model.Document;
@Service
public interface EsDocumentService {

	EsDocument save(EsDocument esDocument);
	EsDocument getDocumentContent(Long documentId);
	List<Document> findAllContentFromElasticSearch(String content);
}
