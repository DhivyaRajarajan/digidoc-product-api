package com.digidoc.dms.controller;

import java.io.IOException;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.jni.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.digidoc.admin.dto.DocumentDto;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.service.UserFavoriteSearchService;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.admin.utill.ValidationForDocDto;
import com.digidoc.dataaccess.es.model.EsDocument;
import com.digidoc.dataaccess.model.Document;
import com.digidoc.dms.service.DocumentService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@RestController
public class DocumentController {

	@Autowired
	ValidationForDocDto validationForDocDto;
	@Autowired
	DocumentService documentService;

	@Autowired
	ModelToDtoConversion modelToDtoConversion;
	
	@Autowired
	UserFavoriteSearchService userFavoriteSearchService;

	@Autowired
	Environment env;

	@PostMapping("/document/add")
	public ResponseEntity<String> addDocument(
			@RequestParam(name = "documentJson", value = "documentJson") String documentJson,
			@RequestParam(name = "scanFile", value = "scanFile", required = true) MultipartFile scanFile) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		String checkexist = null;

		if (scanFile != null) {
			ObjectMapper mapper = new ObjectMapper();
			StringReader reader = new StringReader(documentJson); // Json
																	// Response

			// Convert the JSON response to appropriate DTO ...
			DocumentDto documentDto = null;
			try {
				documentDto = mapper.readValue(reader, DocumentDto.class);
				checkexist = documentService.isValidInput(documentDto);
				if (checkexist.equalsIgnoreCase("success")) {
					documentService.saveDocument(scanFile, documentDto);
					if (documentService != null) {
						statusResponseDTO.setStatus(env.getProperty("success"));
						statusResponseDTO.setMessage(env.getProperty("document.add"));
						return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
					}
				} else {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(checkexist);
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		statusResponseDTO.setStatus(env.getProperty("failure"));
		statusResponseDTO.setMessage(env.getProperty("document.failed"));
		return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
	}

	@PostMapping("/document/update")
	public ResponseEntity<String> updateDocument(
			@RequestParam(name = "documentJson", value = "documentJson") String documentJson,
			@RequestParam(name = "scanFile", value = "scanFile", required = false) MultipartFile scanFile) {
		
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		ObjectMapper mapper = new ObjectMapper();
		StringReader reader = new StringReader(documentJson); // Json Response
		
		DocumentDto documentDto = null;
		try {
			documentDto = mapper.readValue(reader, DocumentDto.class);

			if (scanFile.isEmpty()) {
				documentService.findById(documentDto.getId());
				{

				}
			}

			documentService.saveDocument(scanFile, documentDto);
			if (documentService != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("document.add"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("document.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		documentService.updateDocument(scanFile, documentDto);
		return null;
	}

	@CrossOrigin
	@RequestMapping(value = "/getDocuments/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getUser(@PathVariable("id") Long id, HttpServletRequest request, HttpServletResponse response) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		
		try {
			documentDtoList = documentService.getDocumentsByFolderId(id);
			if (documentDtoList.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("documentlist.failed"));
				statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("documentlist.success"));
				statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/getDocumentVertions/{docId}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> getLatestDocuments(@PathVariable("docId") Long docId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		
		try {
			documentDtoList = documentService.getVersionsByDoId(docId);
			if (documentDtoList.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("documentlist.failed"));
				statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("documentlist.success"));
				statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/document/list", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getDocumentList() {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		List<Document> documentList = new ArrayList<Document>();
		

		try {
			documentDtoList = documentService.getDocumentList();
			
			
			if (documentDtoList.size() == 0) {
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("documentlist.failed");
				statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setMessage("fetchSingleUser.success");
				statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	@CrossOrigin
	@RequestMapping(value = "/document/searchContent/{searchContent}", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> getDocumentBasedOnId(@PathVariable ("searchContent") String searchContent) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<Document> allDocumentObjectWithIdWhereSearchTextAvail = new ArrayList<Document>();		
		List<DocumentMetaDataTagInfoDto> documentsListUsingMetaDataTagValue = new ArrayList<DocumentMetaDataTagInfoDto>();
		List<DocumentDto> documentNameAndIdList = new ArrayList<DocumentDto>();

		
		Set<Long> documentIdsSet = new HashSet<Long>();
		List<DocumentLabelsDto> documentsListUsingLabels = new ArrayList<DocumentLabelsDto>();
		
		
		try {

			documentsListUsingMetaDataTagValue = userFavoriteSearchService.getDocumentsUsingMetaDataTagValue(searchContent);
	        documentsListUsingLabels = userFavoriteSearchService.getDocumentsUsingLabel(searchContent);

			allDocumentObjectWithIdWhereSearchTextAvail = documentService.searchContentInElasticSearch(searchContent);
		//	documentDtoIDList = modelToDtoConversion.documentToDocumentDtoConversion(allDocumentObjectWithIdWhereSearchTextAvail);
			
			documentIdsSet=documentService.findDocumentIdsIfContentAvail(documentsListUsingMetaDataTagValue,documentsListUsingLabels,allDocumentObjectWithIdWhereSearchTextAvail);
			documentNameAndIdList  = documentService.getDocumentNameUsingDocumentIds(documentIdsSet);
			
			

			if (documentNameAndIdList.size() != 0 ) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("documentIds.success"));
				statusResponseDTO.setDocumentDtoList(documentNameAndIdList);
                return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("documentIds.empty"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	
	
	@CrossOrigin
	@RequestMapping(value = "/downloadDoc/{docId}/{userId}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> downloadDoc(@PathVariable("docId") Long docId, @PathVariable("userId") Long userId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		//List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		
		try {
			statusResponseDTO = documentService.isDownloaded(docId, userId);
			if (statusResponseDTO!=null) {
//				statusResponseDTO.setStatus("Already");
//				statusResponseDTO.setMessage("File Already CheckOut By SomeOne");
				//statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage("Document Not Found");
				//statusResponseDTO.setDocumentDtoList(documentDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/revertCheckOut/{docId}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> revertCheckOut(@PathVariable("docId") Long docId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		
		try {
			boolean isRevert = documentService.isReverted(docId);
			if (!isRevert) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage("Revert Checkout Failed");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage("Revert Checkout Success");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/getDocMetaValues/{docId}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> getDocMetaValues(@PathVariable("docId") Long docId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<DocumentMetaDataTagInfoDto> docMetaValueList = new ArrayList<DocumentMetaDataTagInfoDto>();
		
		try {
			docMetaValueList = documentService.getDocMataAndValues(docId);
			if (docMetaValueList.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("documentlist.failed"));
				statusResponseDTO.setDocMetaValues(docMetaValueList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("documentlist.success"));
				statusResponseDTO.setDocMetaValues(docMetaValueList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	/*@CrossOrigin
	@RequestMapping(value = "/addnetworkdata", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> savemeatataginfo(@RequestBody NetworkDto networkDto) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			Boolean savenetshare=false;
			savenetshare=documentService.saveNetworkInfo(networkDto);
				if (savenetshare) {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("network.sucess"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				} else {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("network.failiure"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				}
			

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}*/

}
