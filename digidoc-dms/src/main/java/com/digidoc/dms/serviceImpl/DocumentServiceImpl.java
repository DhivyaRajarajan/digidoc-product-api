package com.digidoc.dms.serviceImpl;

import java.awt.image.BufferedImage;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tika.Tika;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ocr.TesseractOCRConfig;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import com.digidoc.admin.dto.DocumentDto;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;
import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.MetaDataTagValueDTO;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.es.model.EsDocument;
import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.DocumentLabel;
import com.digidoc.dataaccess.model.DocumentMetadataTagInfo;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.Label;
import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;
import com.digidoc.dataaccess.model.NetworkShare;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.repository.DocumentLabelRepsitory;
import com.digidoc.dataaccess.repository.DocumentMetadataTagInfoRepository;
import com.digidoc.dataaccess.repository.DocumentRepository;
import com.digidoc.dataaccess.repository.FolderRepository;
import com.digidoc.dataaccess.repository.LabelRepository;
import com.digidoc.dataaccess.repository.MetaColurRepository;
import com.digidoc.dataaccess.repository.MetaDataTagRepository;
import com.digidoc.dataaccess.repository.MetadataTagValueRepository;
import com.digidoc.dataaccess.repository.NetworkRepository;
import com.digidoc.dataaccess.repository.UserRepository;
import com.digidoc.dms.es.serviceImpl.EsDocumentServiceImpl;
import com.digidoc.dms.service.DocumentService;
import com.itextpdf.text.pdf.PdfReader;



import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

@Service
public class DocumentServiceImpl implements DocumentService {
	

	@Autowired
	DocumentRepository documentRepository;
	
	@Autowired
	MetaColurRepository metaColurRepository;

	@Autowired
	UserRepository userrepository;

	@Autowired
	FolderRepository folderRepository;
	
	@Autowired
	NetworkRepository networkRepository;

	@Autowired
	LabelRepository labelRepository;

	@Autowired
	DocumentLabelRepsitory documentLabelRepsitory;

	@Autowired
	DocumentMetadataTagInfoRepository documentMetadataTagInfoRepository;

	@Autowired
	MetaDataTagRepository metaDataTagRepository;

	@Autowired
	MetadataTagValueRepository metadataTagValueRepository;

	@Autowired
	EsDocumentServiceImpl esDocumentServiceImpl;

	@Autowired
	DocumentDto documentDto;

	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Autowired
	EsDocument esDocument;
	
	
	

	private static String getFileExtension(String fileName) {
		String extension = "";

		try {
			if (fileName != null) {
				extension = fileName.substring(fileName.lastIndexOf(".") + 1);
			}
		} catch (Exception e) {
			extension = "";
		}

		return extension;

	}

	private static String getFileName(String fileName) {
		String extension = "";

		try {
			if (fileName != null) {
				extension = fileName.substring(0, fileName.lastIndexOf("."));
			}
		} catch (Exception e) {
			extension = "";
		}

		return extension;

	}

	/* SKH - 28/08/2018 */
	@Override
	public String saveDocument(MultipartFile file, DocumentDto documentDto) {

		String fileType = getFileExtension(file.getOriginalFilename());
		String fileName = getFileName(file.getOriginalFilename());
		byte[] byteArr = null;
		String filepath = null;
		try {
			byteArr = file.getBytes();

			// mm
			File convFile = new File(file.getOriginalFilename());
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
			filepath = convFile.getAbsolutePath();
			
			/*
			 * String otherFile=filepath.replace("\\", "/");
			 * System.out.println("otherFile===>"+otherFile);
			 */
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Need to identify page count of multipart file
		// Need to research how to identify document total page number
		Document document = new Document();
		PdfReader reader = null;
		try {
			if (fileType.equalsIgnoreCase("pdf")) {
				reader = new PdfReader(byteArr);
				long n = reader.getNumberOfPages();
				
				document.setPageCount(n);
			} else if (fileType.equalsIgnoreCase("doc")) {
				InputStream myInputStream = new ByteArrayInputStream(byteArr);
				HWPFDocument wordDoc = new HWPFDocument(myInputStream);
				long pageCount = wordDoc.getSummaryInformation().getPageCount();
				
				document.setPageCount(pageCount);
			} else {
				long pageCount = 4;
				document.setPageCount(pageCount);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int randomNumber = (int) (Math.random() * 100000000) + 1;

		if (documentDto != null && byteArr != null) {

			User user = userrepository.findById(documentDto.getUserId());
			Folder folder = folderRepository.findById(documentDto.getFolderId());

			// Set values to document
			if (documentDto.getId() != null) {
				Document document1 = documentRepository.findById(documentDto.getId());
				if (document1 != null) {
					// document.setId(documentDto.getId());

					if (document1.getParentDocId() == null) {
						document.setParentDocId(document1.getId());
					} else {
						document.setParentDocId(document1.getParentDocId());
					}

					if (document1.getVersion() != null) {
						document.setVersion(document1.getVersion() + 1);
					} else {
						document.setVersion(1L);
					}

					// document.setId(null);
				}
			} else {
				document.setVersion(1L);
			}

			/*
			 * if(document.getVersion()!=null) {
			 * document.setVersion(document.getVersion()+1); }else {
			 * document.setVersion(1L); }
			 */
			document.setFilePath(filepath);
			document.setOriginalDocumentName(documentDto.getOriginalDocumentName());
			document.setDocumentContent(byteArr);
			document.setDocumentType(fileType);
			document.setDocumentUniqueId((long) randomNumber);
			document.setDocumentName(file.getOriginalFilename());
			document.setUser(user);
			document.setFolder(folder);
			// Set parent doc id if exists
			if (documentDto.getParentdocid() != null) {
				document.setParentDocId(documentDto.getParentdocid());
			}
			if (document != null) {
				document = documentRepository.save(document);

			saveDocumentContentInElastic(document);

				if (document != null) {

					List<String> labelNameCommaSeparated = Arrays.asList(documentDto.getLabelName().split(","));
					if (labelNameCommaSeparated != null) {
						for (String labelName : labelNameCommaSeparated) {
							Label label = new Label();
							label.setLabelName(labelName);

							label = labelRepository.save(label);
							DocumentLabel documentLabel = new DocumentLabel();
							documentLabel.setLabel(label);
							documentLabel.setDocument(document);
							documentLabel = documentLabelRepsitory.save(documentLabel);

						}
					}

					List<MetaDataTagDTO> tagArray = documentDto.getTagArray();
					if (tagArray != null) {

						for (MetaDataTagDTO metaDatatagDto : tagArray) {
							DocumentMetadataTagInfo documentMetadataTagInfo = new DocumentMetadataTagInfo();
							if (metaDatatagDto.getId() != null) {
								MetadataTag metadataTag = metaDataTagRepository.findOne(metaDatatagDto.getId());
								documentMetadataTagInfo.setMetadataTag(metadataTag);

							}
							// check id null or not
							if (metaDatatagDto.getMetaDataTagValueId() != null) {
								MetadataTagValue metadataTagValue = metadataTagValueRepository
										.findOne(metaDatatagDto.getMetaDataTagValueId());
								documentMetadataTagInfo.setMetadataTagValue(metadataTagValue);
							}
							if (metaDatatagDto.getMetaDataTagValue() != null) {
								documentMetadataTagInfo.setValue(metaDatatagDto.getMetaDataTagValue());
							}
							documentMetadataTagInfo.setDocument(document);

							documentMetadataTagInfo = documentMetadataTagInfoRepository.save(documentMetadataTagInfo);

						}

						return "Success";

					}
				}
			}
		}
		return "Failed to add document";
	}

	@Override
	public String updateDocument(MultipartFile scanFile, DocumentDto documentDto) {
		// TODO Auto-generated method stub
		saveDocument(scanFile, documentDto);
		return null;
	}

	@Override
	public List<DocumentDto> getDocumentsByFolderId(Long folderId) {
		
		
		Folder folder = new Folder();
		folder.setId(folderId);
		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		List<DocumentDto> finallist = new ArrayList<DocumentDto>();
		ModelToDtoConversion modelToDtoConversion = new ModelToDtoConversion();

		List<Document> documentList1 = documentRepository.findByFolder(folder);

		List<Document> documentList = new ArrayList<Document>();

		// for loop for get latest version api
		for (Document document : documentList1) {
			if (document.getParentDocId() == null) {
				Document doc = documentRepository.findFirstByParentDocIdOrderByVersionDesc(document.getId());
				
				if (doc != null) {
					documentList.add(doc);
				} else {
					documentList.add(document);
				}
			}
		}

		if (documentList != null) {

			documentDtoList = modelToDtoConversion.documentDtoToModel(documentList);

			if (documentDtoList != null) {
				for (DocumentDto documentDto : documentDtoList) {
					Document document=documentRepository.findById(documentDto.getId());
				List<DocumentMetadataTagInfo> docTagValueInfo=documentMetadataTagInfoRepository.findByDocument(document);
				
				for(DocumentMetadataTagInfo docmeta:docTagValueInfo) {
					if(docmeta.getValue().equalsIgnoreCase("Purchase Invoices") ||docmeta.getValue().equalsIgnoreCase("Purchase orders") ||docmeta.getValue().equalsIgnoreCase("Sales Invoice") ||
							docmeta.getValue().equalsIgnoreCase("Sale Credit Notes") ||docmeta.getValue().equalsIgnoreCase("Purchases returns") ) {
						MetadataTagValue tagvalue=metadataTagValueRepository.findOne(docmeta.getMetadataTagValue().getId());
						MetaColour colour=metaColurRepository.findByMetadataTagValue(tagvalue);
						documentDto.setDocumentColur(colour.getColurValue());
						
					}
					
				}
				finallist.add(documentDto);
				}
				
			}
			return finallist;
		}
		else {
			return finallist;
		}
		
	}

	@Override
	public List<DocumentDto> getVersionsByDoId(Long docId) {

		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		ModelToDtoConversion modelToDtoConversion = new ModelToDtoConversion();

		List<Document> documentList = new ArrayList<Document>();
		
		Document document = documentRepository.findById(docId);
		if (document.getParentDocId() != null) {
			documentList = documentRepository.findByParentDocId(document.getParentDocId());
			if (documentList.size() == 1) {
				documentList = new ArrayList<Document>();
				Document doc = documentRepository.findById(document.getParentDocId());
				documentList.add(doc);
			}
			documentDtoList = modelToDtoConversion.documentDtoToModel(documentList);
		}

		/*
		 * List<Document> documentList =
		 * documentRepository.findByParentDocId(docId); if (documentList.size()
		 * > 0) { documentDtoList =
		 * modelToDtoConversion.documentDtoToModel(documentList); if
		 * (documentDtoList != null) { return documentDtoList; } }
		 */
		return documentDtoList;
	}

	@Override
	public String isValidInput(DocumentDto documentDto) {
		// TODO Auto-generated method stub

		if (documentDto.getFolderId() == null) {
			return "FolderId is null";
		}
		if (documentDto.getUserId() == null) {
			return "UserId is null";
		}
		if (documentDto.getLabelName() == null) {
			return "LabelName is null";
		}
		if (documentDto.getTagArray() == null) {
			return "TagArray is null";
		} else {
			return "success";
		}

	}

	@Override
	public DocumentService findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DocumentDto> getDocumentList() {
		// TODO Auto-generated method stub
		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		List<DocumentDto> finallist = new ArrayList<DocumentDto>();
		List<Document> documentList = new ArrayList<Document>();
		documentList = documentRepository.findAll();
		if (documentList != null) {
			documentDtoList = modelToDtoConversion.documentToDocumentDtoConversion(documentList);
			
		}
		for (DocumentDto documentDto : documentDtoList) {
			Document document=documentRepository.findById(documentDto.getId());
		List<DocumentMetadataTagInfo> docTagValueInfo=documentMetadataTagInfoRepository.findByDocument(document);
		
		for(DocumentMetadataTagInfo docmeta:docTagValueInfo) {
			if(docmeta.getValue().equalsIgnoreCase("Purchase Invoices") ||docmeta.getValue().equalsIgnoreCase("Purchase orders") ||docmeta.getValue().equalsIgnoreCase("Sales Invoice") ||
					docmeta.getValue().equalsIgnoreCase("Sale Credit Notes") ||docmeta.getValue().equalsIgnoreCase("Purchases returns") ) {
				MetadataTagValue tagvalue=metadataTagValueRepository.findOne(docmeta.getMetadataTagValue().getId());
				MetaColour colour=metaColurRepository.findByMetadataTagValue(tagvalue);
				documentDto.setDocumentColur(colour.getColurValue());
				
		}
			
		}
		finallist.add(documentDto);
		}
		
		return finallist;
	}

	@Override
	public String findByDocId(DocumentDto documentDto) {
		// TODO Auto-generated method stub
		Document document = new Document();
		Tika tika = new Tika();
		String content = null;
		document = documentRepository.findOne(documentDto.getId());
		byte[] documentByte = null;

		if (document != null) {
			documentByte = document.getDocumentContent();
		}
		try {
			if ("doc".equalsIgnoreCase(document.getDocumentType())
					|| "docx".equalsIgnoreCase(document.getDocumentType())
					|| "xls".equalsIgnoreCase(document.getDocumentType())) {

				InputStream myInputStream = new ByteArrayInputStream(documentByte);
				content = tika.parseToString(myInputStream);
			} else {
				System.setProperty("sun.java2d.cmm", "sun.java2d.cmm.kcms.KcmsServiceProvider");
				ITesseract instance = new Tesseract();
				File tessDataFolder = LoadLibs.extractTessResources("tessdata");
				instance.setDatapath(tessDataFolder.getAbsolutePath());
				File convFile = new File(document.getDocumentName() + "." + document.getDocumentType());
				convFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(convFile);
				fos.write(documentByte);
				fos.close();
				try {
					content = instance.doOCR(convFile);
				} catch (Exception e) {
					// TODO Auto-generated catch block
				//	e.printStackTrace();
					return "Failure";
				}
			}

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (TikaException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return content;

	}

	private void saveDocumentContentInElastic(Document document) {
		// TODO Auto-generated method stub
		if (document != null) {
			Document document2 = documentRepository.findById(document.getId());
			// byte[] byteArrForElastic = null;
			// byteArrForElastic=document2.getDocumentContent();
			DocumentDto documentDtoEs = new DocumentDto();
			documentDtoEs.setId(document.getId());
			documentDtoEs.setDocumentContent(document2.getDocumentContent());
			String contentFromDocomentTable = findByDocId(documentDtoEs);
			contentFromDocomentTable = contentFromDocomentTable.replaceAll("\t", "");
			contentFromDocomentTable = contentFromDocomentTable.replaceAll("\n", "");
			List<String> contentList = new ArrayList<String>();
			contentList.add(contentFromDocomentTable);
			EsDocument esDocument = new EsDocument(document.getId(), contentFromDocomentTable, document.getId());
			esDocumentServiceImpl.save(esDocument);
		}
	}

	@Override
	public List<Document> searchContentInElasticSearch(String searchContent) {
		// TODO Auto-generated method stub
		if (searchContent != null) {
			List<Document> allDocumentObjectWithIdWhereSearchTextAvail = esDocumentServiceImpl
					.findAllContentFromElasticSearch(searchContent);
			return allDocumentObjectWithIdWhereSearchTextAvail;
		}
		return null;
	}

	@Override
	public Set<Long> findDocumentIdsIfContentAvail(List<DocumentMetaDataTagInfoDto> documentsListUsingMetaDataTagValue,
			List<DocumentLabelsDto> documentsListUsingLabels,
			List<Document> allDocumentObjectWithIdWhereSearchTextAvail) {
		// TODO Auto-generated method stub
		Set<Long> documentIdsSet = new HashSet<Long>();
		if (documentsListUsingMetaDataTagValue.size() != 0) {
			for (DocumentMetaDataTagInfoDto documentMetaDataTagInfoDto : documentsListUsingMetaDataTagValue) {
				documentIdsSet.add(documentMetaDataTagInfoDto.getDocumentId());
			}
		}
		if (documentsListUsingLabels.size() != 0) {
			for (DocumentLabelsDto documentLabelsDto : documentsListUsingLabels) {
				documentIdsSet.add(documentLabelsDto.getDocumentId());
			}
		}
		if (allDocumentObjectWithIdWhereSearchTextAvail.size() != 0) {
			for (Document document : allDocumentObjectWithIdWhereSearchTextAvail) {
				documentIdsSet.add(document.getId());
			}
		}

		
		return documentIdsSet;
	}

	@Override
	public List<DocumentDto> getDocumentNameUsingDocumentIds(Set<Long> documentIdsSet) {

		List<DocumentDto> documents = new ArrayList<DocumentDto>();
		if (documentIdsSet != null) {
			for (Long documentId : documentIdsSet) {

				Document document = documentRepository.findById(documentId);
				if (document != null) {

					documentDto = modelToDtoConversion.documentTodocumentDto(document);
				}
				documents.add(documentDto);

			}

		}

		return documents;

	}
	
	

	@Override
	public StatusResponseDTO isDownloaded(Long docId, Long userId) {
		Document doc = documentRepository.findById(docId);
		StatusResponseDTO sr = new StatusResponseDTO();
		if(doc!= null) {
			if(doc.getCheckoutBy()!=null) {
				if(doc.getCheckoutBy()==userId) {
					/*if(doc.isCheckoutStatus()==true) {
						sr.setStatus("Failure");
						sr.setMessage("Already Checked Out This File By You Please CheckIn It");
						return sr;
					}else {*/
					Document changeDocument = changeCheckoutStatus(doc, userId);
						if(changeDocument!=null) {
							sr.setStatus("Success");
							//sr.setMessage("File Downloaded In Following Path "+filePath);
							ByteArrayResource resource = new ByteArrayResource(changeDocument.getDocumentContent());
							sr.setBar(resource);
							sr.setOriginalDocumentName(changeDocument.getOriginalDocumentName());
							return sr;
						}
					/*}*/
					
				}else if(doc.getCheckoutBy()!=userId){
					if(doc.isCheckoutStatus()==true) {
						sr.setStatus("Failure");
						sr.setMessage("Already Checked Out This File By SomeOne");
						return sr;
					}else {
						Document changeDocument = changeCheckoutStatus(doc, userId);
						if(changeDocument!=null) {
							sr.setStatus("Success");
							//sr.setMessage("File Downloaded In Following Path "+filePath);
							ByteArrayResource resource = new ByteArrayResource(changeDocument.getDocumentContent());
							sr.setBar(resource);
							sr.setOriginalDocumentName(changeDocument.getOriginalDocumentName());
							return sr;
						}
					}
				}
				/*else {
					String filePath = downloadDoc(doc, userId);
					if(filePath!="") {
						sr.setStatus("Success");
						sr.setMessage("File Downloaded In Following Path "+filePath);
						return sr;
					}
				}*/
				
			}else if(doc.getCheckoutBy()==null){
				Document changeDocument = changeCheckoutStatus(doc, userId);
				if(changeDocument!=null) {
					sr.setStatus("Success");
					//sr.setMessage("File Downloaded In Following Path "+filePath);
					ByteArrayResource resource = new ByteArrayResource(changeDocument.getDocumentContent());
					sr.setBar(resource);
					sr.setOriginalDocumentName(changeDocument.getOriginalDocumentName());
					return sr;
				}
			}
		}
		
		return sr;
	}
	
	
	public Document changeCheckoutStatus(Document doc, Long userId) {
			
			doc.setCheckoutBy(userId);
			doc.setCheckoutStatus(true);
			Document docu = documentRepository.save(doc);
			return docu;
	}
	
	/*public String downloadDoc(Document doc, Long userId) {
		
		doc.setCheckoutBy(userId);
		doc.setCheckoutStatus(true);
		documentRepository.save(doc);
		String fileLocation="";
		FileOutputStream fileOuputStream = null;
		File stockDir = new File("E://Stock//stockDir//");
		stockDir.mkdirs();
		System.out.println("stockDir.getAbsolutePath()"+stockDir.getAbsolutePath());
			try { 
			    fileOuputStream = new FileOutputStream(stockDir.getAbsolutePath()+"//"+doc.getOriginalDocumentName()); 
			    fileOuputStream.write(doc.getDocumentContent());
			    fileLocation = stockDir.getAbsolutePath()+"\\"+doc.getOriginalDocumentName();
			    System.out.println("fileLocation==>"+fileLocation);
			    return fileLocation;
			 } catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
			    try {
					fileOuputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			 }
			return fileLocation;
	}*/

	@Override
	public boolean isReverted(Long docId) {
		Document doc = documentRepository.findById(docId);
		if(doc!=null) {
			doc.setCheckoutStatus(false);
			documentRepository.save(doc);
			return true;
		}else {
			return false;
		}
		
	}

	@Override
	public List<DocumentMetaDataTagInfoDto> getDocMataAndValues(Long documentId) {
		Document dc= new Document();
		dc.setId(documentId);
		List<DocumentLabel> docLableList= documentLabelRepsitory.findByDocument(dc);
		StringBuilder lableData = new StringBuilder();
		for(int i=0;i<docLableList.size();i++) {
			if(i==docLableList.size()-1) {
				lableData.append(docLableList.get(i).getLabel().getLabelName());
			}else {
				lableData.append(docLableList.get(i).getLabel().getLabelName()+",");
			}
			
		}
		
		List<DocumentMetadataTagInfo> docMetaValues = documentMetadataTagInfoRepository.findByDocument(dc);
		List<DocumentMetaDataTagInfoDto> dtoList = new ArrayList<DocumentMetaDataTagInfoDto>();
		if(docMetaValues.size()>0) {
			for(DocumentMetadataTagInfo docMetaModel: docMetaValues) {
				DocumentMetaDataTagInfoDto docmetavalue = new DocumentMetaDataTagInfoDto();
				docmetavalue.setDocumentId(docMetaModel.getDocument().getId());
				docmetavalue.setMetaDataTagId(docMetaModel.getMetadataTag().getId());
				if(docMetaModel.getMetadataTagValue()!=null) {
					docmetavalue.setMetaDataTagValueId(docMetaModel.getMetadataTagValue().getId());
				}
				/*docmetavalue.setDocLableValues(lableData.toString());*/
				
				/*MetaDataTagValueDTO metaDataTagValueDTO = new MetaDataTagValueDTO();
				metaDataTagValueDTO.setId(docMetaModel.getMetadataTagValue().getId());
				metaDataTagValueDTO.setMetadataTagValue(docMetaModel.getMetadataTagValue().getMetadataTagValue());
				docmetavalue.setMetaDataTagValueDTO(metaDataTagValueDTO);*/
				docmetavalue.setValue(docMetaModel.getValue());
				dtoList.add(docmetavalue);
			}
			dtoList.get(0).setDocLableValues(lableData.toString());
		}
		return dtoList;
	}

	

	
	/*public Boolean saveNetworkInfo(NetworkDto networkDto) {
		Boolean get=false;
		Folder folder=folderRepository.findById(networkDto.getFolderid());
		NetworkShare netshare=modelToDtoConversion.netDtotoModel(networkDto, folder);
		NetworkShare netshareSave=null;
		netshareSave=networkRepository.save(netshare);
		if(netshareSave!=null) {
			String fileContent = "This is a test file";

		//	new FolderServiceImpl().copyFiles(fileContent, "testFile.text");

			get=getNetworkFiles(networkDto);
			if(get) {
			return true;
			}
			else {
				return false;
			}
			
		}
		else {
			return false;
		}
	}
	
	public boolean getNetworkFiles(NetworkDto networkDto) {

		boolean successful = false;
		try {
			String userdetails= networkDto.getPassword() + ":" + networkDto.getUserName();
			System.out.println("User: " + userdetails);

			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(userdetails);
			String path = networkDto.getNetworkPath();
			System.out.println("Path: " + path);

			SmbFile sFile = new SmbFile(path, auth);

			SmbFile filesList[] = sFile.listFiles();
			FileInputStream fnsr = null;
			InputStream in = null;
			OutputStream outputStream = null;
			Document documentbal = new Document();
			for (SmbFile smbFile : filesList) {
				System.out.println(smbFile.getName());
				in = smbFile.getInputStream();
				
				outputStream = 
	                    new FileOutputStream(new File(smbFile.getName()));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			String fileType = getFileExtension(smbFile.getName());
				System.out.println("byteArr byteArr byteArr--length--" + bytes.length);
				// Need to identify page count of multipart file
				// Need to research how to identify document total page number
				Document document = new Document();
				PdfReader reader = null;
				try {
					if (fileType.equalsIgnoreCase("pdf")) {
						reader = new PdfReader(bytes);
						long n = reader.getNumberOfPages();
						System.out.println("else in pdf file--pageCount--" + n);
						document.setPageCount(n);
					} else if (fileType.equalsIgnoreCase("doc")) {
						InputStream myInputStream = new ByteArrayInputStream(bytes);
						HWPFDocument wordDoc = new HWPFDocument(myInputStream);
						long pageCount = wordDoc.getSummaryInformation().getPageCount();
						System.out.println("else in doc file--pageCount--" + pageCount);
						document.setPageCount(pageCount);
					} else {
						long pageCount = 4;
						document.setPageCount(pageCount);
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int randomNumber = (int) (Math.random() * 100000000) + 1;

				if ( bytes != null) {

					User user = userrepository.findById(networkDto.getUserId());
					Folder folder = folderRepository.findById(networkDto.getFolderid());

					// Set values to document
					if (documentDto.getId() != null) {
						Document document1 = documentRepository.findById(documentDto.getId());
						if (document1 != null) {
							// document.setId(documentDto.getId());

							if (document1.getParentDocId() == null) {
								document.setParentDocId(document1.getId());
							} else {
								document.setParentDocId(document1.getParentDocId());
							}

							if (document1.getVersion() != null) {
								document.setVersion(document1.getVersion() + 1);
							} else {
								document.setVersion(1L);
							}

							// document.setId(null);
						}
					} else {
						document.setVersion(1L);
					}

					
					  if(document.getVersion()!=null) {
					 document.setVersion(document.getVersion()+1); }else {
					  document.setVersion(1L); }
					 
					document.setFilePath(smbFile.getPath());
					document.setOriginalDocumentName(smbFile.getName());
					document.setDocumentContent(bytes);
					document.setDocumentType(fileType);
					document.setDocumentUniqueId((long) randomNumber);
					document.setDocumentName(smbFile.getName());
					document.setUser(user);
					document.setFolder(folder);
					// Set parent doc id if exists
					if (documentDto.getParentdocid() != null) {
						document.setParentDocId(documentDto.getParentdocid());
					}
					if (document != null) {
						document = documentRepository.save(document);

						//saveDocumentContentInElastic(document);

						if (document != null) {

							List<String> labelNameCommaSeparated = Arrays.asList(documentDto.getLabelName().split(","));
							if (labelNameCommaSeparated != null) {
								for (String labelName : labelNameCommaSeparated) {
									Label label = new Label();
									label.setLabelName(labelName);

									label = labelRepository.save(label);
									DocumentLabel documentLabel = new DocumentLabel();
									documentLabel.setLabel(label);
									documentLabel.setDocument(document);
									documentLabel = documentLabelRepsitory.save(documentLabel);

								}
							}

							List<MetaDataTagDTO> tagArray = documentDto.getTagArray();
							if (tagArray != null) {

								for (MetaDataTagDTO metaDatatagDto : tagArray) {
									DocumentMetadataTagInfo documentMetadataTagInfo = new DocumentMetadataTagInfo();
									if (metaDatatagDto.getId() != null) {
										MetadataTag metadataTag = metaDataTagRepository.findOne(metaDatatagDto.getId());
										documentMetadataTagInfo.setMetadataTag(metadataTag);

									}
									// check id null or not
									if (metaDatatagDto.getMetaDataTagValueId() != null) {
										MetadataTagValue metadataTagValue = metadataTagValueRepository
												.findOne(metaDatatagDto.getMetaDataTagValueId());
										documentMetadataTagInfo.setMetadataTagValue(metadataTagValue);
									}
									if (metaDatatagDto.getMetaDataTagValue() != null) {
										documentMetadataTagInfo.setValue(metaDatatagDto.getMetaDataTagValue());
									}
									documentMetadataTagInfo.setDocument(document);

									documentMetadataTagInfo = documentMetadataTagInfoRepository.save(documentMetadataTagInfo);

								}

								

							}
						}
					}
				
			}
			
			System.out.println("Successful" +bytes );
			
			}

			successful = true;
			
		} catch (Exception e) {
			successful = false;
			e.printStackTrace();
		}
		return successful;

	}*/
}
