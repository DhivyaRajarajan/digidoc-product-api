package com.digidoc.ocr.dto;


public class DisplayExtractedOcrDto {

private String key;
public String getKey() {
	return key;
}
public void setKey(String key) {
	this.key = key;
}
private String values;

private byte[] arr;

public byte[] getArr() {
	return arr;
}
public void setArr(byte[] arr) {
	this.arr = arr;
}

public String getValues() {
	return values;
}
public void setValues(String values) {
	this.values = values;
}


}
