package com.digidoc.ocr.dto;

import java.util.List;

public class ZonalTemplateOcrDto {
	private Long id;
	private Long keywordId;
	private String supplierName;
	private double x;
	private double y;
	private double width;
	private double height;
	private List<ZonalTemplateOcrDto> zonalTemplateOcrDtoList;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getKeywordId() {
		return keywordId;
	}
	public void setKeywordId(Long keywordId) {
		this.keywordId = keywordId;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public double getX() {
		return x;
	}
	public void setX(double x) {
		this.x = x;
	}
	public double getY() {
		return y;
	}
	public void setY(double y) {
		this.y = y;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public List<ZonalTemplateOcrDto> getZonalTemplateOcrDtoList() {
		return zonalTemplateOcrDtoList;
	}
	public void setZonalTemplateOcrDtoList(List<ZonalTemplateOcrDto> zonalTemplateOcrDtoList) {
		this.zonalTemplateOcrDtoList = zonalTemplateOcrDtoList;
	}
	
	
}
