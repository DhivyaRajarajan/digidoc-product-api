package com.digidoc.ocr.dto;

import com.digidoc.dataaccess.model.Keyword;

public class DocumentTypeKeywordDto {

	private Long id;
	private String documentType;
	private Long keywordId;
	private String keyword;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public Long getKeywordId() {
		return keywordId;
	}
	public void setKeywordId(Long keywordId) {
		this.keywordId = keywordId;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
		
}
