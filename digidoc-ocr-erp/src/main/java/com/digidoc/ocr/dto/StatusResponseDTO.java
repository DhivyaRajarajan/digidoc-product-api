package com.digidoc.ocr.dto;

import java.util.List;

import lombok.Data;

@Data
public class StatusResponseDTO {
    private String status;
   
    private String message;
    private OcrExtractionDto ocrExtractionDto;
    private byte[] arrayValues;
    private List<DisplayExtractedOcrDto> ocrExtractedShow;
    private String filePath;
    private List<DocumentTypeKeywordDto> docTypeKeywordList;
    
    public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public OcrExtractionDto getOcrExtractionDto() {
		return ocrExtractionDto;
	}
	public void setOcrExtractionDto(OcrExtractionDto ocrExtractionDto) {
		this.ocrExtractionDto = ocrExtractionDto;
	}
	public byte[] getArrayValues() {
		return arrayValues;
	}
	public void setArrayValues(byte[] arrayValues) {
		this.arrayValues = arrayValues;
	}
	public List<DisplayExtractedOcrDto> getOcrExtractedShow() {
		return ocrExtractedShow;
	}
	public void setOcrExtractedShow(List<DisplayExtractedOcrDto> ocrExtractedShow) {
		this.ocrExtractedShow = ocrExtractedShow;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public List<DocumentTypeKeywordDto> getDocTypeKeywordList() {
		return docTypeKeywordList;
	}
	public void setDocTypeKeywordList(List<DocumentTypeKeywordDto> docTypeKeywordList) {
		this.docTypeKeywordList = docTypeKeywordList;
	}
	
    
    
}
