package com.digidoc.ocr.dto;

public class ViewerDto {
	
	private byte[] fileArray;
	private String fileExtension;
	 private String documentName;
	 //fd
	 private String wholeString;

	public String getWholeString() {
		return wholeString;
	}

	public void setWholeString(String wholeString) {
		this.wholeString = wholeString;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public byte[] getFileArray() {
		return fileArray;
	}

	public void setFileArray(byte[] fileArray) {
		this.fileArray = fileArray;
	}

	public String getFileExtension() {
		return fileExtension;
	}

	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

}
