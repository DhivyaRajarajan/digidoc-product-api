package com.digidoc.ocr.dto;


import java.util.List;

public class OcrExtractionDto {
	private Long id;
	private String keywordName;
	private String values;
	List<OcrExtractionDto> sentList;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKeywordName() {
		return keywordName;
	}
	public void setKeywordName(String keywordName) {
		this.keywordName = keywordName;
	}
	public String getValues() {
		return values;
	}
	public void setValues(String values) {
		this.values = values;
	}
	public List<OcrExtractionDto> getSentList() {
		return sentList;
	}
	public void setSentList(List<OcrExtractionDto> sentList) {
		this.sentList = sentList;
	}
	
}
