package com.digidoc.ocr.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.ocr.dto.DisplayExtractedOcrDto;
import com.digidoc.ocr.dto.OcrExtractionDto;
import com.digidoc.ocr.dto.ViewerDto;

@Service
public interface FileService {
	 public OcrExtractionDto scanImageFile(Document document);
     public Document documentDetails(Long id);
     
     
     public List<DisplayExtractedOcrDto> Extraction(Long id);
     public byte[] byteArrayDocument(Long id);
     public String filepath(Long id);
     
     
	public ViewerDto documentIdtoByteArr(Long docId);
	public ViewerDto documentIdtoString(Long docId);
	public ViewerDto documentToPdf(Long docId);
	
	
     
     /*public CheckingDto check(Long id);
     public ExtractInfoInvoiceHeaderDto headerInfo(Long id);*/
     
     
}
