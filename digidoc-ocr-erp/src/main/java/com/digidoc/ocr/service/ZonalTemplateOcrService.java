package com.digidoc.ocr.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digidoc.ocr.dto.DocumentTypeKeywordDto;
import com.digidoc.ocr.dto.ZonalTemplateOcrDto;


public interface ZonalTemplateOcrService {

	 public Boolean saveZonalTemplateOcr(List<ZonalTemplateOcrDto> zonalTemplateOcrDtoList);
	 public List<DocumentTypeKeywordDto> getDocTypeList();
	 public List<DocumentTypeKeywordDto> getKeywordsByDocType(String docType);
}
