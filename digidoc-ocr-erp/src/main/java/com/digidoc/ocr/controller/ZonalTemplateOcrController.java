package com.digidoc.ocr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.DocumentTypeKeyword;
import com.digidoc.dataaccess.repository.DocumentTypeKeyWordRepository;
import com.digidoc.ocr.dto.DocumentTypeKeywordDto;
import com.digidoc.ocr.dto.OcrExtractionDto;
import com.digidoc.ocr.dto.StatusResponseDTO;
import com.digidoc.ocr.dto.ZonalTemplateOcrDto;
import com.digidoc.ocr.service.ZonalTemplateOcrService;
import com.google.gson.Gson;

@RestController
public class ZonalTemplateOcrController {

	@Autowired
    private Environment env;
	
	@Autowired
	ZonalTemplateOcrService zonalTemplateOcrService;
	
	@Autowired
	DocumentTypeKeyWordRepository documentTypeKeyWordRepository;
	
	
	@PostMapping(value = "/saveZonalTemplate", produces = {"application/json" })
	public ResponseEntity<String> saveZonalTemplate(@RequestBody ZonalTemplateOcrDto zonalTemplateOcrDtoList) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			boolean isSaved = zonalTemplateOcrService.saveZonalTemplateOcr(zonalTemplateOcrDtoList.getZonalTemplateOcrDtoList());
			if(isSaved) {
				statusResponseDTO.setMessage("Template List Saved Successfully");
				statusResponseDTO.setStatus(env.getProperty("success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}else {
				statusResponseDTO.setMessage("Template List Saving Failed");
				statusResponseDTO.setStatus(env.getProperty("Failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	
	@GetMapping(value = "/getDocTypeList", produces = {"application/json" })
	public ResponseEntity<String> getDocTypeList() {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			List<DocumentTypeKeywordDto> docTypeKeywordDtoList = zonalTemplateOcrService.getDocTypeList();
			if(docTypeKeywordDtoList.size() > 0) {
				statusResponseDTO.setMessage("Getting DocumentType Keyword List Successfully");
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setDocTypeKeywordList(docTypeKeywordDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}else {
				statusResponseDTO.setMessage("Getting DocumentType Keyword Failed");
				statusResponseDTO.setStatus("Failed");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage("server.problem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	@GetMapping(value = "/getKeywords/{docType}", produces = {"application/json" })
	public ResponseEntity<String> getKeywordsByDocType(@PathVariable() String docType) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			List<DocumentTypeKeywordDto> docTypeKeywordDtoList = zonalTemplateOcrService.getKeywordsByDocType(docType);
			if(docTypeKeywordDtoList.size() > 0) {
				statusResponseDTO.setMessage("Getting DocumentType Keyword List Successfully");
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setDocTypeKeywordList(docTypeKeywordDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}else {
				statusResponseDTO.setMessage("Getting DocumentType Keyword Failed");
				statusResponseDTO.setStatus("Failed");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
}
