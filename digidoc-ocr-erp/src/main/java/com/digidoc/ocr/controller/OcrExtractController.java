package com.digidoc.ocr.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.ocr.dto.DisplayExtractedOcrDto;
//import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.ocr.dto.OcrExtractionDto;
import com.digidoc.ocr.dto.StatusResponseDTO;
import com.digidoc.ocr.dto.ViewerDto;
import com.digidoc.ocr.service.FileService;
import com.google.gson.Gson;


@RestController
public class OcrExtractController {

	@Autowired
	FileService fileService;
	
	@Autowired
    private Environment env;

	// testing service
	@GetMapping("/ocrtest/myself/{id}")
	public String myselftest(@PathVariable(value = "id") Long id) {
		return "success";
	}

	@PostMapping(value = "/scan/{docId}", produces = {"application/json" })
	public ResponseEntity<String> documentScan(@PathVariable() Long docId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		statusResponseDTO.setStatus("failure");
		try {
			
			
			Document document=fileService.documentDetails(docId);
			
			
			OcrExtractionDto ocrExtractionDto=null;
			
			if(document==null) 
				System.out.println("document null only");
			else {
				ocrExtractionDto = fileService.scanImageFile(document);
				statusResponseDTO.setOcrExtractionDto(ocrExtractionDto);
				statusResponseDTO.setStatus(env.getProperty("success"));
			}
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	    
	@GetMapping("/displayExtracted/{id}")
	public ResponseEntity<String> displayExtracted1(@PathVariable(value = "id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		
		
		List<DisplayExtractedOcrDto> displayExtracted=new ArrayList<DisplayExtractedOcrDto>();
		try {
		displayExtracted=fileService.Extraction(id);
		if(displayExtracted!=null) {
		statusResponseDTO.setStatus("success");
		statusResponseDTO.setOcrExtractedShow(displayExtracted);
		
		/*byte[] arrDetails=fileService.byteArrayDocument(id);
		statusResponseDTO.setArrayValues(arrDetails);*/
		String filePath=fileService.filepath(id);
		statusResponseDTO.setFilePath(filePath);
		
		}else {
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
		
		}catch(Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
		return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
	}
	

    @GetMapping(value = "/file/download/{docId}", produces = {
            "application/json"})
    public ResponseEntity<ByteArrayResource> downloadExpiryDocumentFile(@PathVariable(value = "docId") Long docId) {
        StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
        statusResponseDTO.setStatus("failure");
        
        
       // ExpiryDocumentDTO expiryDocumentDTO = expiryDocumentService.downloadFileBasedOnExpiryId(expiryDocumentId);
       // ByteArrayResource resource = new ByteArrayResource(expiryDocumentDTO.getFileArray());
        
        ViewerDto viewerDto=fileService.documentIdtoByteArr(docId);
        ByteArrayResource resource = new ByteArrayResource(viewerDto.getFileArray());
        
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Disposition", "attachment; filename=" + viewerDto.getDocumentName() + "." + viewerDto.getFileExtension());

        return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);

    }
    
    
	@GetMapping(value = "/file/convertToPdf/{docId}", produces = { "application/json" })
	public ResponseEntity<ByteArrayResource> convertToPdf(@PathVariable(value = "docId") Long docId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		statusResponseDTO.setStatus("failure");

		// ExpiryDocumentDTO expiryDocumentDTO =
		// expiryDocumentService.downloadFileBasedOnExpiryId(expiryDocumentId);
		// ByteArrayResource resource = new
		// ByteArrayResource(expiryDocumentDTO.getFileArray());

		ViewerDto viewerDto = fileService.documentToPdf(docId);
		ByteArrayResource resource = new ByteArrayResource(viewerDto.getFileArray());

		HttpHeaders headers = new HttpHeaders();
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		headers.add("Content-Disposition",
				"attachment; filename=" + viewerDto.getDocumentName() + "." + viewerDto.getFileExtension());

		return ResponseEntity.ok().headers(headers).contentType(MediaType.parseMediaType("application/octet-stream"))
				.body(resource);

	}
    
    
	@GetMapping(value = "/file/download/tika/{docId}")
	public String downloadExpiryDocumentFileToString(@PathVariable(value = "docId") Long docId) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		String returnvalue = "No Tika Data";
		try {
		statusResponseDTO.setStatus("failure");

		ViewerDto viewerDto = fileService.documentIdtoString(docId);
		
		String content=viewerDto.getWholeString();
		

		if (content.isEmpty()) 
			System.out.println("empty");
		else 
			returnvalue=content;
		
		
		}catch(Exception e) {
			e.printStackTrace();
		}
		return returnvalue;

	}
	
	
	
	/*
	@GetMapping(value = "/displayExtracted")
	public ResponseEntity<String> displayExtracted() {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		statusResponseDTO.setStatus("failure");
		try {
			
			DisplayExtractedOcrDto displayExtracted=new DisplayExtractedOcrDto();
			displayExtracted=fileService.Extraction();
			
			if(displayExtracted==null) {
				System.out.println("document null only");
			}else {
				displayExtracted=fileService.Extraction();
				statusResponseDTO.setDisplayExtractedOcrDto(displayExtracted);
				statusResponseDTO.setStatus(env.getProperty("success"));
			}
			//return new ResponseEntity<String>("checking", HttpStatus.OK);
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("server.problem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}*/
	
}