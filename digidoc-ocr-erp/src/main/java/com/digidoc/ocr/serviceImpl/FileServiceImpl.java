package com.digidoc.ocr.serviceImpl;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.persistence.EntityNotFoundException;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.ParseException;
import org.apache.http.client.utils.DateUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.ghost4j.GhostscriptException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.DocumentTypeKeyword;
import com.digidoc.dataaccess.model.InvoiceHeaderInfo;
import com.digidoc.dataaccess.model.InvoiceInfo;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.repository.AliasRepository;
import com.digidoc.dataaccess.repository.DocumentRepository;
import com.digidoc.dataaccess.repository.DocumentTypeKeyWordRepository;
import com.digidoc.dataaccess.repository.InvoiceHeaderInfoRepository;
import com.digidoc.dataaccess.repository.InvoiceInfoRepository;
import com.digidoc.dataaccess.repository.KeyWordsRepository;
import com.digidoc.ocr.dto.DisplayExtractedOcrDto;
import com.digidoc.ocr.dto.OcrExtractionDto;
import com.digidoc.ocr.dto.StatusResponseDTO;
import com.digidoc.ocr.dto.ViewerDto;
import com.digidoc.ocr.service.FileService;
import com.itextpdf.text.pdf.PdfWriter;
import com.recognition.software.jdeskew.ImageDeskew;

import net.sourceforge.tess4j.TesseractException;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.util.ImageHelper;
import net.sourceforge.tess4j.util.LoadLibs;
import java.awt.Color;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.*;
import java.util.Iterator;
//itext libraries to write PDF file
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;


@Service
public class FileServiceImpl implements FileService {
	@Autowired
	DocumentTypeKeyWordRepository documentTypeKeyWordRepository;

	@Autowired
	KeyWordsRepository keywordsRepository;

	@Autowired
	AliasRepository aliasRepository;

	@Autowired
	InvoiceInfoRepository invoiceInfoRepository;

	@Autowired
	DocumentRepository documentRepository;

	@Autowired
	InvoiceHeaderInfoRepository invoiceHeaderInfoRepository;
	
	
	private static BufferedImage grayscale, binarized;
	static final double MINIMUM_DESKEW_THRESHOLD = 0.01d;
	
	@Override  
	public OcrExtractionDto scanImageFile(Document document) {

		ITesseract instance = new Tesseract();
		OcrExtractionDto ocrExtractionDto = null;
		// In case you don't have your own tessdata, let it also be extracted for you
		File tessDataFolder = LoadLibs.extractTessResources("tessdata");
		// Set the tessdata path
		instance.setDatapath(tessDataFolder.getAbsolutePath());
		File convFile = null;
		try {
			
/*			byte[] docContent = document.getDocumentContent();
			convFile = new File(document.getDocumentName() + "." + document.getDocumentType());
			convFile.createNewFile();

			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(docContent);
			fos.close();

			String result = instance.doOCR(convFile);
			System.err.println("result==========>" + result);
			ocrExtractionDto = scanDocInfo(result, document.getId());*/
			
			String fileType=document.getDocumentType();
			String content = null;
			
			if(fileType.equalsIgnoreCase(".doc") || fileType.equalsIgnoreCase(".docx") || fileType.equalsIgnoreCase(".xl") || fileType.equalsIgnoreCase(".xlsx")) {
				
					Tika tika = new Tika();
					
					byte[] documentByte = null;
					documentByte = document.getDocumentContent();
					InputStream myInputStream = new ByteArrayInputStream(documentByte);

					try {
						content = tika.parseToString(myInputStream);
					} catch (TikaException e) {
						e.printStackTrace();
					}
					
			}else if(fileType.equalsIgnoreCase(".pdf")) {

			    
				PDDocument doc = PDDocument.load(document.getDocumentContent());
				PDFRenderer pdfRenderer = new PDFRenderer(doc);
				int pages = doc.getNumberOfPages();

				// pdf version increase
				//doc.setVersion((float) 1.7);
			          
				for (int i = 0; i < pages; i++) {
					// 300-dpi
					// BufferedImage bffim = pdfRenderer.renderImageWithDPI(i, 300, ImageType.RGB);
					BufferedImage bffim = pdfRenderer.renderImage(i);
					

					// rescale to improve its pixel color more
					//RescaleOp rescaleOp = new RescaleOp(0.8f, 20, null);
					//rescaleOp.filter(bffim, bffim);

					ImageDeskew id = new ImageDeskew(bffim);
					double imageSkewAngle = id.getSkewAngle();
					// determine skew angle
					if ((imageSkewAngle > MINIMUM_DESKEW_THRESHOLD || imageSkewAngle < -(MINIMUM_DESKEW_THRESHOLD))) {
						bffim = ImageHelper.rotateImage(bffim, -imageSkewAngle); // deskew image
					}

					grayscale = toGray(bffim);
					binarized = binarize(grayscale);

					// to keep file for our reference
				//	ImageIO.write(binarized, "png", outputFile);
				//	System.out.println("Image read successfully");

					content = instance.doOCR(binarized);
					

				}
			        doc.close();
					
			} //pdf condition ends
			else if(fileType.equalsIgnoreCase(".jpg") || fileType.equalsIgnoreCase(".png") || fileType.equalsIgnoreCase(".tiff")) {
				
						
						BufferedImage image = null;
						InputStream myInputStream = new ByteArrayInputStream(document.getDocumentContent());
						
						//image=ImageIO.read
						image = ImageIO.read(myInputStream);

						grayscale = toGray(image);
						binarized = binarize(grayscale);

						// to keep file for our reference
						//ImageIO.write(binarized, "png", outputFile);
						//System.out.println("Image read successfully");

						 content = instance.doOCR(binarized);

						System.out.println("imgText1======>" + content);
				
			}
		
			
		} catch (IOException e) {
			e.printStackTrace();
		}catch (TesseractException e) {
			e.printStackTrace();
		} 
		finally {
			if (convFile.exists()) {
				convFile.delete();
			}
		}

		 return ocrExtractionDto;
	}
	
	
	
	private OcrExtractionDto scanDocInfo(String scannedText,Long docId) {

		System.out.println("scannedText" + scannedText);

		OcrExtractionDto ocrExtractionDto = new OcrExtractionDto();
		// Get list of keywords from document keywords table

		// invoice
		String docType = "invoice";
		List<DocumentTypeKeyword> documentTypeKeyword = documentTypeKeyWordRepository.findByDocumentType(docType);

		// for sending purpose
		List<OcrExtractionDto> extractedValues = new ArrayList<OcrExtractionDto>();
		for (DocumentTypeKeyword documentObj : documentTypeKeyword) {
			
			
			System.out.println("temp.getKeyword()====>" + documentObj.getKeyword().getId());

			long keywordId = documentObj.getKeyword().getId();
			System.out.println("keywordId=>" + keywordId);

			Keyword keyword = keywordsRepository.findById(keywordId);
			String keywordName = keyword.getKeyword();
			System.out.println("keywordName=>" + keywordName);

			List<Alias> aliasObj = aliasRepository.findBykeyword_id(keywordId);

			ArrayList<String> allKeywordsSearch = new ArrayList<String>();
			allKeywordsSearch.add(keywordName);

			for (Alias aliasIter : aliasObj) {
				//System.out.println("aliasIter==>" + aliasIter.getAliasName());
				allKeywordsSearch.add(aliasIter.getAliasName());
			}

			String match = "";
			String keywords = "";
			Iterator<String> itr = allKeywordsSearch.iterator();
			while (itr.hasNext()) {
				keywords = (String) itr.next();
				Matcher m = Pattern.compile(Pattern.quote(keywords) + "(.*?)" + Pattern.quote("\n"))
						.matcher(scannedText); // certificate no

				while (m.find()) {
					match = m.group(1);
					break;
				}
				if (match != null && !match.isEmpty()) {
					break;
				} 

			}
			
			if(match != null && !match.isEmpty()) {
			OcrExtractionDto ocrObj = new OcrExtractionDto();
			ocrObj.setId(keywordId);
			//ocrObj.setKeyword(match);
			ocrObj.setKeywordName(keywordName);
			ocrObj.setValues(match);
			extractedValues.add(ocrObj);
			}

		} //for loop ends

		ocrExtractionDto.setSentList(extractedValues);		
		
		  for(OcrExtractionDto innerOcrExtractionDto :extractedValues) {
			Integer pageCount = 3;
			Document document = documentRepository.findById(innerOcrExtractionDto.getId());
			System.out.println("Insertion part for Invoice Info");
			float confidencePercent = 88.0f;
			long docid = docId;
			InvoiceInfo invoiceInfo = new InvoiceInfo();
			invoiceInfo.setConfidenceScore(confidencePercent);
			Document doc = new Document();
			doc.setId(docid);
			invoiceInfo.setDocument(doc);
			invoiceInfo.setPageCount(pageCount);

			// InvoiceInfo invoiceInfodetails=invoiceInfoRepository.save(invoiceInfo);
			InvoiceInfo invoiceInfoObj = invoiceInfoRepository.save(invoiceInfo);
			Long invoiceId = invoiceInfoObj.getId();
		  
		 
		  //Storing Into Invoice Info Header Table
			long keywordId = innerOcrExtractionDto.getId();
			String extractedval = innerOcrExtractionDto.getValues();
			InvoiceHeaderInfo invoiceHeaderInfo = new InvoiceHeaderInfo();
			invoiceHeaderInfo.setExtractedValue(extractedval);
			Keyword key = new Keyword();
			key.setId(keywordId);
			invoiceHeaderInfo.setKeyword(key);

			InvoiceInfo info = new InvoiceInfo();
			info.setId(invoiceId);
			invoiceHeaderInfo.setInvoiceInfo(info);
			invoiceHeaderInfoRepository.save(invoiceHeaderInfo);
		  
		  }
		return ocrExtractionDto;
	}

	@Override
	public Document documentDetails(Long id) {
		// TODO Auto-generated method stub
		Document document = null;
		if (id != null)
			document = documentRepository.findById(id);

		return document;
	}

	@Override
	public List<DisplayExtractedOcrDto> Extraction(Long id) {

		
		System.out.println("Extraction==id==>"+id);
		DisplayExtractedOcrDto displayExtractedOcrDto = new DisplayExtractedOcrDto();

		List<InvoiceHeaderInfo> dataRetrive = new ArrayList<InvoiceHeaderInfo>();
		InvoiceHeaderInfo invoiceHeaderInfo;

		Document document = documentRepository.findById(id);

		List<InvoiceInfo> invoiceInfo = invoiceInfoRepository.findByDocument(document);

		System.out.println("invoiceInfo====>" + invoiceInfo);

		for (InvoiceInfo InvoiceObjtemp : invoiceInfo) {

			invoiceHeaderInfo = invoiceHeaderInfoRepository.findByInvoiceInfo(InvoiceObjtemp);
			dataRetrive.add(invoiceHeaderInfo);

		}

		List<DisplayExtractedOcrDto> invoiceheaderdto = invoiceheaderToInvoiceheaderDto(dataRetrive);
		return invoiceheaderdto;
	}
	
	
	public List<DisplayExtractedOcrDto> invoiceheaderToInvoiceheaderDto(List<InvoiceHeaderInfo> keywordslist) {

		List<DisplayExtractedOcrDto> invoiceHeaderInfoList = new ArrayList<DisplayExtractedOcrDto>();
		for (InvoiceHeaderInfo invoiceObj : keywordslist) {
			DisplayExtractedOcrDto displayExtractedOcrDto= new DisplayExtractedOcrDto();
			
			//Keyword keyword=keywordsRepository.findById(invoiceObj.getId());
			//Keyword keyword=keywordsRepository.findByKeyword(invoiceObj.getId().toString());
			String keywordName=invoiceObj.getKeyword().getKeyword();
			
			displayExtractedOcrDto.setKey(keywordName);
			displayExtractedOcrDto.setValues(invoiceObj.getExtractedValue());
			invoiceHeaderInfoList.add(displayExtractedOcrDto);
		}
		
		return invoiceHeaderInfoList;

	}

	@Override
	public byte[] byteArrayDocument(Long id) {
		Document document= documentRepository.findById(id);
		byte[] docValue=document.getDocumentContent();
		
		if(docValue!=null)
			System.out.println("not empty");
		else
			System.out.println("empty");
		
		return docValue;
	}

	@Override
	public String filepath(Long id) {
		String filePath=null;
		if(id!=null) {
			
			Document document= documentRepository.findById(id);
			filePath=document.getFilePath();
			
		}
		return filePath;
	}
	
	
	
	
	
	 private static BufferedImage toGray(BufferedImage original) {

			int alpha, red, green, blue;
			int newPixel;

			BufferedImage lum = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

			for (int i = 0; i < original.getWidth(); i++) {
				for (int j = 0; j < original.getHeight(); j++) {

					// Get pixels by R, G, B
					alpha = new Color(original.getRGB(i, j)).getAlpha();
					red = new Color(original.getRGB(i, j)).getRed();
					green = new Color(original.getRGB(i, j)).getGreen();
					blue = new Color(original.getRGB(i, j)).getBlue();

					red = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
					// Return back to original format
					newPixel = colorToRGB(alpha, red, red, red);

					// Write pixels into image
					lum.setRGB(i, j, newPixel);

				}
			}

			return lum;

		}
	  
	  
	  
	  private static int colorToRGB(int alpha, int red, int green, int blue) {

			int newPixel = 0;
			newPixel += alpha;
			newPixel = newPixel << 8;
			newPixel += red;
			newPixel = newPixel << 8;
			newPixel += green;
			newPixel = newPixel << 8;
			newPixel += blue;

			return newPixel;

		}
	  
	  
	  
	  
	  private static BufferedImage binarize(BufferedImage original) {

			int red;
			int newPixel;

			int threshold = otsuTreshold(original);

			BufferedImage binarized = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

			for (int i = 0; i < original.getWidth(); i++) {
				for (int j = 0; j < original.getHeight(); j++) {

					// Get pixels
					red = new Color(original.getRGB(i, j)).getRed();
					int alpha = new Color(original.getRGB(i, j)).getAlpha();
					if (red > threshold) {
						newPixel = 255;
					} else {
						newPixel = 0;
					}
					newPixel = colorToRGB(alpha, newPixel, newPixel, newPixel);
					binarized.setRGB(i, j, newPixel);

				}
			}

			return binarized;

		}
	  
	  
	  
	  private static int otsuTreshold(BufferedImage original) {

			int[] histogram = imageHistogram(original);
			int total = original.getHeight() * original.getWidth();

			float sum = 0;
			for (int i = 0; i < 256; i++)
				sum += i * histogram[i];

			float sumB = 0;
			int wB = 0;
			int wF = 0;

			float varMax = 0;
			int threshold = 0;

			for (int i = 0; i < 256; i++) {
				wB += histogram[i];
				if (wB == 0)
					continue;
				wF = total - wB;

				if (wF == 0)
					break;

				sumB += (float) (i * histogram[i]);
				float mB = sumB / wB;
				float mF = (sum - sumB) / wF;

				float varBetween = (float) wB * (float) wF * (mB - mF) * (mB - mF);

				if (varBetween > varMax) {
					varMax = varBetween;
					threshold = i;
				}
			}

			return threshold;

		}
	  
	  
	  
	  public static int[] imageHistogram(BufferedImage input) {

			int[] histogram = new int[256];

			for (int i = 0; i < histogram.length; i++)
				histogram[i] = 0;

			for (int i = 0; i < input.getWidth(); i++) {
				for (int j = 0; j < input.getHeight(); j++) {
					int red = new Color(input.getRGB(i, j)).getRed();
					histogram[red]++;
				}
			}

			return histogram;

		}



	@Override
	public ViewerDto documentIdtoByteArr(Long docId) {
		
		System.out.println("Inside ServiceImpl");
		ViewerDto viewrDto=new ViewerDto();		
		Document document=new Document();
		document=documentRepository.getOne(docId);
		
		
		if(document!=null) {
		viewrDto.setFileExtension(document.getDocumentType());
		viewrDto.setDocumentName("myselfSetting");
		byte[] decodedBytes = document.getDocumentContent();
		viewrDto.setFileArray(decodedBytes);
		return viewrDto;
		}
		
		return null;
	}



	@Override
	public ViewerDto documentIdtoString(Long docId) {
		ViewerDto viewrDto = new ViewerDto();
		Document document = new Document();

		try {

			document = documentRepository.getOne(docId);
			String content = "";
			System.out.println("Inside document type doc or docx");
			Tika tika = new Tika();

			byte[] documentByte = null;
			documentByte = document.getDocumentContent();
			InputStream myInputStream = new ByteArrayInputStream(documentByte);

			content = tika.parseToString(myInputStream);
			// System.out.println("content====>" + content);

			if (content != null)
				viewrDto.setWholeString(content);
			else {
				System.out.println("content is null");
				viewrDto.setWholeString(null);
			}
			System.out.println("CreateD SuccessFully=====>");

		} catch (Exception e) {
			viewrDto.setWholeString("");
			System.out.println("Error Caught-->rrr");
			e.printStackTrace();

		}
		return viewrDto;
	}

	@Override
	public ViewerDto documentToPdf(Long docId) {

		ViewerDto viewrDto = new ViewerDto();
		Document document = new Document();
		document = documentRepository.getOne(docId);
		System.out.println("yes changes done");

		String fileType = document.getDocumentType();
		
		if(document!=null) {
			
			if (fileType.equalsIgnoreCase("xls")) {
				viewrDto.setFileExtension("pdf");
				viewrDto.setDocumentName("myselfSetting");
				byte[] decodedBytes = document.getDocumentContent();

				FileInputStream input_document = null;
				try {
					ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedBytes);
					HSSFWorkbook xlsWorkbook = new HSSFWorkbook(byteArrayInputStream);
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					com.itextpdf.text.Document iText_xls_2_pdf = new com.itextpdf.text.Document();
					PdfWriter.getInstance(iText_xls_2_pdf, byteArrayOutputStream);
					iText_xls_2_pdf.open();

					int totalPages = xlsWorkbook.getNumberOfSheets();
					System.out.println("count===>" + totalPages);

					for (int i = 0; i < totalPages; i++) {

						System.out.println("my_xls_workbook.getSheetName(i)===>" + xlsWorkbook.getSheetName(i));
						HSSFSheet my_worksheet = xlsWorkbook.getSheetAt(i);
						Iterator<Row> rowIterator = my_worksheet.iterator();
						while (rowIterator.hasNext()) {
							Row row = rowIterator.next();
							Iterator<Cell> cellIterator = row.cellIterator();
							String lineByline = "";
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next(); // Fetch CELL
								switch (cell.getCellType()) {

								case Cell.CELL_TYPE_NUMERIC:
									System.out.println("NUMERIC===>");
									Double doubleConversion = cell.getNumericCellValue();
									int num = doubleConversion.intValue();
									lineByline += num + " ";
									break;
								case Cell.CELL_TYPE_STRING:
									lineByline += cell.getStringCellValue() + " ";
									break;
								}

							}
							iText_xls_2_pdf.add(new Paragraph(lineByline));
							iText_xls_2_pdf.add(new Paragraph("\n"));
						}

					}

					iText_xls_2_pdf.close();
					byte[] encoded = byteArrayOutputStream.toByteArray();
					viewrDto.setFileArray(encoded);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (DocumentException e) {
					e.printStackTrace();
				}

				return viewrDto;
			}//document type if end
			if (fileType.equalsIgnoreCase("xlsx")) {
				viewrDto.setFileExtension("pdf");
				byte[] decodedBytes = document.getDocumentContent();

				FileInputStream input_document = null;
				try {
					ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedBytes);
					XSSFWorkbook xlsWorkbook = new XSSFWorkbook(byteArrayInputStream);
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

					com.itextpdf.text.Document iText_xls_2_pdf = new com.itextpdf.text.Document();
					PdfWriter.getInstance(iText_xls_2_pdf, byteArrayOutputStream);
					iText_xls_2_pdf.open();

					int totalPages = xlsWorkbook.getNumberOfSheets();
					System.out.println("count===>" + totalPages);

					for (int i = 0; i < totalPages; i++) {

						XSSFSheet my_worksheet = xlsWorkbook.getSheetAt(i);
						Iterator<Row> rowIterator = my_worksheet.iterator();

						while (rowIterator.hasNext()) {
							Row row = rowIterator.next();
							Iterator<Cell> cellIterator = row.cellIterator();
							String lineByline = "";
							while (cellIterator.hasNext()) {
								Cell cell = cellIterator.next(); // Fetch CELL
								switch (cell.getCellType()) {

								case Cell.CELL_TYPE_NUMERIC:
									Double doubleConversion = cell.getNumericCellValue();
									int num = doubleConversion.intValue();
									lineByline += num + " ";

									break;
								case Cell.CELL_TYPE_STRING:
									lineByline += cell.getStringCellValue() + " ";
									break;
								}
								// next line

							}
							iText_xls_2_pdf.add(new Paragraph(lineByline));
							iText_xls_2_pdf.add(new Paragraph("\n"));
						}

					}
					iText_xls_2_pdf.close();

					byte[] encoded = byteArrayOutputStream.toByteArray();
					viewrDto.setFileArray(encoded);

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (DocumentException e) {
					e.printStackTrace();
				}
				return viewrDto;
			}
			if (fileType.equalsIgnoreCase("doc") || fileType.equalsIgnoreCase("docx")) {

				byte[] arrCntnt = document.getDocumentContent();
				InputStream targetStream = new ByteArrayInputStream(arrCntnt);

				String k = null;
				try {

					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

					if (fileType.equalsIgnoreCase("doc")) {
						System.out.println("DOC FILE");
						HWPFDocument doc = new HWPFDocument(targetStream);
						WordExtractor we = new WordExtractor(doc);
						k = we.getText();
					}
					if (fileType.equalsIgnoreCase("docx")) {
						System.out.println("DOCX FILE");
						XWPFDocument doc = new XWPFDocument(targetStream);
						XWPFWordExtractor we = new XWPFWordExtractor(doc);
						k = we.getText();

					}

					com.itextpdf.text.Document docs = new com.itextpdf.text.Document();
					PdfWriter.getInstance(docs, byteArrayOutputStream);
					docs.open();
					docs.add(new Paragraph(k));
					docs.close();

					byte[] encoded = byteArrayOutputStream.toByteArray();
					viewrDto.setFileArray(encoded);
					return viewrDto;

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			
		
	} //document != null end
		
		
		return null;
	
	}
	
	
	

}
