package com.digidoc.ocr.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.dataaccess.model.DocumentTypeKeyword;
import com.digidoc.dataaccess.model.ZonalTemplateOcr;
import com.digidoc.dataaccess.repository.DocumentTypeKeyWordRepository;
import com.digidoc.dataaccess.repository.ZonalTemplateOcrRepository;
import com.digidoc.ocr.dto.DocumentTypeKeywordDto;
import com.digidoc.ocr.dto.ZonalTemplateOcrDto;
import com.digidoc.ocr.service.ZonalTemplateOcrService;

@Service
public class ZonalTemplateOcrServiceImpl implements ZonalTemplateOcrService{
	
	@Autowired
	ZonalTemplateOcrRepository zonalTemplateOcrRepository;
	
	@Autowired
	DocumentTypeKeyWordRepository documentTypeKeyWordRepository;

	@Override
	public Boolean saveZonalTemplateOcr(List<ZonalTemplateOcrDto> zonalTemplateOcrDtoList) {
		if(zonalTemplateOcrDtoList != null) {
			for(ZonalTemplateOcrDto zonalTemplateOcrDto:zonalTemplateOcrDtoList) {
				ZonalTemplateOcr zonalTemplateOcr = new ZonalTemplateOcr();
				zonalTemplateOcr.setHeight(zonalTemplateOcrDto.getHeight());
				zonalTemplateOcr.setWidth(zonalTemplateOcrDto.getWidth());
				zonalTemplateOcr.setX(zonalTemplateOcrDto.getX());
				zonalTemplateOcr.setY(zonalTemplateOcrDto.getY());
				zonalTemplateOcr.setKeywordId(zonalTemplateOcrDto.getKeywordId());
				zonalTemplateOcr.setSupplierName(zonalTemplateOcrDto.getSupplierName());
				zonalTemplateOcrRepository.save(zonalTemplateOcr);
			}
			return true;
			
		}else {
			return false;
		}
		
	}

	@Override
	public List<DocumentTypeKeywordDto> getDocTypeList() {
		List<DocumentTypeKeyword> listDocumentTypeKeyword = documentTypeKeyWordRepository.getListKeywordsWithDocType();
		if(listDocumentTypeKeyword.size()>0) {
			List<DocumentTypeKeywordDto> docTypeKeywordDtoList = new ArrayList<DocumentTypeKeywordDto>();
			for(DocumentTypeKeyword documentTypeKeyword:listDocumentTypeKeyword) {
				DocumentTypeKeywordDto documentTypeKeywordDto = new DocumentTypeKeywordDto();
				documentTypeKeywordDto.setId(documentTypeKeyword.getId());
				documentTypeKeywordDto.setDocumentType(documentTypeKeyword.getDocumentType());
				//documentTypeKeywordDto.setKeyword(documentTypeKeyword.getKeyword());
				docTypeKeywordDtoList.add(documentTypeKeywordDto);
			}
			return docTypeKeywordDtoList;
		}else {
			return null;
		}
		
	}

	@Override
	public List<DocumentTypeKeywordDto> getKeywordsByDocType(String docType) {
		List<DocumentTypeKeyword> listDocumentTypeKeyword = documentTypeKeyWordRepository.findByDocumentType(docType);
		if(listDocumentTypeKeyword.size()>0) {
			List<DocumentTypeKeywordDto> docTypeKeywordDtoList = new ArrayList<DocumentTypeKeywordDto>();
			for(DocumentTypeKeyword documentTypeKeyword:listDocumentTypeKeyword) {
				DocumentTypeKeywordDto documentTypeKeywordDto = new DocumentTypeKeywordDto();
				documentTypeKeywordDto.setId(documentTypeKeyword.getId());
				documentTypeKeywordDto.setDocumentType(documentTypeKeyword.getDocumentType());
				documentTypeKeywordDto.setKeyword(documentTypeKeyword.getKeyword().getKeyword());
				documentTypeKeywordDto.setKeywordId(documentTypeKeyword.getKeyword().getId());
				docTypeKeywordDtoList.add(documentTypeKeywordDto);
			}
			return docTypeKeywordDtoList;
		}else {
			return null;
		}
	}

}
