package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the metadata_tag_values database table.
 * 
 */
@Entity
@Table(name="metadata_colour")
@NamedQuery(name="MetaColour.findAll", query="SELECT m FROM MetaColour m")
public class MetaColour implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	
	@Column(name="colur_value")
	private String colurValue;

	
	//bi-directional many-to-one association to MetadataTag
	@ManyToOne
	@JoinColumn(name="metadata_tag_id")
	private MetadataTag metadataTagColur;
	
	
	@OneToOne
	@JoinColumn(name="metadata_value_id")
	private MetadataTagValue metadataTagValue;

	public MetaColour() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColurValue() {
		return colurValue;
	}

	public void setColurValue(String colurValue) {
		this.colurValue = colurValue;
	}

	public MetadataTag getMetadataTagColur() {
		return metadataTagColur;
	}

	public void setMetadataTagColur(MetadataTag metadataTagColur) {
		this.metadataTagColur = metadataTagColur;
	}

	public MetadataTagValue getMetadataTagValue() {
		return metadataTagValue;
	}

	public void setMetadataTagValue(MetadataTagValue metadataTagValue) {
		this.metadataTagValue = metadataTagValue;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	}