package com.digidoc.dataaccess.model;


import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the document_type_keywrods database table.
 * 
 */
@Entity
@Table(name="document_type_keywords")
@NamedQuery(name="DocumentTypeKeyword.findAll", query="SELECT d FROM DocumentTypeKeyword d")
public class DocumentTypeKeyword implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="document_type")
	private String documentType;

	//bi-directional many-to-one association to Keyword
	@ManyToOne
	@JoinColumn(name="keyword_id")
	private Keyword keyword;

	public DocumentTypeKeyword() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public Keyword getKeyword() {
		return this.keyword;
	}

	public void setKeyword(Keyword keyword) {
		this.keyword = keyword;
	}

}