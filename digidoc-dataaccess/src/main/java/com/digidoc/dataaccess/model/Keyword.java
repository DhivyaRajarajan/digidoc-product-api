package com.digidoc.dataaccess.model;

import java.io.Serializable;


import java.util.List;

import javax.persistence.*;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter

/**
 * The persistent class for the keywords database table.
 * 
 */
@Entity
@Table(name="keywords")
@NamedQuery(name="Keyword.findAll", query="SELECT k FROM Keyword k")
public class Keyword implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="keyword")
	private String keyword;
	
	@OneToMany(mappedBy="keyword")
	private List<DocumentTypeKeyword> documentTypeKeyword;
	

	public List<DocumentTypeKeyword> getDocumentTypeKeyword() {
		return documentTypeKeyword;
	}

	public void setDocumentTypeKeyword(List<DocumentTypeKeyword> documentTypeKeyword) {
		this.documentTypeKeyword = documentTypeKeyword;
	}

	public Keyword() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	

}