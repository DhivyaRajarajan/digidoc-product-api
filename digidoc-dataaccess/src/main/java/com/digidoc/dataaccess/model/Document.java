package com.digidoc.dataaccess.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


/**
 * The persistent class for the documents database table.
 * 
 */

@Entity
@Table(name="documents")
@NamedQuery(name="Document.findAll", query="SELECT d FROM Document d")
public class Document implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Lob
	@Column(name="document_content")
	private byte[] documentContent;

	@Column(name="document_name")
	private String documentName;
	
	@Column(name="original_document_name")
	private String originalDocumentName;

	@Column(name="document_type")
	private String documentType;

	@Column(name="document_unique_id")
	private Long documentUniqueId;

	@Column(name="page_count")
	private Long pageCount;

	@Column(name="parent_doc_id")
	private Long parentDocId;
	
	@Column(name="file_path")
	private String filePath;
	
	@Column(name="checkout_status")
	private boolean checkoutStatus;
	
	@Column(name="checkout_by")
	private Long checkoutBy;

	
	
	public Long getCheckoutBy() {
		return checkoutBy;
	}

	public void setCheckoutBy(Long checkoutBy) {
		this.checkoutBy = checkoutBy;
	}

	public boolean isCheckoutStatus() {
		return checkoutStatus;
	}

	public void setCheckoutStatus(boolean checkoutStatus) {
		this.checkoutStatus = checkoutStatus;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	//bi-directional many-to-one association to DocumentLabel
	@OneToMany(mappedBy="document")
	private List<DocumentLabel> documentLabels;

	//bi-directional many-to-one association to DocumentMetadataTagInfo
	@OneToMany(mappedBy="document")
	private List<DocumentMetadataTagInfo> documentMetadataTagInfos;

	//bi-directional many-to-one association to Folder
	@ManyToOne
	private Folder folder;

	//bi-directional many-to-one association to User
	@ManyToOne
	private User user;

	//bi-directional many-to-one association to InvoiceInfo
	@OneToMany(mappedBy="document")
	private List<InvoiceInfo> invoiceInfos;
	
	@NotNull
	private Long version;
	
	
	
	
	
	/*@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
    protected Date creationDate;*/
	
	/*@Column(name="is_check_out")
	private Boolean isCheckOut;*/
	
	public String getOriginalDocumentName() {
		return originalDocumentName;
	}

	public void setOriginalDocumentName(String originalDocumentName) {
		this.originalDocumentName = originalDocumentName;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Document() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDocumentUniqueId() {
		return documentUniqueId;
	}

	public void setDocumentUniqueId(Long documentUniqueId) {
		this.documentUniqueId = documentUniqueId;
	}

	public Long getPageCount() {
		return pageCount;
	}

	public void setPageCount(Long pageCount) {
		this.pageCount = pageCount;
	}

	public Long getParentDocId() {
		return parentDocId;
	}



	public void setParentDocId(Long parentDocId) {
		this.parentDocId = parentDocId;
	}

	public byte[] getDocumentContent() {
		return this.documentContent;
	}

	public void setDocumentContent(byte[] documentContent) {
		this.documentContent = documentContent;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public List<DocumentLabel> getDocumentLabels() {
		return this.documentLabels;
	}

	public void setDocumentLabels(List<DocumentLabel> documentLabels) {
		this.documentLabels = documentLabels;
	}

	public DocumentLabel addDocumentLabel(DocumentLabel documentLabel) {
		getDocumentLabels().add(documentLabel);
		documentLabel.setDocument(this);

		return documentLabel;
	}

	public DocumentLabel removeDocumentLabel(DocumentLabel documentLabel) {
		getDocumentLabels().remove(documentLabel);
		documentLabel.setDocument(null);

		return documentLabel;
	}

	public List<DocumentMetadataTagInfo> getDocumentMetadataTagInfos() {
		return this.documentMetadataTagInfos;
	}

	public void setDocumentMetadataTagInfos(List<DocumentMetadataTagInfo> documentMetadataTagInfos) {
		this.documentMetadataTagInfos = documentMetadataTagInfos;
	}

	public DocumentMetadataTagInfo addDocumentMetadataTagInfo(DocumentMetadataTagInfo documentMetadataTagInfo) {
		getDocumentMetadataTagInfos().add(documentMetadataTagInfo);
		documentMetadataTagInfo.setDocument(this);

		return documentMetadataTagInfo;
	}

	public DocumentMetadataTagInfo removeDocumentMetadataTagInfo(DocumentMetadataTagInfo documentMetadataTagInfo) {
		getDocumentMetadataTagInfos().remove(documentMetadataTagInfo);
		documentMetadataTagInfo.setDocument(null);

		return documentMetadataTagInfo;
	}

	public Folder getFolder() {
		return this.folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<InvoiceInfo> getInvoiceInfos() {
		return this.invoiceInfos;
	}

	public void setInvoiceInfos(List<InvoiceInfo> invoiceInfos) {
		this.invoiceInfos = invoiceInfos;
	}

	public InvoiceInfo addInvoiceInfo(InvoiceInfo invoiceInfo) {
		getInvoiceInfos().add(invoiceInfo);
		invoiceInfo.setDocument(this);

		return invoiceInfo;
	}

	public InvoiceInfo removeInvoiceInfo(InvoiceInfo invoiceInfo) {
		getInvoiceInfos().remove(invoiceInfo);
		invoiceInfo.setDocument(null);

		return invoiceInfo;
	}

	/*public Boolean getIsCheckOut() {
		return isCheckOut;
	}

	public void setIsCheckOut(Boolean isCheckOut) {
		this.isCheckOut = isCheckOut;
	}*/

}