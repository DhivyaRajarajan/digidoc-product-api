package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the metadata_tag database table.
 * 
 */
@Entity
@Table(name="metadata_tag")
@NamedQuery(name="MetadataTag.findAll", query="SELECT m FROM MetadataTag m")
public class MetadataTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="is_free_text")
	private boolean isFreeText;

	@Column(name="is_mandatory")
	private boolean isMandatory;

	@Column(name="is_radio")
	private boolean isRadio;

	@Column(name="is_select")
	private boolean isSelect;

	@Column(name="is_visible")
	private boolean isVisible;
	
	@Column(name="colour_coding")
	private boolean colourCoding;

	private String name;

	//bi-directional many-to-one association to DocumentMetadataTagInfo
	@OneToMany(mappedBy="metadataTag")
	private List<DocumentMetadataTagInfo> documentMetadataTagInfos;

	//bi-directional many-to-one association to MetadataTagValue
	@OneToMany(mappedBy="metadataTagColur")
	private List<MetaColour> metadatatagColur;
	
	
	@OneToMany(mappedBy="metadataTag")
	private List<MetadataTagValue> metadataTagValues;
	
	

	

	

	public List<MetaColour> getMetadatatagColur() {
		return metadatatagColur;
	}

	public void setMetadatatagColur(List<MetaColour> metadatatagColur) {
		this.metadatatagColur = metadatatagColur;
	}

	public boolean isColourCoding() {
		return colourCoding;
	}

	public void setColourCoding(boolean colourCoding) {
		this.colourCoding = colourCoding;
	}

	public MetadataTag() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	
	public boolean isFreeText() {
		return isFreeText;
	}

	public void setFreeText(boolean isFreeText) {
		this.isFreeText = isFreeText;
	}

	public boolean isMandatory() {
		return isMandatory;
	}

	public void setMandatory(boolean isMandatory) {
		this.isMandatory = isMandatory;
	}

	public boolean isRadio() {
		return isRadio;
	}

	public void setRadio(boolean isRadio) {
		this.isRadio = isRadio;
	}

	public boolean isSelect() {
		return isSelect;
	}

	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<DocumentMetadataTagInfo> getDocumentMetadataTagInfos() {
		return this.documentMetadataTagInfos;
	}

	public void setDocumentMetadataTagInfos(List<DocumentMetadataTagInfo> documentMetadataTagInfos) {
		this.documentMetadataTagInfos = documentMetadataTagInfos;
	}

	public DocumentMetadataTagInfo addDocumentMetadataTagInfo(DocumentMetadataTagInfo documentMetadataTagInfo) {
		getDocumentMetadataTagInfos().add(documentMetadataTagInfo);
		documentMetadataTagInfo.setMetadataTag(this);

		return documentMetadataTagInfo;
	}

	public DocumentMetadataTagInfo removeDocumentMetadataTagInfo(DocumentMetadataTagInfo documentMetadataTagInfo) {
		getDocumentMetadataTagInfos().remove(documentMetadataTagInfo);
		documentMetadataTagInfo.setMetadataTag(null);

		return documentMetadataTagInfo;
	}

	public List<MetadataTagValue> getMetadataTagValues() {
		return this.metadataTagValues;
	}

	public void setMetadataTagValues(List<MetadataTagValue> metadataTagValues) {
		this.metadataTagValues = metadataTagValues;
	}

	public MetadataTagValue addMetadataTagValue(MetadataTagValue metadataTagValue) {
		getMetadataTagValues().add(metadataTagValue);
		metadataTagValue.setMetadataTag(this);

		return metadataTagValue;
	}

	public MetadataTagValue removeMetadataTagValue(MetadataTagValue metadataTagValue) {
		getMetadataTagValues().remove(metadataTagValue);
		metadataTagValue.setMetadataTag(null);

		return metadataTagValue;
	}

}