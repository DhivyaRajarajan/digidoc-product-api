package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the aliases database table.
 * 
 */
@Entity
@Table(name="aliases")
@NamedQuery(name="Alias.findAll", query="SELECT a FROM Alias a")
public class Alias implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="alias_name")
	private String aliasName;

	//bi-directional many-to-one association to Keyword
	@ManyToOne
	private Keyword keyword;

	public Alias() {
	}

	/*public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}*/

	public String getAliasName() {
		return this.aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public Keyword getKeyword() {
		return this.keyword;
	}

	public void setKeyword(Keyword keyword) {
		this.keyword = keyword;
	}

}