package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the document_labels database table.
 * 
 */
@Entity
@Table(name="document_labels")
@NamedQuery(name="DocumentLabel.findAll", query="SELECT d FROM DocumentLabel d")
public class DocumentLabel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	//bi-directional many-to-one association to Document
	@ManyToOne
	private Document document;

	//bi-directional many-to-one association to Label
	@ManyToOne
	private Label label;

	public DocumentLabel() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Label getLabel() {
		return this.label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

}