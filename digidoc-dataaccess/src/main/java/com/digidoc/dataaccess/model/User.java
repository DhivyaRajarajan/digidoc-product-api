package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private String email;

	@Column(name="mobile_no")
	private String mobileNo;

	private String password;

	@Column(name="user_name")
	private String userName;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;

	//bi-directional many-to-one association to Document
	@OneToMany(mappedBy="user")
	private List<Document> documents;

	//bi-directional many-to-one association to UserFavoriteSearch
	@OneToMany(mappedBy="user")
	private List<UserFavoriteSearch> userFavoriteSearches;

	//bi-directional many-to-one association to UserRolesInfo
	@OneToMany(mappedBy="user")
	private List<UserRolesInfo> userRolesInfos;

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public User() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public List<Document> getDocuments() {
		return this.documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public Document addDocument(Document document) {
		getDocuments().add(document);
		document.setUser(this);

		return document;
	}

	public Document removeDocument(Document document) {
		getDocuments().remove(document);
		document.setUser(null);

		return document;
	}

	public List<UserFavoriteSearch> getUserFavoriteSearches() {
		return this.userFavoriteSearches;
	}

	public void setUserFavoriteSearches(List<UserFavoriteSearch> userFavoriteSearches) {
		this.userFavoriteSearches = userFavoriteSearches;
	}

	public UserFavoriteSearch addUserFavoriteSearch(UserFavoriteSearch userFavoriteSearch) {
		getUserFavoriteSearches().add(userFavoriteSearch);
		userFavoriteSearch.setUser(this);

		return userFavoriteSearch;
	}

	public UserFavoriteSearch removeUserFavoriteSearch(UserFavoriteSearch userFavoriteSearch) {
		getUserFavoriteSearches().remove(userFavoriteSearch);
		userFavoriteSearch.setUser(null);

		return userFavoriteSearch;
	}

	public List<UserRolesInfo> getUserRolesInfos() {
		return this.userRolesInfos;
	}

	public void setUserRolesInfos(List<UserRolesInfo> userRolesInfos) {
		this.userRolesInfos = userRolesInfos;
	}

	public UserRolesInfo addUserRolesInfo(UserRolesInfo userRolesInfo) {
		getUserRolesInfos().add(userRolesInfo);
		userRolesInfo.setUser(this);

		return userRolesInfo;
	}

	public UserRolesInfo removeUserRolesInfo(UserRolesInfo userRolesInfo) {
		getUserRolesInfos().remove(userRolesInfo);
		userRolesInfo.setUser(null);

		return userRolesInfo;
	}

}