package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the metadata_tag_values database table.
 * 
 */
@Entity
@Table(name="network_share")
@NamedQuery(name="NetworkShare.findAll", query="SELECT m FROM NetworkShare m")
public class NetworkShare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	
	@Column(name="network_path")
	private String networkPath;
	
	@Column(name="user_name")
	private String userName;

	@Column(name="password")
	private String Password;

	@Column(name="poll_time")
	private String pollTime;

	
	//bi-directional many-to-one association to MetadataTag
	@ManyToOne
	@JoinColumn(name="folder_id")
	private Folder folder;
	
	
	

		public NetworkShare() {
	}


		public Long getId() {
			return id;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getNetworkPath() {
			return networkPath;
		}


		public void setNetworkPath(String networkPath) {
			this.networkPath = networkPath;
		}


		public String getUserName() {
			return userName;
		}


		public void setUserName(String userName) {
			this.userName = userName;
		}


		public String getPassword() {
			return Password;
		}


		public void setPassword(String password) {
			Password = password;
		}


		public String getPollTime() {
			return pollTime;
		}


		public void setPollTime(String pollTime) {
			this.pollTime = pollTime;
		}


		public Folder getFolder() {
			return folder;
		}


		public void setFolder(Folder folder) {
			this.folder = folder;
		}


		public static long getSerialversionuid() {
			return serialVersionUID;
		}
		
		
		

		}