package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the role database table.
 * 
 */
@Entity
@NamedQuery(name="Role.findAll", query="SELECT r FROM Role r")
public class Role implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="role_name")
	private String roleName;

	//bi-directional many-to-one association to RolePermissionInfo
	@OneToMany(mappedBy="role")
	private List<RolePermissionInfo> rolePermissionInfos;

	//bi-directional many-to-one association to UserRolesInfo
	@OneToMany(mappedBy="role")
	private List<UserRolesInfo> userRolesInfos;

	public Role() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<RolePermissionInfo> getRolePermissionInfos() {
		return this.rolePermissionInfos;
	}

	public void setRolePermissionInfos(List<RolePermissionInfo> rolePermissionInfos) {
		this.rolePermissionInfos = rolePermissionInfos;
	}

	public RolePermissionInfo addRolePermissionInfo(RolePermissionInfo rolePermissionInfo) {
		getRolePermissionInfos().add(rolePermissionInfo);
		rolePermissionInfo.setRole(this);

		return rolePermissionInfo;
	}

	public RolePermissionInfo removeRolePermissionInfo(RolePermissionInfo rolePermissionInfo) {
		getRolePermissionInfos().remove(rolePermissionInfo);
		rolePermissionInfo.setRole(null);

		return rolePermissionInfo;
	}

	public List<UserRolesInfo> getUserRolesInfos() {
		return this.userRolesInfos;
	}

	public void setUserRolesInfos(List<UserRolesInfo> userRolesInfos) {
		this.userRolesInfos = userRolesInfos;
	}

	public UserRolesInfo addUserRolesInfo(UserRolesInfo userRolesInfo) {
		getUserRolesInfos().add(userRolesInfo);
		userRolesInfo.setRole(this);

		return userRolesInfo;
	}

	public UserRolesInfo removeUserRolesInfo(UserRolesInfo userRolesInfo) {
		getUserRolesInfos().remove(userRolesInfo);
		userRolesInfo.setRole(null);

		return userRolesInfo;
	}

}