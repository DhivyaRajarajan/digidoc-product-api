package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the labels database table.
 * 
 */
@Entity
@Table(name="labels")
@NamedQuery(name="Label.findAll", query="SELECT l FROM Label l")
public class Label implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="label_name")
	private String labelName;

	//bi-directional many-to-one association to DocumentLabel
	@OneToMany(mappedBy="label")
	private List<DocumentLabel> documentLabels;

	public Label() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLabelName() {
		return this.labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public List<DocumentLabel> getDocumentLabels() {
		return this.documentLabels;
	}

	public void setDocumentLabels(List<DocumentLabel> documentLabels) {
		this.documentLabels = documentLabels;
	}

	public DocumentLabel addDocumentLabel(DocumentLabel documentLabel) {
		getDocumentLabels().add(documentLabel);
		documentLabel.setLabel(this);

		return documentLabel;
	}

	public DocumentLabel removeDocumentLabel(DocumentLabel documentLabel) {
		getDocumentLabels().remove(documentLabel);
		documentLabel.setLabel(null);

		return documentLabel;
	}

}