package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_roles_info database table.
 * 
 */
@Entity
@Table(name="user_roles_info")
@NamedQuery(name="UserRolesInfo.findAll", query="SELECT u FROM UserRolesInfo u")
public class UserRolesInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	//bi-directional many-to-one association to Role
	@ManyToOne
	@JoinColumn(name="role_id")
	private Role role;

	public UserRolesInfo() {
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

}