package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the document_metadata_tag_info database table.
 * 
 */
@Entity
@Table(name="document_metadata_tag_info")
@NamedQuery(name="DocumentMetadataTagInfo.findAll", query="SELECT d FROM DocumentMetadataTagInfo d")
public class DocumentMetadataTagInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String value;

	//bi-directional many-to-one association to Document
	@ManyToOne
	private Document document;

	//bi-directional many-to-one association to MetadataTag
	@ManyToOne
	@JoinColumn(name="metadata_tag_id")
	private MetadataTag metadataTag;

	//bi-directional many-to-one association to MetadataTagValue
	@ManyToOne
	@JoinColumn(name="metadata_tag_value_id")
	private MetadataTagValue metadataTagValue;

	public DocumentMetadataTagInfo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public MetadataTag getMetadataTag() {
		return this.metadataTag;
	}

	public void setMetadataTag(MetadataTag metadataTag) {
		this.metadataTag = metadataTag;
	}

	public MetadataTagValue getMetadataTagValue() {
		return this.metadataTagValue;
	}

	public void setMetadataTagValue(MetadataTagValue metadataTagValue) {
		this.metadataTagValue = metadataTagValue;
	}

}