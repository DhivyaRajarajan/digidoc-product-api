package com.digidoc.dataaccess.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;


/**
 * The persistent class for the invoice_info database table.
 * 
 */
@Entity
@Table(name="invoice_info")
@NamedQuery(name="InvoiceInfo.findAll", query="SELECT i FROM InvoiceInfo i")
public class InvoiceInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="confidence_score")
	private float confidenceScore;

	@Column(name="page_count")
	private int pageCount;

	//bi-directional many-to-one association to Document
	@ManyToOne
	private Document document;

	public InvoiceInfo() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public float getConfidenceScore() {
		return this.confidenceScore;
	}

	public void setConfidenceScore(float confidenceScore) {
		this.confidenceScore = confidenceScore;
	}

	public int getPageCount() {
		return this.pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public Document getDocument() {
		return this.document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}
	
	//myself
	@OneToMany(mappedBy="invoiceInfo")
	private List<InvoiceHeaderInfo> invoiceHeaderInfo;
	
	/*@Column(name="document_id")
	private Long docmentId;

	public Long getDocmentId() {
		return docmentId;
	}

	public void setDocmentId(Long docmentId) {
		this.docmentId = docmentId;
	}*/

}