package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the permission database table.
 * 
 */
@Entity
@NamedQuery(name="Permission.findAll", query="SELECT p FROM Permission p")
public class Permission implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	private String privelege;

	//bi-directional many-to-one association to RolePermissionInfo
	@OneToMany(mappedBy="permission")
	private List<RolePermissionInfo> rolePermissionInfos;

	public Permission() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrivelege() {
		return this.privelege;
	}

	public void setPrivelege(String privelege) {
		this.privelege = privelege;
	}

	public List<RolePermissionInfo> getRolePermissionInfos() {
		return this.rolePermissionInfos;
	}

	public void setRolePermissionInfos(List<RolePermissionInfo> rolePermissionInfos) {
		this.rolePermissionInfos = rolePermissionInfos;
	}

	public RolePermissionInfo addRolePermissionInfo(RolePermissionInfo rolePermissionInfo) {
		getRolePermissionInfos().add(rolePermissionInfo);
		rolePermissionInfo.setPermission(this);

		return rolePermissionInfo;
	}

	public RolePermissionInfo removeRolePermissionInfo(RolePermissionInfo rolePermissionInfo) {
		getRolePermissionInfos().remove(rolePermissionInfo);
		rolePermissionInfo.setPermission(null);

		return rolePermissionInfo;
	}

}