package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the metadata_tag_values database table.
 * 
 */
@Entity
@Table(name="metadata_tag_values")
@NamedQuery(name="MetadataTagValue.findAll", query="SELECT m FROM MetadataTagValue m")
public class MetadataTagValue implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="is_eligible_ocr")
	private Boolean isEligibleOcr;

	@Column(name="metadata_tag_value")
	private String metadataTagValue;

	//bi-directional many-to-one association to DocumentMetadataTagInfo
	@OneToMany(mappedBy="metadataTagValue")
	private List<DocumentMetadataTagInfo> documentMetadataTagInfos;
	
	@OneToOne(mappedBy="metadataTagValue")
	private MetaColour MetaColour;

	//bi-directional many-to-one association to MetadataTag
	@ManyToOne
	@JoinColumn(name="metadata_tag_id")
	private MetadataTag metadataTag;

	
	
	

	public MetaColour getMetaColour() {
		return MetaColour;
	}

	public void setMetaColour(MetaColour metaColour) {
		MetaColour = metaColour;
	}

	public MetadataTagValue() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

	public Boolean getIsEligibleOcr() {
		return isEligibleOcr;
	}

	public void setIsEligibleOcr(Boolean isEligibleOcr) {
		this.isEligibleOcr = isEligibleOcr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getMetadataTagValue() {
		return this.metadataTagValue;
	}

	public void setMetadataTagValue(String metadataTagValue) {
		this.metadataTagValue = metadataTagValue;
	}

	public List<DocumentMetadataTagInfo> getDocumentMetadataTagInfos() {
		return this.documentMetadataTagInfos;
	}

	public void setDocumentMetadataTagInfos(List<DocumentMetadataTagInfo> documentMetadataTagInfos) {
		this.documentMetadataTagInfos = documentMetadataTagInfos;
	}

	public DocumentMetadataTagInfo addDocumentMetadataTagInfo(DocumentMetadataTagInfo documentMetadataTagInfo) {
		getDocumentMetadataTagInfos().add(documentMetadataTagInfo);
		documentMetadataTagInfo.setMetadataTagValue(this);

		return documentMetadataTagInfo;
	}

	public DocumentMetadataTagInfo removeDocumentMetadataTagInfo(DocumentMetadataTagInfo documentMetadataTagInfo) {
		getDocumentMetadataTagInfos().remove(documentMetadataTagInfo);
		documentMetadataTagInfo.setMetadataTagValue(null);

		return documentMetadataTagInfo;
	}

	public MetadataTag getMetadataTag() {
		return this.metadataTag;
	}

	public void setMetadataTag(MetadataTag metadataTag) {
		this.metadataTag = metadataTag;
	}

}