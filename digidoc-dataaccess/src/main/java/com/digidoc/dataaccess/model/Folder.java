package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the folders database table.
 * 
 */
@Entity
@Table(name="folders")
@NamedQuery(name="Folder.findAll", query="SELECT f FROM Folder f")
public class Folder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="folder_name")
	private String folderName;

	//bi-directional many-to-one association to Document
	
	@OneToMany(mappedBy="folder")
	private List<Document> documents;

	//bi-directional many-to-one association to Folder
	@ManyToOne
	@JoinColumn(name="parent_folder_id")
	private Folder folder;

	//bi-directional many-to-one association to Folder
	@OneToMany(mappedBy="folder")
	private List<Folder> folders;
	
	@OneToMany(mappedBy="folder")
	private List<NetworkShare> networkShare;
	
	
	

	public List<NetworkShare> getNetworkShare() {
		return networkShare;
	}

	public void setNetworkShare(List<NetworkShare> networkShare) {
		this.networkShare = networkShare;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Folder() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFolderName() {
		return this.folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public List<Document> getDocuments() {
		return this.documents;
	}

	public void setDocuments(List<Document> documents) {
		this.documents = documents;
	}

	public Document addDocument(Document document) {
		getDocuments().add(document);
		document.setFolder(this);

		return document;
	}

	public Document removeDocument(Document document) {
		getDocuments().remove(document);
		document.setFolder(null);

		return document;
	}

	public Folder getFolder() {
		return this.folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public List<Folder> getFolders() {
		return this.folders;
	}

	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}

	public Folder addFolder(Folder folder) {
		getFolders().add(folder);
		folder.setFolder(this);

		return folder;
	}

	public Folder removeFolder(Folder folder) {
		getFolders().remove(folder);
		folder.setFolder(null);

		return folder;
	}

}