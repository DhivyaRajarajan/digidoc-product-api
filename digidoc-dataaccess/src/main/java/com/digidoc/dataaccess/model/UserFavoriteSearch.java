package com.digidoc.dataaccess.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_favorite_search database table.
 * 
 */
@Entity
@Table(name="user_favorite_search")
@NamedQuery(name="UserFavoriteSearch.findAll", query="SELECT u FROM UserFavoriteSearch u")
public class UserFavoriteSearch implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@Column(name="search_text")
	private String searchText;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;

	public UserFavoriteSearch() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSearchText() {
		return this.searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}