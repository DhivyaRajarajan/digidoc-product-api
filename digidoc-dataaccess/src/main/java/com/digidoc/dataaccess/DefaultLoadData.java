package com.digidoc.dataaccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserRolesInfo;
import com.digidoc.dataaccess.repository.FolderRepository;
import com.digidoc.dataaccess.repository.MetaColurRepository;
import com.digidoc.dataaccess.repository.MetaDataTagRepository;
import com.digidoc.dataaccess.repository.MetadataTagValueRepository;
import com.digidoc.dataaccess.repository.RoleRepository;
import com.digidoc.dataaccess.repository.UserRepository;
import com.digidoc.dataaccess.repository.UserRolesInfoRepository;

@Component
public class DefaultLoadData implements CommandLineRunner {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MetaDataTagRepository metaDataTagRepository;
	
	@Autowired
	MetaColurRepository metaColurRepository;
	
	@Autowired
	MetadataTagValueRepository metadataTagValueRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	UserRolesInfoRepository userRolesInfoRepository;
	
	@Autowired
	FolderRepository folderRepository;
	
	public void run(String... args) {

		
		
		Long roleCount = roleRepository.count();
		if(roleCount == 0) {
			Role role1 = new Role();
			role1.setId(new Long(1));
			role1.setRoleName("Read only");
			role1 = roleRepository.saveAndFlush(role1);
			
			Role role2 = new Role();
			role2.setId(new Long(2));
			role2.setRoleName("Data entry");
			role2 = roleRepository.saveAndFlush(role2);
			
			Role role3 = new Role();
			role3.setId(new Long(3));
			role3.setRoleName("Approver");
			role3 = roleRepository.saveAndFlush(role3);
			
			Role role4 = new Role();
			role4.setId(new Long(4));
			role4.setRoleName("Admin");
			role4 = roleRepository.saveAndFlush(role4);	
		}

		
		Long userCount  = userRepository.count();
		if(userCount == 0) {
			User admin = new User();
			admin.setId(new Long(1));
			admin.setEmail("Admin@email.com");
			admin.setMobileNo("9632548712");
			admin.setFirstName("admin");
			admin.setLastName("admin");
			//EncryptDecrypt encrypt=new EncryptDecrypt();
			try {
				admin.setPassword("32NJbZOnjStVBLISfC3jVA==");
			} catch (Exception e) {
				e.printStackTrace();
			}
			admin.setUserName("Admin");

			userRepository.saveAndFlush(admin);
			
			User user = new User();
			user.setId(new Long(2));
			user.setEmail("User@email.com");
			user.setMobileNo("9865321457");
			user.setFirstName("user");
			user.setLastName("user");
			//EncryptDecrypt encrypt=new EncryptDecrypt();
			try {
				user.setPassword("VFEV0xzltBKt2NITay5Wwg==");
			} catch (Exception e) {
				e.printStackTrace();
			}
			user.setUserName("User");

			userRepository.saveAndFlush(user);	
		}
		
		Long userRoleCount = userRolesInfoRepository.count();
		if(userRoleCount == 0) {
			User admin = userRepository.findById(new Long(1));
			Role role4 = roleRepository.findById(new Long(4));
			if(admin != null && role4 != null) {
				UserRolesInfo userRolesInfo = new UserRolesInfo();
				userRolesInfo.setId(new Long(1));
				userRolesInfo.setUser(admin);
				userRolesInfo.setRole(role4);
				userRolesInfoRepository.saveAndFlush(userRolesInfo);	
			}
			
			User user = userRepository.findById(new Long(2));
			Role role2 = roleRepository.findById(new Long(2));	
			if(user!=null && role2!=null) {
				UserRolesInfo userrolesInfo = new UserRolesInfo();
				userrolesInfo.setId(new Long(2));
				userrolesInfo.setUser(user);
				userrolesInfo.setRole(role2);
				userRolesInfoRepository.save(userrolesInfo);	
			}				
		}
		
		Long metaDataTagCount = metaDataTagRepository.count();
		if(metaDataTagCount == 0) {
			MetadataTag tag=new MetadataTag(); 
			tag.setColourCoding(true);
			tag.setId(new Long(1));
			tag.setMandatory(true);
			tag.setVisible(true);
			tag.setRadio(false);
			tag.setSelect(true);
			tag.setFreeText(false);
			tag.setName("type of document");
			metaDataTagRepository.save(tag);	
		}
		
		Long metaDataTagValueCount = metadataTagValueRepository.count();
		if(metaDataTagValueCount == 0) {
			MetadataTag tag = metaDataTagRepository.findByName("type of document");
			if(tag != null) {
				MetadataTagValue value=new MetadataTagValue();
				value.setId(new Long(1));
				value.setIsEligibleOcr(true);
				value.setMetadataTagValue("Purchase Invoices");
				value.setMetadataTag(tag);
				metadataTagValueRepository.save(value);
				
				MetaColour colur=new MetaColour();
				colur.setId(new Long(1));
				colur.setColurValue("blue");
				colur.setMetadataTagColur(tag);
				colur.setMetadataTagValue(value);
				metaColurRepository.save(colur);
				
				MetadataTagValue valueordeer=new MetadataTagValue();
				valueordeer.setId(new Long(2));
				valueordeer.setIsEligibleOcr(true);
				valueordeer.setMetadataTagValue("Purchase orders");
				valueordeer.setMetadataTag(tag);
				metadataTagValueRepository.save(valueordeer);
				
				MetaColour colurorder=new MetaColour();
				colurorder.setId(new Long(2));
				colurorder.setColurValue("green");
				colurorder.setMetadataTagColur(tag);
				colurorder.setMetadataTagValue(valueordeer);
				metaColurRepository.save(colurorder);
				
				MetadataTagValue valuesale=new MetadataTagValue();
				valuesale.setId(new Long(3));
				valuesale.setIsEligibleOcr(true);
				valuesale.setMetadataTagValue("Sales Invoice");
				valuesale.setMetadataTag(tag);
				metadataTagValueRepository.save(valuesale);
				
				MetaColour colurvalue=new MetaColour();
				colurvalue.setId(new Long(3));
				colurvalue.setColurValue("Orange");
				colurvalue.setMetadataTagColur(tag);
				colurvalue.setMetadataTagValue(valuesale);
				metaColurRepository.save(colurvalue);
				
				MetadataTagValue valuecredit=new MetadataTagValue();
				valuecredit.setId(new Long(4));
				valuecredit.setIsEligibleOcr(true);
				valuecredit.setMetadataTagValue("Sale Credit Notes");
				valuecredit.setMetadataTag(tag);
				metadataTagValueRepository.save(valuecredit);
				
				MetaColour colourcredit=new MetaColour();
				colourcredit.setId(new Long(4));
				colourcredit.setColurValue("Yellow");
				colourcredit.setMetadataTagColur(tag);
				colourcredit.setMetadataTagValue(valuecredit);
				metaColurRepository.save(colourcredit);
				
				MetadataTagValue valuereturn=new MetadataTagValue();
				valuereturn.setId(new Long(5));
				valuereturn.setIsEligibleOcr(true);
				valuereturn.setMetadataTagValue("Purchases returns");
				valuereturn.setMetadataTag(tag);
				metadataTagValueRepository.save(valuereturn);
				
				MetaColour colourreturn=new MetaColour();
				colourreturn.setId(new Long(5));
				colourreturn.setColurValue("red");
				colourreturn.setMetadataTagColur(tag);
				colourreturn.setMetadataTagValue(valuereturn);
				metaColurRepository.save(colourreturn);	
				
				
				MetadataTag tagval=new MetadataTag(); 
				tagval.setColourCoding(false);
				tagval.setId(new Long(2));
				tagval.setMandatory(true);
				tagval.setVisible(true);
				tagval.setRadio(false);
				tagval.setSelect(true);
				tagval.setFreeText(false);
				tagval.setName("Evolution");
				metaDataTagRepository.save(tagval);
				
				MetadataTagValue valueCustomer=new MetadataTagValue();
				valueCustomer.setId(new Long(6));
				valueCustomer.setIsEligibleOcr(true);
				valueCustomer.setMetadataTagValue("Customers");
				valueCustomer.setMetadataTag(tagval);
				metadataTagValueRepository.save(valueCustomer);
				
				MetadataTagValue valueSuppliers=new MetadataTagValue();
				valueSuppliers.setId(new Long(7));
				valueSuppliers.setIsEligibleOcr(true);
				valueSuppliers.setMetadataTagValue("Suppliers");
				valueSuppliers.setMetadataTag(tagval);
				metadataTagValueRepository.save(valueSuppliers);
				
				MetadataTagValue valueAssest=new MetadataTagValue();
				valueAssest.setId(new Long(8));
				valueAssest.setIsEligibleOcr(true);
				valueAssest.setMetadataTagValue("Fixed Assets");
				valueAssest.setMetadataTag(tagval);
				metadataTagValueRepository.save(valueAssest);
				
				MetadataTagValue valueInventory=new MetadataTagValue();
				valueInventory.setId(new Long(9));
				valueInventory.setIsEligibleOcr(true);
				valueInventory.setMetadataTagValue("Inventory");
				valueInventory.setMetadataTag(tagval);
				metadataTagValueRepository.save(valueInventory);
				
				MetadataTagValue valueProjects=new MetadataTagValue();
				valueProjects.setId(new Long(10));
				valueProjects.setIsEligibleOcr(true);
				valueProjects.setMetadataTagValue("Projects");
				valueProjects.setMetadataTag(tagval);
				metadataTagValueRepository.save(valueProjects);
			}				
		}
				
		Long folderCount = folderRepository.count();
		if(folderCount == 0) {
			Folder folder = new Folder();
			folder.setId(new Long(1));
			folder.setFolderName("DigiDoc");
			folder.setFolder(null);
			folderRepository.save(folder);
		}
				
	}
}