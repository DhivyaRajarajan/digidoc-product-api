package com.digidoc.dataaccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Spring boot application
 *
 */
@SpringBootApplication
@Configuration
@ComponentScan(basePackages={"com.digidoc.dataaccess"})
@EnableJpaRepositories(basePackages="com.digidoc.dataaccess.repository")
@EnableElasticsearchRepositories(basePackages = "com.digidoc.dataaccess.es.repository")
@EntityScan({"com.digidoc.dataaccess.model","com.digidoc.dataaccess.es.model"})
@PropertySource({ "classpath:application.properties", "classpath:message.properties" })
public class Application extends SpringBootServletInitializer{
	
    public static void main( String[] args )
    {
		SpringApplication.run(Application.class, args);
    }
}