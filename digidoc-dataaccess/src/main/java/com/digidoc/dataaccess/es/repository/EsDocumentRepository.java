package com.digidoc.dataaccess.es.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.es.model.EsDocument;

@Repository
public interface EsDocumentRepository extends ElasticsearchRepository<EsDocument, Long>{
	
	//List<EsDocument> findAll();
	EsDocument findByDocumentMappingId(Long documentid);

}
