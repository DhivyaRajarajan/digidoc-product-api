package com.digidoc.dataaccess.es.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Document(indexName = "es-search-document-data", type = "documents")
@Component
public class EsDocument {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
	
	private String searchText;
	private Long documentMappingId;
	
	public EsDocument(){
		
	}
	
	public EsDocument(Long id, String searchText, Long documentId) {
		this.id=id;
		this.searchText = searchText;
		this.documentMappingId = documentId;
		
	}


	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public Long getDocumentMappingId() {
		return documentMappingId;
	}

	public void setDocumentMappingId(Long documentMappingId) {
		this.documentMappingId = documentMappingId;
	}

}