package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.model.User;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	public Role findByRoleName(String userName);

	public Role findById(Long id);
}
