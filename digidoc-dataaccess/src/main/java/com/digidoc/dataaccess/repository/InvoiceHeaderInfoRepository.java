package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.digidoc.dataaccess.model.InvoiceHeaderInfo;
import com.digidoc.dataaccess.model.InvoiceInfo;

public interface InvoiceHeaderInfoRepository extends JpaRepository<InvoiceHeaderInfo,Long>{

	InvoiceHeaderInfo findById(Long id);
    public InvoiceHeaderInfo findByInvoiceInfo(InvoiceInfo invoiceObjtemp);
    //invoice_id
}
