package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserRolesInfo;



@Repository
public interface UserRolesInfoRepository extends JpaRepository<UserRolesInfo,Long> {
	public List<UserRolesInfo> findByUser(User userName);

	public List<UserRolesInfo> findByUser_id(Long id);
	
	
	
}
