package com.digidoc.dataaccess.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Folder;

@Repository
public interface FolderRepository extends JpaRepository<Folder, Long> {
	
	public List<Folder> findByFolder(Folder folder);
	public Folder findByFolderName(String folderName);
	public Set<Folder> findByFolderNameAndFolder(String folderName, Folder parentFolder);
	public Folder findById(long id);
}
