package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.DocumentLabel;

@Repository
public interface DocumentLabelRepsitory extends JpaRepository<DocumentLabel, Long>{

//	DocumentLabel findBylabel(Long id);
	DocumentLabel findByLabel_Id(Long id);
	
	List<DocumentLabel> findByDocument(Document doc);

}
