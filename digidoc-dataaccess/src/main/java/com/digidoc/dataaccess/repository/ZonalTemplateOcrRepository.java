package com.digidoc.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.ZonalTemplateOcr;

@Repository
public interface ZonalTemplateOcrRepository extends JpaRepository<ZonalTemplateOcr,Long> {

}
