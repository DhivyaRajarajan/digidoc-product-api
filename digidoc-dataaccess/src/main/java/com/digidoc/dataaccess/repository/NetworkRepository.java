package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;
import com.digidoc.dataaccess.model.NetworkShare;


@Repository
public interface NetworkRepository extends JpaRepository<NetworkShare ,Long> {
	
}
	
