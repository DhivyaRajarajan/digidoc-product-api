package com.digidoc.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.User;



@Repository
public interface UserRepository extends JpaRepository<User,Long> {
	
	public User findByUserName(String userName);
	public User findByEmail(String userName);
	public User findById(Long id);
	
	
}
