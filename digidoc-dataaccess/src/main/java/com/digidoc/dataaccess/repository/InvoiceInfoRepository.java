package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.InvoiceInfo;

public interface InvoiceInfoRepository extends JpaRepository<InvoiceInfo,Long> {
	
	public List<InvoiceInfo> findByDocument(Document id);

}
