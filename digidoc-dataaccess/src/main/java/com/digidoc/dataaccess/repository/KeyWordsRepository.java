package com.digidoc.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.model.User;

@Repository
public interface KeyWordsRepository extends JpaRepository<Keyword, Long> {

	//public Keyword findById(Long userName);
	Keyword findById(long id);

	public Keyword findByKeyword(String id);

}
