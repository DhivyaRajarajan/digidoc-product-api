package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;


@Repository
public interface MetaColurRepository extends JpaRepository<MetaColour ,Long> {
	MetaColour findByMetadataTagValue(MetadataTagValue metadataTagValue);
	
	List<MetaColour> findByMetadataTagColur(MetadataTag metadataTag);
}
