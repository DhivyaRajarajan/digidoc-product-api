package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Label;

@Repository
public interface LabelRepository extends JpaRepository<Label, Long>{

	List<Label> findBylabelName(String value);

}
