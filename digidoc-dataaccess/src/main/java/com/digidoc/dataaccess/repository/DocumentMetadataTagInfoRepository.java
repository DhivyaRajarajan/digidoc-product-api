package com.digidoc.dataaccess.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.DocumentMetadataTagInfo;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;
@Repository
public interface DocumentMetadataTagInfoRepository extends JpaRepository<DocumentMetadataTagInfo, Long>{

	
	List<DocumentMetadataTagInfo> findByMetadataTagValue(MetadataTagValue metatagvalue);
	
	List<DocumentMetadataTagInfo> findByMetadataTag(MetadataTag metadataTag);

	List<DocumentMetadataTagInfo> findByValue(String value);
	
	List<DocumentMetadataTagInfo> findByDocument(Document doc);

	
}
