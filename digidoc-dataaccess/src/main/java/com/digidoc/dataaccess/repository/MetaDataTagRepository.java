package com.digidoc.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.digidoc.dataaccess.model.MetadataTag;


@Repository
public interface MetaDataTagRepository extends JpaRepository<MetadataTag,Long> {
	MetadataTag findByName(String name);
	
}
