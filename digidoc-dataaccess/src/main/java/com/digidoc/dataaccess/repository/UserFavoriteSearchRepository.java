package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserFavoriteSearch;

public interface UserFavoriteSearchRepository extends JpaRepository<UserFavoriteSearch, Long> {

	UserFavoriteSearch findByUserAndSearchText(User user, String searchText);

	List<UserFavoriteSearch> findByUserId(Long userId);
}
