package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.Folder;

public interface DocumentRepository extends JpaRepository<Document, Long>{
	
	List<Document> findByFolder(Folder folder);
	Document findById(Long id);
	Document findFirstByParentDocIdOrderByVersionDesc(Long parentDocId);
	List<Document> findByParentDocId(long id);
	
	Document findByDocumentName(String name);

}
