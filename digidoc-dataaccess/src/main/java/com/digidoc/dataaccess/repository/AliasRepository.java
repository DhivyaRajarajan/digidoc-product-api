package com.digidoc.dataaccess.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.Keyword;

public interface AliasRepository extends JpaRepository<Alias,Long> {
	List<Alias> findByKeyword(Keyword key);
	Alias findById(long id);
	public List<Alias> findBykeyword_id(Long id);
	Alias findByAliasName(String string);
}
