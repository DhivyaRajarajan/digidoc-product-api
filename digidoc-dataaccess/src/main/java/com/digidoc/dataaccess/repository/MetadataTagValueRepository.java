package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;


@Repository
public interface MetadataTagValueRepository extends JpaRepository<MetadataTagValue,Long> {
		
	List<MetadataTagValue> findByMetadataTag(MetadataTag metadataTag);
	
	MetadataTagValue findByMetadataTagValue(String value);
}
