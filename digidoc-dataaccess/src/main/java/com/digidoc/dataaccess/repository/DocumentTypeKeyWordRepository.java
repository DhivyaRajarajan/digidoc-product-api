package com.digidoc.dataaccess.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.digidoc.dataaccess.model.DocumentTypeKeyword;

@Repository
public interface DocumentTypeKeyWordRepository extends JpaRepository<DocumentTypeKeyword,Long>{
	
	List<DocumentTypeKeyword> findByDocumentType(String name);
	
	/*@Query("SELECT v.keyword, v.documentType FROM DocumentTypeKeyword v GROUP BY v.keyword, v.documentType")*/
	@Query("SELECT dt FROM DocumentTypeKeyword dt GROUP BY dt.documentType")
	List<DocumentTypeKeyword> getListKeywordsWithDocType();

}
