package com.digidoc.dataaccess.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.digidoc.dataaccess.model.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Long> {

	
}
