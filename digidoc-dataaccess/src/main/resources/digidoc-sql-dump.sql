/*
SQLyog Community v12.2.1 (64 bit)
MySQL - 5.7.16-log : Database - digidoc-product
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`digidoc-product` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `digidoc-product`;

/*Table structure for table `aliases` */

DROP TABLE IF EXISTS `aliases`;

CREATE TABLE `aliases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alias_name` varchar(200) NOT NULL,
  `keyword_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_alias_1` (`keyword_id`),
  CONSTRAINT `fk_alias_1` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Table structure for table `documents` */

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `folder_id` bigint(20) NOT NULL,
  `document_unique_id` bigint(20) NOT NULL,
  `document_name` varchar(250) NOT NULL,
  `document_type` varchar(250) NOT NULL,
  `document_content` longblob NOT NULL,
  `page_count` bigint(20) NOT NULL,
  `parent_doc_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_docu_1` (`user_id`),
  KEY `fk_docu_2` (`folder_id`),
  CONSTRAINT `fk_docu_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_docu_2` FOREIGN KEY (`folder_id`) REFERENCES `folders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `folders` */

DROP TABLE IF EXISTS `folders`;

CREATE TABLE `folders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `folder_name` varchar(250) NOT NULL,
  `parent_folder_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_folders_1` (`parent_folder_id`),
  CONSTRAINT `fk_folders_1` FOREIGN KEY (`parent_folder_id`) REFERENCES `folders` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Table structure for table `keywords` */

DROP TABLE IF EXISTS `keywords`;

CREATE TABLE `keywords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `labels` */

DROP TABLE IF EXISTS `labels`;

CREATE TABLE `labels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label_name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `metadata_tag` */

DROP TABLE IF EXISTS `metadata_tag`;

CREATE TABLE `metadata_tag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `is_radio` tinyint(1) DEFAULT NULL,
  `is_select` tinyint(1) DEFAULT NULL,
  `is_free_text` tinyint(1) DEFAULT NULL,
  `is_mandatory` tinyint(1) DEFAULT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `metadata_tag_values` */

DROP TABLE IF EXISTS `metadata_tag_values`;

CREATE TABLE `metadata_tag_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `metadata_tag_id` bigint(20) NOT NULL,
  `metadata_tag_value` varchar(200) NOT NULL,
  `is_eligible_ocr` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tag_val_1` (`metadata_tag_id`),
  CONSTRAINT `fk_tag_val_1` FOREIGN KEY (`metadata_tag_id`) REFERENCES `metadata_tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `permission` */

DROP TABLE IF EXISTS `permission`;

CREATE TABLE `permission` (
  `id` bigint(20) NOT NULL,
  `privelege` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL,
  `role_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `role_permission_info` */

DROP TABLE IF EXISTS `role_permission_info`;

CREATE TABLE `role_permission_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `permission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_role_per_2` (`permission_id`),
  KEY `fk_role_per_1` (`role_id`),
  CONSTRAINT `fk_role_per_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_role_per_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(250) NOT NULL,
  `mobile_no` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Table structure for table `user_favorite_search` */

DROP TABLE IF EXISTS `user_favorite_search`;

CREATE TABLE `user_favorite_search` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `search_text` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_fav_1` (`user_id`),
  CONSTRAINT `fk_user_fav_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `user_roles_info` */

DROP TABLE IF EXISTS `user_roles_info`;

CREATE TABLE `user_roles_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_roles_1` (`user_id`),
  KEY `fk_user_roles_2` (`role_id`),
  CONSTRAINT `fk_user_roles_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_roles_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `invoice_info` */

DROP TABLE IF EXISTS `invoice_info`;

CREATE TABLE `invoice_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_id` bigint(20) NOT NULL,
  `page_count` int(11) NOT NULL,
  `confidence_score` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inv_1` (`document_id`),
  CONSTRAINT `fk_inv_1` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `invoice_header_info` */

DROP TABLE IF EXISTS `invoice_header_info`;

CREATE TABLE `invoice_header_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint(20) NOT NULL,
  `keyword_id` bigint(20) NOT NULL,
  `extracted_value` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_inv_header_1` (`invoice_id`),
  KEY `fk_inv_header_2` (`keyword_id`),
  CONSTRAINT `fk_inv_header_1` FOREIGN KEY (`invoice_id`) REFERENCES `invoice_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_inv_header_2` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*Table structure for table `document_labels` */

DROP TABLE IF EXISTS `document_labels`;

CREATE TABLE `document_labels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_id` bigint(20) NOT NULL,
  `label_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_doc_label_1` (`document_id`),
  KEY `fk_doc_label_2` (`label_id`),
  CONSTRAINT `fk_doc_label_1` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_doc_label_2` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `document_metadata_tag_info` */

DROP TABLE IF EXISTS `document_metadata_tag_info`;

CREATE TABLE `document_metadata_tag_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_id` bigint(20) NOT NULL,
  `metadata_tag_id` bigint(20) NOT NULL,
  `metadata_tag_value_id` bigint(20) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_doc_tag_1` (`document_id`),
  KEY `fk_doc_tag_2` (`metadata_tag_id`),
  KEY `fk_doc_tag_3` (`metadata_tag_value_id`),
  CONSTRAINT `fk_doc_tag_1` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_doc_tag_2` FOREIGN KEY (`metadata_tag_id`) REFERENCES `metadata_tag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_doc_tag_3` FOREIGN KEY (`metadata_tag_value_id`) REFERENCES `metadata_tag_values` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `document_type_keywrods` */

DROP TABLE IF EXISTS `document_type_keywrods`;

CREATE TABLE `document_type_keywrods` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `keyword_id` bigint(20) NOT NULL,
  `document_type` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_doc_key_1` (`keyword_id`),
  CONSTRAINT `fk_doc_key_1` FOREIGN KEY (`keyword_id`) REFERENCES `keywords` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
