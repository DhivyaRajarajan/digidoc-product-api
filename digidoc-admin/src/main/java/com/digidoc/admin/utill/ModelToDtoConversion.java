package com.digidoc.admin.utill;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.UserDto;
import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.dto.UserRoleInfoDTO;
import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserRolesInfo;
import com.digidoc.dataaccess.repository.DocumentMetadataTagInfoRepository;
import com.digidoc.dataaccess.repository.MetaColurRepository;
import com.digidoc.dataaccess.repository.MetadataTagValueRepository;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTagValue;
import com.digidoc.dataaccess.model.NetworkShare;
import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.DocumentDto;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.admin.dto.UserFavoriteSearchDto;
import com.digidoc.dataaccess.model.UserFavoriteSearch;
import com.digidoc.dataaccess.model.UserRolesInfo;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.apache.tika.parser.AutoDetectParser;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import com.digidoc.dataaccess.model.DocumentLabel;
import com.digidoc.dataaccess.model.DocumentMetadataTagInfo;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;

@Service
public class ModelToDtoConversion {
	
	public List<FolderDto> folderToFolderDto(List<Folder> folderList) {
		List<FolderDto> folderDtoList = new ArrayList<FolderDto>();
		for (Folder folde : folderList) {
			FolderDto folderDt = new FolderDto();
			folderDt.setId(folde.getId());
			folderDt.setFolderName(folde.getFolderName());
			if (folde.getFolder() != null) {
				folderDt.setFolder(folde.getFolder().getId());
			} else {
				folderDt.setFolder(null);
			}

			folderDtoList.add(folderDt);
		}
		return folderDtoList;

	}

	public UserRoleDto userRoleModeltodto(Role userRolemodel) {

		UserRoleDto userRoleDto = new UserRoleDto();
		userRoleDto.setId(userRolemodel.getId());
		userRoleDto.setRoleName(userRolemodel.getRoleName());
		return userRoleDto;

	}
	
	
	public NetworkShare netDtotoModel(NetworkDto networkDto,Folder folder) {

		NetworkShare share = new NetworkShare();
		share.setNetworkPath(networkDto.getNetworkPath());
		share.setPassword(networkDto.getPassword());
		share.setUserName(networkDto.getUserName());
		share.setPollTime(networkDto.getPollTime());
		share.setFolder(folder);
		return share;

	}

	public List<UserRoleDto> userRoleToUserRoleDto(List<Role> userRoleList) {
		List<UserRoleDto> userRoleDtoList = new ArrayList<UserRoleDto>();
		for (Role userRole : userRoleList) {
			UserRoleDto userRoleDto = new UserRoleDto();
			userRoleDto.setId(userRole.getId());
			userRoleDto.setRoleName(userRole.getRoleName());
			// userRoleDto.setRolePermissionInfoId(userRole.getRolePermissionInfos());
			// userRoleDto.setRoleName(userRole.getRoleName());

			// if(folde.getFolder()!=null) {
			// folderDt.setFolder(folde.getFolder().getId());
			// }else {
			// folderDt.setFolder(null);
			// }

			userRoleDtoList.add(userRoleDto);
		}
		return userRoleDtoList;

	}

	public List<UserDto> userToUserDto(List<User> userList) {
		List<UserDto> userDtoList = new ArrayList<UserDto>();
		for (User user : userList) {
			UserDto userDto = new UserDto();
			userDto.setId(user.getId());
			userDto.setUserName(user.getUserName());

			EncryptDecrypt encrypt = new EncryptDecrypt();
			try {
				userDto.setPassword(encrypt.decrypt(user.getPassword()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			userDto.setMobileNo(user.getMobileNo());
			userDto.setEmail(user.getEmail());
			userDtoList.add(userDto);
		}
		return userDtoList;

	}

	public UserDto userToUserDtoForSingleObject(User user, List<UserRolesInfo> rols) {
		List<UserRoleInfoDTO> roleinfos = roleModelToRoleDTO(rols);
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setUserName(user.getUserName());
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setUserRoleinfoDto(roleinfos);
		EncryptDecrypt encrypt = new EncryptDecrypt();
		try {
			userDto.setPassword(encrypt.decrypt(user.getPassword()));
		} catch (Exception e) {

			e.printStackTrace();
		}
		userDto.setMobileNo(user.getMobileNo());
		userDto.setEmail(user.getEmail());
		userDto.setId(user.getId());

		return userDto;

	}

	public List<UserRoleInfoDTO> roleModelToRoleDTO(List<UserRolesInfo> userRolesInfo) {

		List<UserRoleInfoDTO> rolelist = new ArrayList<UserRoleInfoDTO>();
		for (UserRolesInfo role : userRolesInfo) {
			UserRoleInfoDTO roledto = new UserRoleInfoDTO();
			roledto.setId(role.getId());
			roledto.setRoleid(role.getRole().getRoleName());
			roledto.setUserid(role.getUser().getUserName());
			rolelist.add(roledto);
		}
		return rolelist;

	}

	public List<KeywordDto> keywordsToKeywordsDto(List<Keyword> keywordslist) {

		List<KeywordDto> KeywordsList = new ArrayList<KeywordDto>();
		for (Keyword keyword : keywordslist) {
			KeywordDto keywordsDto = new KeywordDto();
			keywordsDto.setId(keyword.getId());
			keywordsDto.setKeyword(keyword.getKeyword());

			KeywordsList.add(keywordsDto);
		}
		return KeywordsList;

	}

	public List<DocumentDto> documentDtoToModel(List<Document> documentList) {
		
		
		List<DocumentDto> documentDtoList = new ArrayList<DocumentDto>();
		if(documentList!=null)
		{
		for (Document document : documentList) {
			DocumentDto documentDto = new DocumentDto();
			documentDto.setDocumentColur(null);
			documentDto.setDocumentName(document.getDocumentName());
			documentDto.setDocumentType(document.getDocumentType());
			//documentDto.setDocumentContent(document.getDocumentContent());
			documentDto.setDocumentUniqueId(document.getDocumentUniqueId());
			documentDto.setFolderId(document.getFolder().getId());
			documentDto.setUserId(document.getUser().getId());
			documentDto.setLabelName(document.getDocumentLabels().toString());
			documentDto.setPageCount(document.getPageCount());
			documentDto.setId(document.getId());
			documentDto.setVersId(document.getVersion());
			documentDto.setOriginalDocumentName(document.getOriginalDocumentName());
			documentDto.setDocumentType(document.getDocumentType());
			documentDtoList.add(documentDto);
			
		}
		return documentDtoList;
		}
		return null;
	}

	public List<AliasDto> aliasToAliasDto(List<Alias> aliasList) {

		List<AliasDto> List = new ArrayList<AliasDto>();
		for (Alias alias : aliasList) {
			AliasDto aliasDto = new AliasDto();
			aliasDto.setId(alias.getId());
			aliasDto.setKeywordId(alias.getKeyword().getId());
			aliasDto.setAliasName(alias.getAliasName());

			List.add(aliasDto);
		}
		return List;
	}

	public AliasDto aliasToAliasDto(Alias alias) {
		AliasDto aliasDto = new AliasDto();
		if (alias != null) {
			aliasDto.setId(alias.getId());
			aliasDto.setKeywordId(alias.getKeyword().getId());
			aliasDto.setAliasName(alias.getAliasName());
		}
		return aliasDto;
	}

	public KeywordDto keywordModelToKeyWordDto(Keyword keyWord) {

		KeywordDto Keyword = new KeywordDto();
		if (keyWord != null) {
			Keyword.setId(keyWord.getId());
			Keyword.setKeyword(keyWord.getKeyword());

		}
		return Keyword;
	}

	/*public List<FolderDto> folderDtoToFolderConversion(List<Folder> folderList) {
		// TODO Auto-generated method stub
		List<FolderDto> listFolderDto = new ArrayList<FolderDto>();
		if (folderList.size() != 0) {
			for (Folder folder : folderList) {
				FolderDto folderDto = new FolderDto();
				folderDto.setId(folder.getId());
				folderDto.setFolderName(folder.getFolderName());
				// folderDto.setSubFolders(folderToFolderDtoConversion(folder.getFolders()));
				// folderDto.setSubFolders(folderDtoToFolderConversion(folder.getFolders()));
				listFolderDto.add(folderDto);
			}
			return listFolderDto;
		}
		return null;
	}*/

	public List<FolderDto> folderToFolderDtoConversion(List<Folder> folderList) {
		// TODO Auto-generated method stub
		List<FolderDto> listFolderDto = new ArrayList<FolderDto>();
		if (folderList.size() != 0) {
			for (Folder folder : folderList) {
				FolderDto folderDto = new FolderDto();
				folderDto.setId(folder.getId());
				folderDto.setFolderName(folder.getFolderName());
				// folderDto.setSubFolders(folderToFolderDtoConversion(folder.getFolders()));
				folderDto.setSubFolders(folderToFolderDtoConversion(folder.getFolders()));
				
				listFolderDto.add(folderDto);
			}
			return listFolderDto;
		}
		return null;
	}


	public List<DocumentDto> documentToDocumentDtoConversion(List<Document> documentList) {
		DocumentDto documentDto2=new DocumentDto();
		documentDto2.setDocumentColur(null);
		// TODO Auto-generated method stub
		List<DocumentDto> documentDtos = new ArrayList<DocumentDto>();
		if(documentList!=null)
		{
		for(Document document:documentList){
			documentDto2.setId(document.getId());
			documentDto2.setDocumentName(document.getDocumentName());
			documentDto2.setDocumentType(document.getDocumentType());
	//		documentDto2.setDocumentContent(document.getDocumentContent());
			documentDto2.setDocumentUniqueId(document.getDocumentUniqueId());
			documentDto2.setFolderId(document.getFolder().getId());
			documentDto2.setUserId(document.getUser().getId());
			documentDto2.setLabelName(document.getDocumentLabels().toString());
			documentDto2.setPageCount(document.getPageCount());
			documentDtos.add(documentDto2);
		}
		return documentDtos;
		}
		return null;
	}
	
	public List<UserFavoriteSearchDto> UserFavoriteSearchListToUserFavoriteSearchDto(
			List<UserFavoriteSearch> userFavoriteSearchList) {

		List<UserFavoriteSearchDto> List = new ArrayList<UserFavoriteSearchDto>();
		for (UserFavoriteSearch userFavoriteSearch : userFavoriteSearchList) {
			UserFavoriteSearchDto userFavoriteSearchDto = new UserFavoriteSearchDto();
			userFavoriteSearchDto.setUserId(userFavoriteSearch.getUser().getId().longValue());
			userFavoriteSearchDto.setSearchText(userFavoriteSearch.getSearchText());
			userFavoriteSearchDto.setId(userFavoriteSearch.getId());

			List.add(userFavoriteSearchDto);
		}
		return List;
	}
	
	/*public String readeDocumentContent(InputStream stream) throws IOException, SAXException, TikaException{
		 Detector detector = new DefaultDetector();
	        Metadata metadata = new Metadata();

	        MediaType mediaType = detector.detect(stream, metadata);
	        System.out.println("mediaType----"+mediaType.toString());
	        return mediaType.toString();
		 Parser parser = new AutoDetectParser();
	        ContentHandler handler = new BodyContentHandler();
	        Metadata metadata = new Metadata();
	        ParseContext context = new ParseContext();

	        parser.parse(stream, handler, metadata, context);
	        return handler.toString();
	}*/
	public List<DocumentMetaDataTagInfoDto> documentsListTodocumentsListDto(List<DocumentMetadataTagInfo> documentsListUsingMetaDataTagValue) {

		List<DocumentMetaDataTagInfoDto> List = new ArrayList<DocumentMetaDataTagInfoDto>();
		for (DocumentMetadataTagInfo documentMetadataTagInfo : documentsListUsingMetaDataTagValue) {
			DocumentMetaDataTagInfoDto documentMetaDataTagInfoDto = new DocumentMetaDataTagInfoDto();
			documentMetaDataTagInfoDto.setId(documentMetadataTagInfo.getId());
			documentMetaDataTagInfoDto.setDocumentId(documentMetadataTagInfo.getDocument().getId());
			documentMetaDataTagInfoDto.setMetaDataTagId(documentMetadataTagInfo.getMetadataTag().getId());
			if(documentMetadataTagInfo.getMetadataTagValue()!=null) {
			documentMetaDataTagInfoDto.setMetaDataTagValueId(documentMetadataTagInfo.getMetadataTagValue().getId());
			}
			documentMetaDataTagInfoDto.setValue(documentMetadataTagInfo.getValue());
 

			List.add(documentMetaDataTagInfoDto);
		}
		
		return List;
	}

	public DocumentLabelsDto documentLabelTodocumentLabelDto(DocumentLabel documentLabel) {
		
		DocumentLabelsDto documentLabelsDto = new DocumentLabelsDto();
		documentLabelsDto.setId(documentLabel.getId());
		documentLabelsDto.setDocumentId(documentLabel.getDocument().getId());
		documentLabelsDto.setLabelId(documentLabel.getLabel().getId());
		
 
     	return documentLabelsDto;
	}

	public DocumentDto documentTodocumentDto(Document document) {
		
		DocumentDto documentDto = new DocumentDto();
		documentDto.setId(document.getId());
		documentDto.setDocumentName(document.getDocumentName());
		
		
		
		return documentDto;
	}
	
	
	public UserDto userToUserDto(User user, List<UserRolesInfo> userRolesInfo, List<Role> userRolesinfo) {
		String role = "";
		
		//List<UserDto> userDtoList = new ArrayList<UserDto>();
		    UserDto userDto = new UserDto();
			userDto.setId(user.getId());
			userDto.setUserName(user.getUserName());
			
		if (user.getFirstName() != null) {
			userDto.setFirstName(user.getFirstName());
		}
		if (user.getLastName() != null) {
			userDto.setLastName(user.getLastName());
		}

			EncryptDecrypt encrypt = new EncryptDecrypt();
			try {
				userDto.setPassword(encrypt.decrypt(user.getPassword()));
			} catch (Exception e) {
				e.printStackTrace();
			}

			userDto.setMobileNo(user.getMobileNo());
			userDto.setEmail(user.getEmail());
			if (!(userRolesinfo.isEmpty())) {
			for (Role userRoles : userRolesinfo)
			{
				role =  userRoles.getRoleName()+","+role;	
			
		}
			role = role.substring(0, role.length() - 1);
			userDto.setUserRole(role);
			}
		
		return userDto;
	}


	
	
}
