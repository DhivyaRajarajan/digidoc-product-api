package com.digidoc.admin.utill;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.MetaDataTagValueDTO;
import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;

@Component
public class MetaDataTagConversion {
	
	public static final String radio="Radio";
	public static final String select="Dropdown";
	public static final String freetext="Freetext";
	
	
	public MetadataTag metaDtoToModel(MetaDataTagDTO metaDto) {
		MetadataTag metainfo = new MetadataTag();
		metainfo.setFreeText(metaDto.isFree_text());
		metainfo.setMandatory(metaDto.isIsmandatory());
		metainfo.setRadio(metaDto.isRadio());
		metainfo.setSelect(metaDto.isSelect());
		metainfo.setVisible(metaDto.isIsvisible());
		metainfo.setName(metaDto.getName());
		metainfo.setId(metaDto.getId());
		metainfo.setColourCoding(metaDto.isColorEnable());
		return metainfo;

	}
	
	public MetaDataTagDTO metaModelToDto( MetadataTag meta) {
		MetaDataTagDTO metainfo = new MetaDataTagDTO();
		metainfo.setIsmandatory(meta.isMandatory());
		metainfo.setIsvisible(meta.isVisible());
		metainfo.setName(meta.getName());
		metainfo.setId(meta.getId());
		metainfo.setRadio(meta.isRadio());
		metainfo.setSelect(meta.isSelect());
		metainfo.setFree_text(meta.isFreeText());
		metainfo.setColorEnable(meta.isColourCoding());
		if(meta.isRadio()) {
			metainfo.setTypeVal(radio);
		}
		if(meta.isSelect()) {
			metainfo.setTypeVal(select);
				}
		if(meta.isFreeText()) {
			metainfo.setTypeVal(freetext);
		}
		
		return metainfo;

	}
	
	
	public MetaColour metaColourDtoToModel(MetaDataTagDTO metaDto) {
		MetaColour metatagcolurinfo = new MetaColour();
		MetadataTagValue metatagvalue = new MetadataTagValue();
		metatagvalue.setId(metaDto.getValueid());
		metatagcolurinfo.setMetadataTagValue(metatagvalue);
		MetadataTag metainfo = new MetadataTag();
		metainfo.setId(metaDto.getMetaDataTagValueId());
		metatagcolurinfo.setMetadataTagColur(metainfo);
		metatagcolurinfo.setId(metaDto.getColorId());
		metatagcolurinfo.setColurValue(metaDto.getColorValue());
		metatagcolurinfo.setId(metaDto.getColorId());
	
		return metatagcolurinfo;

	}
	
	public MetadataTagValue metaTagvalueDtoToModel(MetaDataTagDTO metaDto) {
		MetadataTagValue metataginfo = new MetadataTagValue();
		metataginfo.setIsEligibleOcr(metaDto.isIseligibleOcr());
		metataginfo.setMetadataTagValue(metaDto.getMetaDataTagValue());
		MetadataTag metainfo = new MetadataTag();
		metainfo.setId(metaDto.getMetaDataTagValueId());
		metataginfo.setMetadataTag(metainfo);
		metataginfo.setId(metaDto.getId());
		return metataginfo;

	}
	
	public MetaDataTagDTO metaTagvalueModelToDto(MetadataTagValue metadataTagValue) {
		MetaDataTagDTO metatagvaluedto = new MetaDataTagDTO();
		metatagvaluedto.setId(metadataTagValue.getId());
		metatagvaluedto.setIseligibleOcr(metadataTagValue.getIsEligibleOcr());
		metatagvaluedto.setMetaDataTagValue(metadataTagValue.getMetadataTagValue());
		metatagvaluedto.setMetaDataTagValueId(metadataTagValue.getMetadataTag().getId());
		metatagvaluedto.setName(metadataTagValue.getMetadataTag().getName());
		
		return metatagvaluedto;
		

	}
	
	
	public MetaDataTagDTO metaTagvaluecolurModelToDto(MetadataTagValue metadataTagValue,MetaColour colur) {
		MetaDataTagDTO metatagvaluedto = new MetaDataTagDTO();
		metatagvaluedto.setId(metadataTagValue.getId());
		metatagvaluedto.setIseligibleOcr(metadataTagValue.getIsEligibleOcr());
		metatagvaluedto.setMetaDataTagValue(metadataTagValue.getMetadataTagValue());
		metatagvaluedto.setMetaDataTagValueId(metadataTagValue.getMetadataTag().getId());
		metatagvaluedto.setName(metadataTagValue.getMetadataTag().getName());
		if(colur!=null) {
		metatagvaluedto.setColorValue(colur.getColurValue());
		metatagvaluedto.setColorId(colur.getId());
		}
		else {
			metatagvaluedto.setColorValue("");
			metatagvaluedto.setColorId(0L);
		}
		return metatagvaluedto;
		

	}
	
	public MetaDataTagValueDTO MetaTagvalueModelToMetaDatatagValueDto(MetadataTagValue metadataTagValue) {
		MetaDataTagValueDTO metatagvaluedto = new MetaDataTagValueDTO();
		metatagvaluedto.setId(metadataTagValue.getId());
		metatagvaluedto.setIseligibleOcr(metadataTagValue.getIsEligibleOcr());
		metatagvaluedto.setMetadataTagValue(metadataTagValue.getMetadataTagValue());
		metatagvaluedto.setMetadata_tagvalue_id(metadataTagValue.getMetadataTag().getId());
		return metatagvaluedto;
		

	}
	

}