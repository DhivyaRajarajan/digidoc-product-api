package com.digidoc.admin.utill;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.UserDto;
import com.digidoc.admin.dto.UserFavoriteSearchDto;
import com.digidoc.admin.dto.UserRoleInfoDTO;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserFavoriteSearch;
import com.digidoc.dataaccess.model.UserRolesInfo;
import com.digidoc.dataaccess.repository.FolderRepository;
import com.digidoc.dataaccess.repository.RoleRepository;
import com.digidoc.dataaccess.repository.UserRepository;


@Service
public class DtoToModelConversion {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	FolderRepository folderRepository;
	
	public User userDtoToUser(UserDto userDto) {
		User user = new User();
		if(userDto.getId() != null) {
			user.setId(userDto.getId());
		}
		user.setEmail(userDto.getEmail());
		user.setMobileNo(userDto.getMobileNo());
		EncryptDecrypt encrypt=new EncryptDecrypt();
		try {
			user.setPassword(encrypt.encrypt(userDto.getPassword()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		user.setUserName(userDto.getUserName());
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		
		Role userRole = new Role();
		userRole.setId(userDto.getUserRoleId());
		user.setId(userDto.getId());
		return user;
		
	}
	public Keyword keyWordrDtoToModel(KeywordDto KeywordsDto) {
		Keyword keyword = new Keyword();
		if (KeywordsDto.getId() != null) {
			keyword.setId(KeywordsDto.getId());
		}
		if (KeywordsDto.getKeyword() != null) {
			keyword.setKeyword(KeywordsDto.getKeyword());
		}
		// Document document = new Document();
		// document.setId(userDto.getDocumentsId());
		// user.setDocuments(documents);
		return keyword;

	}
	
	
	public UserRolesInfo userRolesDtoToUserRoles(String rolename,Long userid) {
		UserRolesInfo userrole = new UserRolesInfo();
		User user=userRepository.findOne(userid);
		userrole.setUser(user);
		Role roleID=roleRepository.findByRoleName(rolename);
		Role role=roleRepository.findOne(roleID.getId());
		userrole.setRole(role);
		return userrole;
		
	}
	
	public UserRoleInfoDTO userRolesToUserRolesdto(UserRolesInfo role) {
		UserRoleInfoDTO userrole = new UserRoleInfoDTO();
		userrole.setRoleid(role.getRole().getRoleName());
		
		return userrole;
		
	}

	/*public Document documentDtoToDocument(DocumentDto documentDto) {
		// TODO Auto-generated method stub
		Document document = new Document();
		if(documentDto.getDocumentName()!=null){
		//	document.setUser(documentDto.getUserId());
		
			document.setFolder(documentDto.getFolderId());
			document.setDocumentUniqueId(documentDto.getDocument_unique_id());
			document.setDocumentName(documentDto.getDocumentName());
			document.setDocumentType(documentDto.getDocument_type());
			document.setDocumentContent(documentDto.getDocument_content().toByteArray());
			document.setPageCount(documentDto.getPage_count());
			document.setParentDocId(documentDto.getParent_doc_id());
			
		}
		return document;
	}
*/
	
	public Folder folderDtoToFolderConversion(FolderDto folderDto) {
		// TODO Auto-generated method stub
		Folder folder=new Folder();

		if(folderDto.getId()!=null && folderDto.getFolder()==null){
			folder = folderRepository.findById(folderDto.getId());			
		}
		if(folderDto.getFolderName()!=null && !folderDto.getFolderName().equals("")){
			
			folder.setFolderName(folderDto.getFolderName());
		}
		
		if(folderDto.getFolder()!=null) {
			Folder parentFolder = folderRepository.findById(folderDto.getFolder());
			folder.setFolder(parentFolder);
		}
		
		return folder;
	}
	
	
public Alias AliasDtoToModel(AliasDto aliasDto,Keyword keyword) {
		
		Alias alias = new Alias();
		if (aliasDto.getId() != null) {
			alias.setId(aliasDto.getId());
		}
		
		if (aliasDto.getKeywordId() != null) {
			alias.setKeyword(keyword);
		}
		
		if (aliasDto.getAliasName() != null) {
			alias.setAliasName(aliasDto.getAliasName());
		}
		
		
		
		
		return alias;
		
	}

public UserFavoriteSearch UserFavoriteSearchDtoToModel(UserFavoriteSearchDto userFavoriteSearchDto, User user) {
	UserFavoriteSearch userFavoriteSearch = new UserFavoriteSearch();

	if (userFavoriteSearchDto.getUserId() != null) {
		userFavoriteSearch.setUser(user);
	}
	if (userFavoriteSearchDto.getSearchText() != null) {
		userFavoriteSearch.setSearchText(userFavoriteSearchDto.getSearchText());
		;
	}
	// Document document = new Document();
	// document.setId(userDto.getDocumentsId());
	// user.setDocuments(documents);
	return userFavoriteSearch;

}

	
	
	
}
