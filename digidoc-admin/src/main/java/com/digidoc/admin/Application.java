package com.digidoc.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

/**
 * Spring boot application
 *
 */
@SpringBootApplication
@Configuration
@ComponentScan(basePackages="com.digidoc")
@EnableJpaRepositories(basePackages="com.digidoc.dataaccess.repository")
@EntityScan("com.digidoc.dataaccess.model")
@PropertySource({ "classpath:application.properties", "classpath:message.properties" })
public class Application extends SpringBootServletInitializer{
	
    public static void main( String[] args )
    {
		SpringApplication.run(Application.class, args);
    }
    @Bean
    public RestTemplate getRestTemplate() {
       return new RestTemplate();
    }
}
