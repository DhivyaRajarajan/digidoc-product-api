package com.digidoc.admin.serviceImpl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.UserFavoriteSearchDto;
import com.digidoc.admin.service.UserFavoriteSearchService;
import com.digidoc.admin.utill.DtoToModelConversion;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.DocumentLabel;
import com.digidoc.dataaccess.model.DocumentMetadataTagInfo;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.model.Label;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserFavoriteSearch;
import com.digidoc.dataaccess.repository.DocumentLabelRepsitory;
import com.digidoc.dataaccess.repository.DocumentMetadataTagInfoRepository;
import com.digidoc.dataaccess.repository.KeyWordsRepository;
import com.digidoc.dataaccess.repository.LabelRepository;
import com.digidoc.dataaccess.repository.UserFavoriteSearchRepository;
import com.digidoc.dataaccess.repository.UserRepository;

@Service
public class UserFavoriteSearchImpl implements UserFavoriteSearchService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Autowired
	UserFavoriteSearchRepository userFavoriteSearchRepository;
	@Autowired
	DocumentMetadataTagInfoRepository documentMetadataTagInfoRepository;
	@Autowired
	DocumentLabelRepsitory documentLabelRepsitory;
	@Autowired
	LabelRepository labelRepository;
	

	@Override
	public boolean addUserFavoriteSearchText(UserFavoriteSearchDto userFavoriteSearchDto) {
		DtoToModelConversion dtoToModelConversion = new DtoToModelConversion();

		User user = userRepository.findOne(userFavoriteSearchDto.getUserId());
		if (user != null) {

			UserFavoriteSearch userFavoriteSearch = userFavoriteSearchRepository.findByUserAndSearchText(user,
					userFavoriteSearchDto.getSearchText());
			if (userFavoriteSearch != null) {
				return true;
			}
			userFavoriteSearch = dtoToModelConversion.UserFavoriteSearchDtoToModel(userFavoriteSearchDto, user);
			// if (!(alias.getAliasName().isEmpty())) {
			userFavoriteSearchRepository.save(userFavoriteSearch);
			// return true;
			// }
			return true;

		} else {
			return false;

		}
	}

	@Override
	public List<UserFavoriteSearchDto> getAllUserFavoriteSearchText(Long userId) {
		

	     //	User user = new User();
		  //   user.setId(userId);
			List<UserFavoriteSearch> UserFavoriteSearchList = userFavoriteSearchRepository.findByUserId(userId);

			List<UserFavoriteSearchDto> UserFavoriteSearchDtoList = modelToDtoConversion.UserFavoriteSearchListToUserFavoriteSearchDto(UserFavoriteSearchList);
			return UserFavoriteSearchDtoList;

		}

	@Override
	public List<DocumentMetaDataTagInfoDto> getDocumentsUsingMetaDataTagValue(String value) {
		
		List<DocumentMetadataTagInfo> documentsListUsingMetaDataTagValue = documentMetadataTagInfoRepository.findByValue(value);
       
		List<DocumentMetaDataTagInfoDto> documentsListUsingMetaDataTagValueDto = modelToDtoConversion.documentsListTodocumentsListDto(documentsListUsingMetaDataTagValue);
		return documentsListUsingMetaDataTagValueDto;
		
		
	}

	@Override
	public List<DocumentLabelsDto> getDocumentsUsingLabel(String value) {
		
		DocumentLabelsDto documentLabelsDto = new DocumentLabelsDto();
		List<DocumentLabelsDto> list = new ArrayList<DocumentLabelsDto>();

		List<Label> labelList = labelRepository.findBylabelName(value);
		for (Label label : labelList)
		{
			DocumentLabel documentLabel  =  documentLabelRepsitory.findByLabel_Id(label.getId());
			if (documentLabel!=null) {
				documentLabelsDto = modelToDtoConversion.documentLabelTodocumentLabelDto(documentLabel);
				list.add(documentLabelsDto);
			}
			
		}
		return list;
	}
}
