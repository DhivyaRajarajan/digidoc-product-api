package com.digidoc.admin.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.MetaDataTagValueDTO;
import com.digidoc.admin.service.MetadataTagService;
import com.digidoc.admin.utill.MetaDataTagConversion;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.DocumentMetadataTagInfo;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.model.MetaColour;
import com.digidoc.dataaccess.model.MetadataTag;
import com.digidoc.dataaccess.model.MetadataTagValue;
import com.digidoc.dataaccess.repository.DocumentMetadataTagInfoRepository;
import com.digidoc.dataaccess.repository.MetaColurRepository;
import com.digidoc.dataaccess.repository.MetaDataTagRepository;
import com.digidoc.dataaccess.repository.MetadataTagValueRepository;
import com.digidoc.dataaccess.repository.UserRepository;

@Service
public class MetadataTagServiceImpl implements MetadataTagService {

	@Autowired
	MetaDataTagRepository metaDataTagRepository;

	@Autowired
	MetaColurRepository metaColurRepository;

	@Autowired
	MetaDataTagConversion metaDataTagConversion;

	@Autowired
	DocumentMetadataTagInfoRepository documentMetadataTagInfoRepository;

	@Autowired
	MetadataTagValueRepository metaDataTagValueRepository;

	public static final String radio = "IS radio";
	public static final String select = "IS select";
	public static final String freetext = "IS freeText";

	@Override
	public boolean addMetaDataInfo(MetaDataTagDTO metatag) {

		if (metatag.getTypeVal().equalsIgnoreCase("Radio")) {
			metatag.setRadio(true);
		}
		if (metatag.getTypeVal().equalsIgnoreCase("Dropdown")) {
			metatag.setSelect(true);
		}
		if (metatag.getTypeVal().equalsIgnoreCase("Freetext")) {
			metatag.setFree_text(true);
		}
		MetadataTag metadataTag = metaDataTagConversion.metaDtoToModel(metatag);
		MetadataTag meta = null;
		meta = metaDataTagRepository.save(metadataTag);
		if (metadataTag != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<MetaDataTagDTO> getAllMetaDataTagList() {
		List<MetaDataTagDTO> metaDatatagDto = new ArrayList<MetaDataTagDTO>();
		List<MetadataTag> metadataTag = metaDataTagRepository.findAll();
		for (MetadataTag metatag : metadataTag) {
			MetaDataTagDTO metadto = metaDataTagConversion.metaModelToDto(metatag);
			metaDatatagDto.add(metadto);
		}

		return metaDatatagDto;
	}

	@Override

	public boolean addMetaDatavalue(MetaDataTagDTO metatag) {
		MetadataTagValue metadataTag = metaDataTagConversion.metaTagvalueDtoToModel(metatag);
		MetadataTagValue metavalue = null;
		MetaColour metacolour = null;
		metavalue = metaDataTagValueRepository.save(metadataTag);

		if (metatag.getColorValue() != null) {
			metatag.setValueid(metavalue.getId());
			MetaColour colouredit = null;

			MetaColour colour = metaDataTagConversion.metaColourDtoToModel(metatag);
			metacolour = metaColurRepository.save(colour);
		}
		if (metavalue != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<MetaDataTagDTO> getAllMetaDataTagValueList(Long id) {
		List<MetaDataTagDTO> metaDatatagDto = new ArrayList<MetaDataTagDTO>();
		MetadataTag metadataTagvalue = new MetadataTag();
		metadataTagvalue = metaDataTagRepository.findOne(id);

		List<MetadataTagValue> metadataTag = metaDataTagValueRepository.findByMetadataTag(metadataTagvalue);
		for (MetadataTagValue metatag : metadataTag) {
			MetaColour colour = null;
			colour = metaColurRepository.findByMetadataTagValue(metatag);
			MetaDataTagDTO metadto = metaDataTagConversion.metaTagvaluecolurModelToDto(metatag, colour);
			metaDatatagDto.add(metadto);
		}
		return metaDatatagDto;
	}

	@Override
	public MetaDataTagDTO getMetaDataTag(Long id) {
		MetadataTag meta = null;
		meta = metaDataTagRepository.findOne(id);
		MetaDataTagDTO metadto = metaDataTagConversion.metaModelToDto(meta);
		return metadto;
	}

	@Override
	public MetaDataTagDTO getMetaDataTagValue(Long id) {
		MetadataTagValue metavalue = null;
		MetadataTagValue meta = null;
		MetaDataTagDTO metadto = null;
		meta = metaDataTagValueRepository.findOne(id);
		MetaColour colour = null;
		colour = metaColurRepository.findByMetadataTagValue(meta);

		if (meta != null) {

			metadto = metaDataTagConversion.metaTagvaluecolurModelToDto(meta, colour);

		}
		return metadto;

	}

	@Override
	public List<MetaDataTagDTO> getMetaTagValueByTagId(Long tagId) {
		MetadataTag metadataTag = new MetadataTag();
		metadataTag.setId(tagId);
		List<MetaDataTagDTO> metaDatatagDto = new ArrayList<MetaDataTagDTO>();
		List<MetadataTagValue> metadataTagValues = metaDataTagValueRepository.findByMetadataTag(metadataTag);
		for (MetadataTagValue metatag : metadataTagValues) {
			MetaDataTagDTO metadto = metaDataTagConversion.metaTagvalueModelToDto(metatag);
			metaDatatagDto.add(metadto);
		}
		return metaDatatagDto;
	}

	@Override
	public Boolean isMetaTagNameExist(String metadataValue) {
		MetadataTag meta = null;
		meta = metaDataTagRepository.findByName(metadataValue);
		if (meta != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean isMetaTagValueNameExist(String metadataTagValue) {
		MetadataTagValue meta = null;
		meta = metaDataTagValueRepository.findByMetadataTagValue(metadataTagValue);
		if (meta != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<MetaDataTagDTO> getMetaDataTagWithValue() {
		// List<MetaDatatagWithValueDto> MetaDatatagWithValueDto=new
		// ArrayList<MetaDatatagWithValueDto>();
		List<MetaDataTagDTO> metaDatatagDto = new ArrayList<MetaDataTagDTO>();

		List<MetadataTag> metadataTag = metaDataTagRepository.findAll();
		for (int i = 0; i < metadataTag.size(); i++) {
			List<MetadataTagValue> metadataTagValueList = metaDataTagValueRepository
					.findByMetadataTag(metadataTag.get(i));

			List<MetaDataTagValueDTO> metaDatatagValueDtoList = new ArrayList<MetaDataTagValueDTO>();
			for (MetadataTagValue metatag : metadataTagValueList) {
				MetaDataTagValueDTO metadto = metaDataTagConversion.MetaTagvalueModelToMetaDatatagValueDto(metatag);
				metaDatatagValueDtoList.add(metadto);
			}

			MetaDataTagDTO metadto = metaDataTagConversion.metaModelToDto(metadataTag.get(i));
			metadto.setMetaDatatagValueDto(metaDatatagValueDtoList);
			metaDatatagDto.add(metadto);
		}

		return metaDatatagDto;
	}

	@Override
	public boolean deletemetatagname(Long id) {
		MetadataTag metadataTag = new MetadataTag();
		metadataTag.setId(id);
		List<MetaDataTagDTO> metaDatatagDto = new ArrayList<MetaDataTagDTO>();
		List<MetadataTagValue> metadataTagValues = metaDataTagValueRepository.findByMetadataTag(metadataTag);

		List<MetaColour> metaColourlist = metaColurRepository.findByMetadataTagColur(metadataTag);
		for (MetaColour metaColourval : metaColourlist) {
			metaColurRepository.delete(metaColourval);
		}

		for (MetadataTagValue metatagvalue : metadataTagValues) {
			List<DocumentMetadataTagInfo> documentMetadataTagInfolist = documentMetadataTagInfoRepository
					.findByMetadataTagValue(metatagvalue);
			for (DocumentMetadataTagInfo documentinfo : documentMetadataTagInfolist) {
				documentMetadataTagInfoRepository.delete(documentinfo);
			}
			metaDataTagValueRepository.delete(metatagvalue.getId());
		}

		List<DocumentMetadataTagInfo> documentMetadataTagInfolist = documentMetadataTagInfoRepository
				.findByMetadataTag(metadataTag);
		for (DocumentMetadataTagInfo documentinfo : documentMetadataTagInfolist) {
			documentMetadataTagInfoRepository.delete(documentinfo);
		}

		metaDataTagRepository.delete(id);
		return true;
	}

	@Override
	public boolean deletemetatagvalue(Long id) {
		MetadataTagValue value = metaDataTagValueRepository.findOne(id);
		MetaColour metaColour = metaColurRepository.findByMetadataTagValue(value);
		metaColurRepository.delete(metaColour);
		metaDataTagValueRepository.delete(id);
		return true;
	}

	public String addSuperAdminMetaDataTag(String listMetaDataTagAndValuesInJson) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObj = new JSONObject(listMetaDataTagAndValuesInJson.toString());
			JSONArray jsonArrayMetaDataTag = jsonObj.getJSONArray("metalist");

			for (int i = 0; i < jsonArrayMetaDataTag.length(); i++) {
				MetaDataTagDTO metaDataTagDTO = new MetaDataTagDTO();
				MetadataTag metadataTagExist = null;
				MetadataTag savedMetaData = null;
				JSONObject objects = jsonArrayMetaDataTag.getJSONObject(i);
				JSONArray jsonArrayMetaDataTagValues = objects.getJSONArray("listmetadataTagValueDtoObj");

				metaDataTagDTO.setName(objects.getString("name"));
				metaDataTagDTO.setIsvisible(objects.getBoolean("isvisible"));
				metaDataTagDTO.setIsmandatory(objects.getBoolean("ismandatory"));
				if (objects.getString("isRadio").equalsIgnoreCase("true")) {
					metaDataTagDTO.setTypeVal("Radio");
				}
				if (objects.getString("isSelect").equalsIgnoreCase("true")) {
					metaDataTagDTO.setTypeVal("Dropdown");
				}
				if (objects.getString("isFree_text").equalsIgnoreCase("true")) {
					metaDataTagDTO.setTypeVal("Freetext");
				}

				metadataTagExist = metaDataTagRepository.findByName(metaDataTagDTO.getName());
				if (metadataTagExist != null) {
					continue;
				}
				savedMetaData = addMetaDataOfSuperAdmin(metaDataTagDTO);
				if (savedMetaData == null) {
					return "failed";
				}

				for (int a = 0; a < jsonArrayMetaDataTagValues.length(); a++) {
					
					Boolean savedMetaDataTagValues = false;
					MetadataTagValue metadataTagValueExist = null;

					JSONObject tagValuesObjects = jsonArrayMetaDataTagValues.getJSONObject(a);

					metaDataTagDTO.setMetaDataTagValue(tagValuesObjects.getString("metadataTagValue"));
					metaDataTagDTO.setMetaDataTagValueId(savedMetaData.getId());
					metaDataTagDTO.setIseligibleOcr(objects.getBoolean("iseligibleOcr"));

					metadataTagValueExist = metaDataTagValueRepository
							.findByMetadataTagValue(metaDataTagDTO.getMetaDataTagValue());
					if (metadataTagValueExist != null) {
						continue;
					}
					savedMetaDataTagValues = addMetaDatavalue(metaDataTagDTO);
					if (!savedMetaDataTagValues) {
						return "failed";
					}
				}
			}
			return "success";

		} catch (JSONException e) {
			return "Failure";
		}
	}

	private MetadataTag addMetaDataOfSuperAdmin(MetaDataTagDTO metaDataTagDTO) {
		// TODO Auto-generated method stub

		if (metaDataTagDTO.getTypeVal().equalsIgnoreCase("Radio")) {
			metaDataTagDTO.setRadio(true);
		}
		if (metaDataTagDTO.getTypeVal().equalsIgnoreCase("Dropdown")) {
			metaDataTagDTO.setSelect(true);
		}
		if (metaDataTagDTO.getTypeVal().equalsIgnoreCase("Freetext")) {
			metaDataTagDTO.setFree_text(true);
		}
		MetadataTag metadataTag = metaDataTagConversion.metaDtoToModel(metaDataTagDTO);
		MetadataTag meta = null;
		meta = metaDataTagRepository.save(metadataTag);
		if (metadataTag != null) {
			return meta;
		}
		return meta;
	}

}
