package com.digidoc.admin.serviceImpl;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.service.KeywordsService;
import com.digidoc.admin.utill.DtoToModelConversion;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.repository.AliasRepository;
import com.digidoc.dataaccess.repository.KeyWordsRepository;

@Service
public class KeywordsServiceImpl implements KeywordsService {

	@Autowired
	KeyWordsRepository keyWordsRepository;

	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Autowired
	AliasServiceImpl aliasServiceImpl;

	@Autowired
	AliasRepository aliasRepository;

	/*
	 * @Autowired DtoToModelConversion dtoToModelConversion;
	 */

	public List<KeywordDto> geAllKeyWords() {
		List<Keyword> keywordslist = keyWordsRepository.findAll();
		List<KeywordDto> keywordsDtoList = modelToDtoConversion.keywordsToKeywordsDto(keywordslist);
		return keywordsDtoList;

	}

	@Override
	public Boolean update(KeywordDto keywordsDto) {

		DtoToModelConversion dtoToModelConversion = new DtoToModelConversion();

		Keyword keyword = dtoToModelConversion.keyWordrDtoToModel(keywordsDto);

		keyWordsRepository.save(keyword);
		return true;
	}

	@Override
	public Boolean delete(KeywordDto keywordDto) {
		DtoToModelConversion dtoToModelConversion = new DtoToModelConversion();

		Keyword keyword = keyWordsRepository.findOne(keywordDto.getId());
		if (keyword != null) {
			keyWordsRepository.delete(keyword.getId().longValue());
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isKeyWordIdExist(KeywordDto keywordDto) {
		// DtoToModelConversion dtoToModelConversion = new
		// DtoToModelConversion();
		// Keyword keyword =
		// dtoToModelConversion.keyWordrDtoToModel(keywordDto);
		Keyword keyword = keyWordsRepository.findOne(keywordDto.getId());
		if (keyword != null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public KeywordDto adddKeyWord(KeywordDto keywordDto) {
		DtoToModelConversion dtoToModelConversion = new DtoToModelConversion();
		KeywordDto KeyWordDto = new KeywordDto();

		Keyword keyWord = null;
		Keyword keyWordExist = null;
		Keyword keyword = dtoToModelConversion.keyWordrDtoToModel(keywordDto);
		keyWordExist = keyWordsRepository.findByKeyword(keyword.getKeyword());

		if (keyWordExist == null) {
			if (!(keyword.getKeyword().isEmpty())) {
				keyWord = keyWordsRepository.save(keyword);
				KeyWordDto = modelToDtoConversion.keywordModelToKeyWordDto(keyWord);
				return KeyWordDto;
			} else {
				return KeyWordDto;
			}
		} else {
			return KeyWordDto;
		}

	}

	public String addSuperAdminKeywords(String listkeywordInJson) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsonObj = new JSONObject(listkeywordInJson.toString());
			JSONArray jsonArrayKeyword = jsonObj.getJSONArray("listKeywords");

			for (int i = 0; i < jsonArrayKeyword.length(); i++) {
				KeywordDto keywordDto = new KeywordDto();
				Keyword keyword = new Keyword();
				Keyword keyWordExist = null;
				JSONObject objects = jsonArrayKeyword.getJSONObject(i);
				JSONArray jsonArrayAlias = objects.getJSONArray("listAliasDtos");

				keywordDto.setKeyword(objects.getString("keyword"));
				keyword.setKeyword(keywordDto.getKeyword());
				keyWordExist = keyWordsRepository.findByKeyword(keyword.getKeyword());
				if (keyWordExist != null) {
					//return "KeywordExist";
					continue;
				}
				keywordDto = adddKeyWord(keywordDto);

				for (int a = 0; a < jsonArrayAlias.length(); a++) {
					AliasDto aliasDto = new AliasDto();
					Alias alias = new Alias();
					Alias aliasExist = null;

					JSONObject aliasObject = jsonArrayAlias.getJSONObject(a);

					aliasDto.setAliasName(aliasObject.getString("aliasName"));
					aliasDto.setKeywordId(keywordDto.getId());

					alias.setAliasName(aliasDto.getAliasName());
					aliasExist = aliasRepository.findByAliasName(alias.getAliasName());
					if (aliasExist != null) {
						continue;
						//return "AliasExist";
					}
					Boolean addAlias = aliasServiceImpl.addAlias(aliasDto);
				}
			}
			return "success";

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
