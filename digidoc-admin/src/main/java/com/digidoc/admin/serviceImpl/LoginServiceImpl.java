package com.digidoc.admin.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.LoginDTO;
import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.dto.UserRoleInfoDTO;
import com.digidoc.admin.service.LogInService;
import com.digidoc.admin.utill.DtoToModelConversion;
import com.digidoc.admin.utill.EncryptDecrypt;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserRolesInfo;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.repository.UserRepository;
import com.digidoc.dataaccess.repository.UserRolesInfoRepository;
import com.digidoc.dataaccess.repository.RoleRepository;

@Service
public class LoginServiceImpl implements LogInService {

	@Autowired
	UserRepository userRepository; 
	
	
	@Autowired
	RoleRepository userRoleRepository;
	
	@Autowired
	UserRolesInfoRepository userRolesinfoRepository;
	
	
	@Autowired
	ModelToDtoConversion modelToDtoConversion;
	
	@Autowired
	DtoToModelConversion dtoToModelConversions;
	
	
	
	@Autowired
	
	Environment env;

	@Override
	public LoginDTO isUsernameAndPasswordExist(LoginDTO loginDTO) {
		EncryptDecrypt encrypt=new EncryptDecrypt();
		LoginDTO loginDTOResponse=new LoginDTO() ;
		User userdetail = userRepository.findByUserName(loginDTO.getUserName().trim());
		if (userdetail != null) {
			
			try {
				if(userdetail.getUserName().equals(loginDTO.getUserName())
						&&encrypt.decrypt(userdetail.getPassword()).equals(loginDTO.getPassword())){

					loginDTOResponse.setLogin(true);
					loginDTOResponse.setId(userdetail.getId());
					loginDTOResponse.setUserName(userdetail.getUserName());
					loginDTOResponse.setPassword(userdetail.getPassword());
					List<UserRolesInfo> rolelist=null;
					rolelist=userRolesinfoRepository.findByUser(userdetail);
					List<UserRoleInfoDTO> rolelistdto=new ArrayList<UserRoleInfoDTO>();
					for(UserRolesInfo roleinfo:rolelist) {
						UserRoleInfoDTO roledto=dtoToModelConversions.userRolesToUserRolesdto(roleinfo);
						rolelistdto.add(roledto);
					}
					loginDTOResponse.setUserRolesinfo(rolelistdto);
					return loginDTOResponse;
				}
				else {
					loginDTOResponse.setLogin(false);
					return loginDTOResponse;
				}
			} catch (Exception e) {
				e.printStackTrace();
				loginDTOResponse.setLogin(false);
				return loginDTOResponse;
				
			}

		} else {
			loginDTOResponse.setLogin(false);
			return loginDTOResponse;
		}

		

	}

	@Override
	public User findEmailid(String emailId) {
		User userdetail = userRepository.findByEmail(emailId);
		return userdetail;
	}

}
