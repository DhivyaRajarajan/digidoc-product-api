package com.digidoc.admin.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.digidoc.admin.dto.UserDto;
import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.service.UserService;
import com.digidoc.admin.utill.DtoToModelConversion;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.model.User;
import com.digidoc.dataaccess.model.UserRolesInfo;
import com.digidoc.dataaccess.repository.RoleRepository;
import com.digidoc.dataaccess.repository.UserRepository;
import com.digidoc.dataaccess.repository.UserRolesInfoRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	UserRolesInfoRepository userRolesInfoRepository; 
	
	@Autowired
	ModelToDtoConversion modelToDtoConversion;
	
	@Autowired
	DtoToModelConversion dtoToModelConversions;
	
	@Autowired
	RoleRepository roleRepository;
	
	
	
	
	@Override
	public boolean saveUser(UserDto userDto) {
		// TODO Auto-generated method stub
		DtoToModelConversion dtoToModelConversion = new DtoToModelConversion();
		User user = dtoToModelConversion.userDtoToUser(userDto);
		User isSave = userRepository.save(user);
		if(isSave != null) {
			if(userDto.getId()!=null) {
				List<UserRolesInfo> deleteinfo=userRolesInfoRepository.findByUser(isSave);
				if(deleteinfo.size()!=0) {
				for(UserRolesInfo  role:deleteinfo) {
					userRolesInfoRepository.delete(role.getId());
				}
				}
			}
			for(String role:userDto.getUserRoleDto()) {
				UserRolesInfo userRole=dtoToModelConversions.userRolesDtoToUserRoles(role,isSave.getId());
				UserRolesInfo saveUserRoleInfo=userRolesInfoRepository.save(userRole);
			}
			return true;
		}else {
			return false;
		}
	}

	@Override
	public boolean isUserEmailExist(UserDto userDto) {
		
		User isExist = userRepository.findByEmail(userDto.getEmail());
		if(isExist != null) {
			return true;
		}else {
			return false;
		}
	}
	
	@Override
	public List<UserDto> geAllUser() {
		List<UserDto> userDtoList = new  ArrayList<UserDto>();
		Role userRolesIinfo = null;
		

		List<User> userList= userRepository.findAll();
		for (User user : userList)
		{ 
			List<Role> userRolesIinfoList   =new ArrayList<Role>();
			List<UserRolesInfo> userRolesInfo  =  userRolesInfoRepository.findByUser_id(user.getId());
			for (UserRolesInfo userRoleInfo : userRolesInfo )
			{
				userRolesIinfo  =  roleRepository.findById(userRoleInfo.getRole().getId());
				userRolesIinfoList.add(userRolesIinfo);
              }
			UserDto userDto = modelToDtoConversion.userToUserDto(user ,userRolesInfo, userRolesIinfoList );
			userDtoList.add(userDto);		
				
		}
		
		
		
/*		if(userList != null){
		//List<UserDto> userDtoList = modelToDtoConversion.userToUserDto(userList);
	//	return userDtoList;
		}*/
		return userDtoList;
	}

	@Override
	public UserDto editUser(Long id) {
		List<UserRolesInfo> userrolelist=null;
		UserDto	userDto;
		User user= userRepository.findById(id);
		userrolelist=userRolesInfoRepository.findByUser(user);
		if(userrolelist != null){
		userDto = modelToDtoConversion.userToUserDtoForSingleObject(user,userrolelist);
		return userDto;	
		}
		return null;
	}

	@Override
	public Boolean deleteUser(Long id) {
		List<UserRolesInfo> userrolelist=null;
		User user= userRepository.findById(id);
		userrolelist=userRolesInfoRepository.findByUser(user);
		for(UserRolesInfo roleinfo:userrolelist) {
			userRolesInfoRepository.delete(roleinfo.getId());
		}
		userRepository.delete(id);
		return true;
	}

	@Override
	public boolean isUserNameExist(UserDto userDto) {
		User isExist = userRepository.findByUserName(userDto.getUserName());
		if(isExist != null) {
			return true;
		}else {
			return false;
		}
	}

}
