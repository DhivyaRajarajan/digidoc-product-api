package com.digidoc.admin.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.service.AliasService;
import com.digidoc.admin.utill.DtoToModelConversion;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Alias;
import com.digidoc.dataaccess.model.Keyword;
import com.digidoc.dataaccess.repository.AliasRepository;
import com.digidoc.dataaccess.repository.KeyWordsRepository;

@Service
public class AliasServiceImpl implements AliasService {

	@Autowired
	AliasRepository aliasRepository;

	@Autowired
	KeyWordsRepository keyWordsRepository;

	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Override
	public Boolean addAlias(AliasDto aliasDto) {

		DtoToModelConversion dtoToModelConversion = new DtoToModelConversion();

		Keyword keyword = keyWordsRepository.findOne(aliasDto.getKeywordId());
		List<Alias> aliasList = aliasRepository.findAll();
		boolean isAliasExist = true;
		for (Alias alias: aliasList)
		{
			if (alias.getAliasName().equalsIgnoreCase(aliasDto.getAliasName().trim()))
			{
				isAliasExist = false;
			}
		}

		if (keyword != null && isAliasExist) {
			Alias alias = dtoToModelConversion.AliasDtoToModel(aliasDto, keyword);
			// if (!(alias.getAliasName().isEmpty())) {
			aliasRepository.save(alias);
			// return true;
			// }
			return true;

		} else {
			return false;

		}

	}

	@Override
	public List<AliasDto> getAllAliasForKeyWord(Long id) {

		Keyword key = new Keyword();
		key.setId(id);
		List<Alias> aliasList = aliasRepository.findByKeyword(key);

		List<AliasDto> aliaslist = modelToDtoConversion.aliasToAliasDto(aliasList);
		return aliaslist;

	}

	@Override
	public AliasDto isAliasIdExist(Long id) {

		Alias alias = aliasRepository.findById(id);
		AliasDto aliasDto = modelToDtoConversion.aliasToAliasDto(alias);

		return aliasDto;
	}

	@Override
	public Boolean delete(Long id) {

		Alias alias = aliasRepository.findById(id);
		if (alias != null) {
			aliasRepository.delete(alias.getId());
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Boolean isKeyWordHasAliases(KeywordDto keywordDto) {
		List<Alias> aliasList = aliasRepository.findBykeyword_id(keywordDto.getId());
		if (!(aliasList.isEmpty()))
		{
			aliasRepository.delete(aliasList);
			return true;
		}else
		{
			return false;
		}
		
	}

}
