package com.digidoc.admin.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.service.UserRoleService;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.repository.RoleRepository;

@Service
public class UserRoleServiceImpl implements UserRoleService{
	
	@Autowired
	RoleRepository userRoleRepository;
	
	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Override
	public List<UserRoleDto> geAllUserRoles() {
		List<Role> userRoleList= userRoleRepository.findAll();
		List<UserRoleDto> userRoleDtoList = modelToDtoConversion.userRoleToUserRoleDto(userRoleList);
		return userRoleDtoList;
	}

}
