package com.digidoc.admin.serviceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.admin.service.FolderService;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.NetworkShare;
import com.digidoc.dataaccess.repository.FolderRepository;
import com.digidoc.dataaccess.repository.NetworkRepository;

@Service
public class FolderServiceImpl implements FolderService {

	@Autowired
	FolderRepository folderRepository;
	
	@Autowired
	NetworkRepository networkRepository;
	
	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Override
	public String addRootFolder(Folder folder) {
		// TODO Auto-generated method stub

		if (folder != null) {
			folderRepository.save(folder);
		}
		return "success";
	}

	@Override
	public List<Folder> listAllRootFolder() {
		// TODO Auto-generated method stub
		List<Folder> listFolder = new ArrayList<Folder>();
		listFolder = folderRepository.findAll();
		if (listFolder.size() != 0) {
			return listFolder;
		}
		return null;

	}

	@Override
	public String isValidInput(FolderDto folderDto) {
		// TODO Auto-generated method stub

		if (folderDto.getFolder() != null && folderDto.getFolderName() != null) {

			Folder folder = folderRepository.findById(folderDto.getFolder());

			for (Folder folderData : folder.getFolders()) {
				if (folderDto.getFolderName().equalsIgnoreCase(folderData.getFolderName())) {
					return "The SubFolder Folder Name Is Alrady Exist";
				}
			}
		}
		if (folderDto.getFolder() == null && folderDto.getFolderName() != null) {

			Set<Folder> rootFolder = folderRepository.findByFolderNameAndFolder(folderDto.getFolderName(), null);
			if (rootFolder != null && rootFolder.size() > 0) {
				return "The Root Folder Name Is Alrady Exist";
			}
		}
		return "success";
	}

	@Override
	public List<Folder> findByFolder(Folder folder) {
		List<Folder> rootFolders = new ArrayList<Folder>();
		rootFolders = folderRepository.findByFolder(folder);
		if (rootFolders != null) {
			return rootFolders;
		}
		return null;
	}

	

}
