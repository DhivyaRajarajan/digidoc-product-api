package com.digidoc.admin.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.LoginDTO;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.service.LogInService;
import com.digidoc.admin.utill.EncryptDecrypt;
import com.digidoc.dataaccess.model.User;
import com.google.gson.Gson;



@RestController
public class LoginController {
	@Autowired
	Environment env;
	
	//@Autowired
	//private EmailNotificationService emailNotificationService;
	
	
	@Autowired
	LogInService logInService;
	
	/*
	 * @author:Muthuselvi date :18/08/2018 This method for User Login 
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> Login(@RequestBody LoginDTO loginDTO) {
		LoginDTO loginDTOResponse = new LoginDTO();
		try {
			LoginDTO validlogin =null;
			validlogin = logInService.isUsernameAndPasswordExist(loginDTO);
			
			if (!validlogin.isLogin()) {
				loginDTOResponse.setStatus(env.getProperty("failure"));
				loginDTOResponse.setMessage(env.getProperty("login.failed"));
				return new ResponseEntity<String>(new Gson().toJson(loginDTOResponse), HttpStatus.PARTIAL_CONTENT);

			}
			else {
				validlogin.setStatus(env.getProperty("success"));
				validlogin.setMessage(env.getProperty("login.success"));
				return new ResponseEntity<String>(new Gson().toJson(validlogin), HttpStatus.OK);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			loginDTOResponse.setStatus(env.getProperty("failure"));
			loginDTOResponse.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(loginDTOResponse), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	
//	@CrossOrigin
//	@RequestMapping(value = "/GenricIco/forgetpassword", method = RequestMethod.POST, produces = {
//			"application/json" })
//	public ResponseEntity<String> sendforgetpassword(@RequestBody LoginDTO loginDTO) {
//		
//		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
//		boolean registerd = false;
//		try {
//			User user=new  User();
//			user = logInService
//					.findEmailid(loginDTO.getEmailId());
//			if(user!=null){
//			if (user.getEmail().equalsIgnoreCase(user.getEmail())) {
//				registerd=true;
//				String emailId = user.getEmail();
//				String verificationLink = "Hi," + "<br><br>" + env.getProperty("email.forgetpassword") + "<br>"
//						+ "Password = " + StringUtils.trim(EncryptDecrypt.decrypt(user.getPassword())) + "<br>" 
//								
//								+ "</a><br><br>"  
//								+ "With Regards,<br>" + env.getProperty("GenericIco.team");
//						//boolean isEmailSent = emailNotificationService.sendEmail(user.getEmail(), "Admin Registration",
//								//verificationLink);		
//						
////				if (!isEmailSent) {
////					// Email sending failed
////					statusResponseDTO.setStatus(env.getProperty("failure"));
////					statusResponseDTO.setMessage(env.getProperty("register.emailSendFailed"));
////					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
////				}
////				if (registerd && isEmailSent) {
////					statusResponseDTO.setStatus(env.getProperty("success"));
////					statusResponseDTO.setMessage(env.getProperty("forgetpassword"));
////					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
////				}
//				else{
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(env.getProperty("emailId.notexist"));
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//				}
//				
//			}
//			else{
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(env.getProperty("emailId.notexist"));
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//				
//			}
//			}
//			else{
//				statusResponseDTO.setStatus(env.getProperty("failure"));
//				statusResponseDTO.setMessage(env.getProperty("emailId.notexist"));
//				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//				
//			}
//			
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			statusResponseDTO.setStatus(env.getProperty("failure"));
//			statusResponseDTO.setMessage(env.getProperty("serverproblem"));
//			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
//
//		}
//
//	}
}


