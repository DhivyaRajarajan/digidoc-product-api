package com.digidoc.admin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.dto.UserDto;
import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.service.UserService;
import com.digidoc.dataaccess.model.User;
import com.google.gson.Gson; 

@RestController
public class UserController {

	@Autowired
	UserService userService;
	
	@Autowired
	Environment env;
	
	// -------------------Add Users---------------------------------------------
	@CrossOrigin
	@RequestMapping(value = "/saveUser", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> savemeatataginfo(@RequestBody UserDto userDto) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			boolean checkemail=false;
			boolean checkName=false;
			if(userDto.getId()==null) {
				checkemail=userService.isUserEmailExist(userDto);
			}
				
			if(userDto.getId()==null) {
				
				checkName=userService.isUserNameExist(userDto);
			}
			
			if(checkName) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("email.exist.name"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				
			}
			if(!checkemail) {
				Boolean savemetaDta =false;
				savemetaDta = userService.saveUser(userDto);
				if (!savemetaDta) {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(env.getProperty("usersave.failed"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
				else {
					if(userDto.getId()==null) {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("usersave.success"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
					}else {
						statusResponseDTO.setStatus(env.getProperty("success"));
						statusResponseDTO.setMessage(env.getProperty("usersave.update"));
						return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
					}
				}
			}else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("email.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
			

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	
	// -------------------Retrieve All Users---------------------------------------------
	
	@CrossOrigin
	@RequestMapping(value = "/getUserList", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> listAllUsers() {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<UserDto> userDtoList= new ArrayList<UserDto>();
		
		try {
			userDtoList = userService.geAllUser();
					if (userDtoList.size()==0) {
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("listuser.failure");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
						HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setMessage("listuser.success");
				statusResponseDTO.setUserDtoList(userDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	// -------------------Retrieve Single User for edit---------------------------------------------
	
		@CrossOrigin
		@RequestMapping(value = "/getUser/{id}", method = RequestMethod.GET, produces = {
				"application/json" })
		public ResponseEntity<String> getUser(@PathVariable("id") Long id) {
			StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
			UserDto userDto= new UserDto();
			
			try {
				userDto = userService.editUser(id);
						if (userDto == null) {
					statusResponseDTO.setStatus("Failure");
					statusResponseDTO.setMessage("fetchSingleUser.failure");
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
							HttpStatus.PARTIAL_CONTENT);
				} else {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("metatag.success"));
					statusResponseDTO.setUserDto(userDto);
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);	
					
				}

			} catch (Exception e) {
				e.printStackTrace();
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("serverProblem");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}

		}
		
		// -------------------Delete Single User ---------------------------------------------
		
			@CrossOrigin
			@RequestMapping(value = "/deleteuser/{id}", method = RequestMethod.GET, produces = {
					"application/json" })
			public ResponseEntity<String> deleteUser(@PathVariable("id") Long id) {
				StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
				Boolean userdelete= false;
				
				try {
					userdelete = userService.deleteUser(id);
					if (!userdelete) {
						statusResponseDTO.setStatus("Failure");
						statusResponseDTO.setMessage("userdelete.failure");
						return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
								HttpStatus.PARTIAL_CONTENT);
					} else {
						statusResponseDTO.setStatus(env.getProperty("success"));
						statusResponseDTO.setMessage(env.getProperty("userdelete.success"));
						return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);	
						
					}

				} catch (Exception e) {
					e.printStackTrace();
					statusResponseDTO.setStatus("Failure");
					statusResponseDTO.setMessage("serverProblem");
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}

			}

}
