package com.digidoc.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.dto.UserFavoriteSearchDto;
import com.digidoc.admin.service.KeywordsService;
import com.digidoc.admin.service.UserFavoriteSearchService;
import com.google.gson.Gson;

@RestController
@CrossOrigin
public class UserFavoriteSearchController {

	@Autowired
	UserFavoriteSearchService userFavoriteSearchService;

	@Autowired
	Environment env;

	/*---------------------------------------AddUserFavorite----------------------------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/AddUserFavoriteSearchText", method = RequestMethod.POST, produces = {
			"application/json" })

	public ResponseEntity<String> AddUserFavoriteSearchText(@RequestBody UserFavoriteSearchDto userFavoriteSearchDto,
			HttpServletRequest request, HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			Boolean validAddUserFavoriteSearchText = false;
			UserFavoriteSearchDto userFavoriteDto = new UserFavoriteSearchDto();
			validAddUserFavoriteSearchText = userFavoriteSearchService.addUserFavoriteSearchText(userFavoriteSearchDto);

			if (validAddUserFavoriteSearchText) {
				/* keywordsService.adddKeyWord(keywordDto); */
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("AddUserFavoriteSearchText.add"));

				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("AddUserFavoriteSearchText.add.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	/*----------------------------- getUserFavoriteSearchTextList-------------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/getUserFavoriteSearchTextList/{userId}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> getUserFavoriteSearchTextList(@PathVariable("userId") Long userId) {
		// List<UserFavoriteSearchDto> UserFavoriteSearchTextList = null;
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			List<UserFavoriteSearchDto> UserFavoriteSearchTextList = new ArrayList<UserFavoriteSearchDto>();

			UserFavoriteSearchTextList = userFavoriteSearchService.getAllUserFavoriteSearchText(userId);

			if (UserFavoriteSearchTextList.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("UserFavoriteSearchTextList.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("UserFavoriteSearchTextList.success"));
				statusResponseDTO.setUserFavoriteSearchList(UserFavoriteSearchTextList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	/*---------------------------get document ids using labels and meta data tag values-----------------------------------*/
	@CrossOrigin
	@RequestMapping(value = "/getDocumentIdsUsingLabelsAndMetaDataTagValue/{value}", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> getDocumentIdsUsingLabelsAndMetaDataTagValue(@PathVariable("value") String value) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			List<DocumentMetaDataTagInfoDto> documentsListUsingMetaDataTagValue = new ArrayList<DocumentMetaDataTagInfoDto>();
			List documentIds = new ArrayList();
     		documentsListUsingMetaDataTagValue = userFavoriteSearchService.getDocumentsUsingMetaDataTagValue(value);
			
			List<DocumentLabelsDto> documentsListUsingLabels = new ArrayList<DocumentLabelsDto>();
			documentsListUsingLabels = userFavoriteSearchService.getDocumentsUsingLabel(value);
			
			
			
			
			
			for (DocumentMetaDataTagInfoDto DocumentsListUsingMetaDataTagValue : documentsListUsingMetaDataTagValue)
			{
				if (!documentIds.contains(DocumentsListUsingMetaDataTagValue.getDocumentId()))
				{
					documentIds.add(DocumentsListUsingMetaDataTagValue.getDocumentId());
					
				}
			}
			
			for (DocumentLabelsDto DocumentsListUsingLabels: documentsListUsingLabels)
			{
				if (!documentIds.contains(DocumentsListUsingLabels.getDocumentId()))
				{
					documentIds.add(DocumentsListUsingLabels.getDocumentId());
					
				}
			}
			
			
			
     		if (documentIds.size() != 0 ||documentsListUsingMetaDataTagValue.size()!=0|| documentsListUsingLabels.size()!=0 ) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("documentIds.success"));
				statusResponseDTO.setdocumentIdsList(documentIds);
                return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
     	} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("documentIds.failure"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
        	} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}	
}
