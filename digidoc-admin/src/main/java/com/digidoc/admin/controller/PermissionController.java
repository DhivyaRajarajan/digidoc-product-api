package com.digidoc.admin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.service.FolderService;
import com.digidoc.dataaccess.model.Permission;
import com.digidoc.dataaccess.repository.PermissionRepository;
import com.google.gson.Gson;

@RestController
public class PermissionController {
	
	@Autowired
	PermissionRepository permissionRepository;
	
	@Autowired
	FolderService folderService;
	
	@Autowired
	Environment env;

	
	@GetMapping("/getAllPermissions")
	public List<Permission> getAllPermissions() {
		
		
		List<Permission> permissionList=permissionRepository.findAll();
		return permissionList;
		//return null;
	}
	
	
	
}
