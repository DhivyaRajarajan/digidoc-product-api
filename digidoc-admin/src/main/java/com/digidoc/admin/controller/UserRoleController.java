package com.digidoc.admin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.service.UserRoleService;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.Role;
import com.digidoc.dataaccess.repository.RoleRepository;
import com.google.gson.Gson;

@RestController
public class UserRoleController {
	
	@Autowired
	UserRoleService userRoleService;
	
	@CrossOrigin
	@RequestMapping(value = "/getUserRoleList", method = RequestMethod.GET, produces = {
			"application/json" })
	public ResponseEntity<String> getUserRoleList() {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<UserRoleDto> userRoleDtoList= new ArrayList<UserRoleDto>();
		
		try {
			userRoleDtoList = userRoleService.geAllUserRoles();
			if (userRoleDtoList.size()==0) {
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("fundtransfer.failure");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
						HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setMessage("fundtransfer.success");
				statusResponseDTO.setUserRoleDtoList(userRoleDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

}
