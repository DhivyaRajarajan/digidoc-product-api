package com.digidoc.admin.controller;

import java.io.IOException;
import java.util.Arrays;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.serviceImpl.KeywordsServiceImpl;
import com.digidoc.admin.serviceImpl.MetadataTagServiceImpl;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;

@RestController
public class RestTempleteController {

	@Autowired
	Environment env;

	@Autowired
	KeywordsServiceImpl keywordsServiceImpl;
	
	@Autowired
	MetadataTagServiceImpl metadataTagServiceImpl;

	@RequestMapping(value = "/template/addAllKeywords")
	public ResponseEntity<String> getProductList()
			throws JsonParseException, JsonMappingException, IOException, JSONException {
		String addSuperAdminKey = null;
		String url = env.getProperty("addKeywordAndAliasURLOfSuperAdmin");
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String listkeywordInJson = restTemplate
				.exchange(url, HttpMethod.GET, entity, String.class).getBody();

		addSuperAdminKey = keywordsServiceImpl.addSuperAdminKeywords(listkeywordInJson);
		
		if ("success".equalsIgnoreCase(addSuperAdminKey)) {
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("KeywordAndAliasAdd.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		} else {
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	
	@RequestMapping(value = "/template/addAllMetaDataTag")
	public ResponseEntity<String> getMetaDataTagList()
			throws JsonParseException, JsonMappingException, IOException, JSONException {
		String addSuperAdminMetaDataTag = null;
		String url = env.getProperty("addMetaDataTagAndValuesURLOfSuperAdmin");
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String listMetaDataTagAndValuesInJson = restTemplate
				.exchange(url, HttpMethod.GET, entity, String.class).getBody();
		addSuperAdminMetaDataTag = metadataTagServiceImpl.addSuperAdminMetaDataTag(listMetaDataTagAndValuesInJson);
		
		if ("success".equalsIgnoreCase(addSuperAdminMetaDataTag)) {
			statusResponseDTO.setStatus(env.getProperty("success"));
			statusResponseDTO.setMessage(env.getProperty("MetaDataTagAndValuesAdd.success"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		}
		if ("failed".equalsIgnoreCase(addSuperAdminMetaDataTag)) {
			statusResponseDTO.setStatus(env.getProperty("failed"));
			statusResponseDTO.setMessage(env.getProperty("MetaDataTagAndValuesAdd.failed"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
		}
		
		else {
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

}