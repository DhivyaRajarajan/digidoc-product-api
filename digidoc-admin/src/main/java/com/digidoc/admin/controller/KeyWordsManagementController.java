package com.digidoc.admin.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.LoginDTO;
import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.dto.UserRoleDto;
import com.digidoc.admin.service.AliasService;
import com.digidoc.admin.service.KeywordsService;
import com.digidoc.admin.service.LogInService;
import com.digidoc.admin.service.UserRoleService;
import com.google.gson.Gson;

@RestController
@CrossOrigin
public class KeyWordsManagementController {
	@Autowired
	Environment env;

	@Autowired
	LogInService logInService;

	@Autowired
	KeywordsService keywordsService;

	@Autowired
	AliasService aliasService;

	@CrossOrigin
	@RequestMapping(value = "/getkeywords", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getKeywordsList() {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<KeywordDto> keywordsList = new ArrayList<KeywordDto>();
		try {
			keywordsList = keywordsService.geAllKeyWords();
			if (keywordsList.size() == 0) {
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("fundtransfer.failure");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setMessage("fundtransfer.success");
				statusResponseDTO.setKeywordList(keywordsList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	/*--------------------------------------deletekeyword--------------------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/deleteKeyword", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> deleteKeyWord(@RequestBody KeywordDto KeywordsDto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			Boolean valid = false;

			aliasService.isKeyWordHasAliases(KeywordsDto);

			valid = keywordsService.delete(KeywordsDto);

			if (!valid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("deleteKeyword.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("delete.success"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	/*---------------------------------  UpdateKeyword---------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/updatekeyword", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> updateKeyword(@RequestBody KeywordDto keywordDto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			
			Boolean validId = false;

			validId = keywordsService.isKeyWordIdExist(keywordDto);

			if (validId) {
				keywordsService.update(keywordDto);
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("keyword.update"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("keyword.update.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	/*-------------------------------------  addKeyWord----------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/addKeyWord", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> addKeyWord(@RequestBody KeywordDto keywordDto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			
			KeywordDto KeywordDto = new KeywordDto();
			KeywordDto = keywordsService.adddKeyWord(keywordDto);

			if (KeywordDto.getId()!=null) {
				/* keywordsService.adddKeyWord(keywordDto); */
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("keyword.add"));
				statusResponseDTO.setUserKeyword(KeywordDto);

				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("keyword.add.failed"));
				statusResponseDTO.setUserKeyword(keywordDto);

				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	/*----------------------------------------------addAlias---------------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/addAlias", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> addAlias(@RequestBody AliasDto aliasDto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			Boolean validId = false;

			validId = aliasService.addAlias(aliasDto);
			if (validId) {
				/* keywordsService.adddKeyWord(keywordDto); */
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("Alias.add"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("Alias.add.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

	/*-------------------------------------------ListAlias----------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/getAliasList/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getAliasList(@PathVariable("id") Long id) {
		List<MetaDataTagDTO> metalist = null;
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			List<AliasDto> AliasList = new ArrayList<AliasDto>();

			AliasList = aliasService.getAllAliasForKeyWord(id);

			if (AliasList.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("aliaslist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("aliaslist.success"));
				statusResponseDTO.setAliasList(AliasList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	/*----------------------------EditAlias----------------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/editAlias/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> editAlias(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			AliasDto AliasDto = new AliasDto();

			AliasDto = aliasService.isAliasIdExist(id);

			if (AliasDto.getId() != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("alias.edit"));
				statusResponseDTO.setAliasDto(AliasDto);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("alias.edit.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	/*------------------------------------------deleteAlias-----------------------------------------*/

	@CrossOrigin
	@RequestMapping(value = "/deleteAlias/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> deleteAlias(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			Boolean valid = false;
			valid = aliasService.delete(id);

			if (!valid) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("deleteAlias.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("deleteAlias.sucess"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
/*----------------------- check whether keyword has aliases into it ------------------------------------------*/
	
	
	@CrossOrigin
	@RequestMapping(value = "/keywordHasAliases", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> keywordHasAliases(@RequestBody KeywordDto keywordDto, HttpServletRequest request,
			HttpServletResponse response, HttpSession session) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			Boolean validId = false;

			validId = aliasService.isKeyWordHasAliases(keywordDto);

			if (validId) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("keyword.containsAliases"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("keyword.notContainsAliases"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("keyword.notContainsAliases"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	
	
}
