package com.digidoc.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.LoginDTO;
import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.MetaDataTagDTO;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.service.LogInService;
import com.digidoc.admin.service.MetadataTagService;
import com.digidoc.dataaccess.model.MetadataTag;
import com.google.gson.Gson;

@RestController
public class MetaDataTagController {

	@Autowired
	Environment env;

	@Autowired
	MetadataTagService metadataTagService;

	/*
	 * @author:Muthuselvi date :20/08/2018 This method for Add and Update Metadata
	 * Tag information
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/addmetadata", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> savemeatataginfo(@RequestBody MetaDataTagDTO metatagsave) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			Boolean savemetaData = false;
			Boolean checkexist = false;
			// validation 
			if(metatagsave.getId() == null) {
			checkexist=metadataTagService.isMetaTagNameExist(metatagsave.getName());
			}
			
			// If validation fails
			if(checkexist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatag.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			// save metadata 
			savemetaData = metadataTagService.addMetaDataInfo(metatagsave);

			if (!savemetaData) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatag.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				if (metatagsave.getId() == null) {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("metatag.success"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				} else {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("metatag.update"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

	/*
	 * @author:Muthuselvi date :20/08/2018 This method for get Metadata Tag
	 * information
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/getmetadata/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> editmeatataginfo(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			MetaDataTagDTO metaTagValue = null;

			metaTagValue = metadataTagService.getMetaDataTag(id);

			if (metaTagValue == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metatagedit.sucess"));
				statusResponseDTO.setMetalistdto(metaTagValue);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

	/*
	 * @author:Muthuselvi date :21/08/2018 This method for get all Metadata Tag
	 * information
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/getallmetadatalist", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getallmetadatataglist() {
		List<MetaDataTagDTO> metalist = null;
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			metalist = metadataTagService.getAllMetaDataTagList();

			if (metalist.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metataglist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metataglist.success"));
				statusResponseDTO.setMetalist(metalist);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

	/*
	 * @author:Muthuselvi date :21/08/2018 This method for Add Update Metadata Tag
	 * value to metadata Tag information
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/addmetadatatagvalue", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> savemeatatagvalue(@RequestBody MetaDataTagDTO metatagvalueadd) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			
			Boolean savemetaDatavalue = false;
			Boolean checkexist = false;
			
			// validation 
			if(metatagvalueadd.getId() == null) {
			checkexist=metadataTagService.isMetaTagValueNameExist(metatagvalueadd.getMetaDataTagValue());
			}
			
			// If validation fails
			if(checkexist) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatag.exist"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
			
			// save metadata tag value
			savemetaDatavalue = metadataTagService.addMetaDatavalue(metatagvalueadd);

			if (!savemetaDatavalue) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagvalue.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				if (metatagvalueadd.getId() == null) {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("metatagvalue.success"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				} else {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("metatagvalue.update"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}

	/*
	 * @author:Muthuselvi date :20/08/2018 This method for get  Metadata Tag
	 * Value information based on meta tag name
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/getmetatagvaluelist/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getmetaTagValuelist(@PathVariable("id") Long id) {
		List<MetaDataTagDTO> metalist = null;
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			metalist = metadataTagService.getAllMetaDataTagValueList(id);

			if (metalist.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.success"));
				statusResponseDTO.setMetalist(metalist);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	/*
	 * @author:Muthuselvi date :20/08/2018 This method for edit Metadata Tag Value
	 * information
	 **
	 */

	@CrossOrigin
	@RequestMapping(value = "/getmetadatatagvalue/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> editmetaTagValuedata(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			MetaDataTagDTO metaTagValue = null;

			metaTagValue = metadataTagService.getMetaDataTagValue(id);
			

			if (metaTagValue == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metatagedit.sucess"));
				statusResponseDTO.setMetalistdto(metaTagValue);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/getvaluesbytag/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getmetaTagValueByTagId(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			List<MetaDataTagDTO> metaTagValue = null;

			metaTagValue = metadataTagService.getMetaTagValueByTagId(id);

			if (metaTagValue == null) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metatagedit.sucess"));
				statusResponseDTO.setMetalist(metaTagValue);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	@CrossOrigin
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> getMetaTagWithValue() {
		List<MetaDataTagDTO> metalist = null;
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {

			metalist = metadataTagService.getMetaDataTagWithValue();

			if (metalist.size() == 0) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metataglist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metataglist.success"));
				statusResponseDTO.setMetalist(metalist);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	
	@CrossOrigin
	@RequestMapping(value = "/deletemetatagname/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> deletemetatagname(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			boolean metaTagValuedelete = false;

			metaTagValuedelete = metadataTagService.deletemetatagname(id);

			if (!metaTagValuedelete) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metatagedit.sucess"));
			
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	

	@CrossOrigin
	@RequestMapping(value = "/deletemetatagvalue/{id}", method = RequestMethod.GET, produces = { "application/json" })
	public ResponseEntity<String> deletemetatagvalue(@PathVariable("id") Long id) {
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		try {
			boolean metaTagValuedelete = false;

			metaTagValuedelete = metadataTagService.deletemetatagvalue(id);

			if (!metaTagValuedelete) {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("metatagValuelist.failed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);

			} else {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("metatagedit.sucess"));
			
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus(env.getProperty("failure"));
			statusResponseDTO.setMessage(env.getProperty("serverProblem"));
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}
	}
	
	

}
