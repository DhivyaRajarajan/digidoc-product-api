package com.digidoc.admin.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.StatusResponseDTO;
import com.digidoc.admin.service.FolderService;
import com.digidoc.admin.utill.DtoToModelConversion;
import com.digidoc.admin.utill.ModelToDtoConversion;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.repository.FolderRepository;
import com.google.gson.Gson;

@RestController
public class FolderController {

	@Autowired
	FolderService folderService;

	@Autowired
	FolderRepository folderRepository;

	@Autowired
	ModelToDtoConversion modelToDtoConversion;

	@Autowired
	DtoToModelConversion dtoToModelConversion;

	@Autowired
	Environment env;

	@CrossOrigin
	@RequestMapping(value = "/getSubFolderlist", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> getSubFolderlist(@RequestBody FolderDto folderDto) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<Folder> folderList = new ArrayList<Folder>();

		try {
			Folder folder = folderRepository.findByFolderName(folderDto.getFolderName());
			folderList = folderRepository.findByFolder(folder);
			if (folderList.size() == 0) {
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("fundtransfer.failure");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				List<FolderDto> folderDtoList = modelToDtoConversion.folderToFolderDto(folderList);
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setMessage("fundtransfer.success");
				statusResponseDTO.setFolderlist(folderDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}

	
		@CrossOrigin
	@RequestMapping(value = "/getRootFolderlist", method = RequestMethod.POST, produces = { "application/json" })
	public ResponseEntity<String> getRootFolderlist(@RequestBody FolderDto folderDto) {

		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<Folder> folderList = new ArrayList<Folder>();

		try {
			// Folder folder =
			// folderRepository.findByFolderName(folderDto.getFolderName());
			folderList = folderRepository.findByFolder(null);
			// folderList = folderRepository.findAll();
			if (folderList.size() == 0) {
				statusResponseDTO.setStatus("Failure");
				statusResponseDTO.setMessage("fundtransfer.failure");
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			} else {
				List<FolderDto> folderDtoList = modelToDtoConversion.folderToFolderDto(folderList);
				statusResponseDTO.setStatus("Success");
				statusResponseDTO.setMessage("fundtransfer.success");
				statusResponseDTO.setFolderlist(folderDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}

		} catch (Exception e) {
			e.printStackTrace();
			statusResponseDTO.setStatus("Failure");
			statusResponseDTO.setMessage("serverProblem");
			return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
		}

	}
	 

	@PostMapping("/folder/add")
	public ResponseEntity<String> addRootFolder(@RequestBody FolderDto folderDto)

	{
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		Folder folder = new Folder();
		try {
			if (folderDto.getFolderName() != null) {

				String isValid = folderService.isValidInput(folderDto);
				if (isValid.equalsIgnoreCase("success")) {
					folder = dtoToModelConversion.folderDtoToFolderConversion(folderDto);
					folderService.addRootFolder(folder);
					if (folderService != null) {
						statusResponseDTO.setStatus(env.getProperty("success"));
						statusResponseDTO.setMessage(env.getProperty("rootfolder.add"));
						return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);

					} else {
						statusResponseDTO.setStatus(env.getProperty("failure"));
						statusResponseDTO.setMessage(env.getProperty("rootfolder.failed"));
						return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO),
								HttpStatus.PARTIAL_CONTENT);
					}
				} else {
					statusResponseDTO.setStatus(env.getProperty("failure"));
					statusResponseDTO.setMessage(isValid);
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		statusResponseDTO.setStatus(env.getProperty("failure"));
		statusResponseDTO.setMessage(env.getProperty("rootfolder.failed"));
		return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
	}

	@PostMapping("/folder/update")
	public ResponseEntity<String> updateRootFolder(@RequestBody FolderDto folderDto)

	{
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		Folder folder = new Folder();
		try {
			if (folderDto.getId() != null) {
				folder = dtoToModelConversion.folderDtoToFolderConversion(folderDto);
				folderService.addRootFolder(folder);
				if (folderService != null) {
					statusResponseDTO.setStatus(env.getProperty("success"));
					statusResponseDTO.setMessage(env.getProperty("rootfolder.update"));
					return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
				}
			} else {
				statusResponseDTO.setStatus(env.getProperty("failure"));
				statusResponseDTO.setMessage(env.getProperty("rootfolder.updatefailed"));
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		statusResponseDTO.setStatus(env.getProperty("failure"));
		statusResponseDTO.setMessage(env.getProperty("rootfolder.updatefailed"));
		return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
	}

	@GetMapping("/folder/list")
	public ResponseEntity<String> listRootFolder()

	{
		StatusResponseDTO statusResponseDTO = new StatusResponseDTO();
		List<FolderDto> folderDtoList = new ArrayList<FolderDto>();
		try {
			Folder folder = null;
			List<Folder> rootFolders = null;

			rootFolders = folderService.findByFolder(folder);
			// folderList=folderService.listAllRootFolder();
			folderDtoList = modelToDtoConversion.folderToFolderDtoConversion(rootFolders);
			if (folderDtoList != null) {
				statusResponseDTO.setStatus(env.getProperty("success"));
				statusResponseDTO.setMessage(env.getProperty("rootfolderlist.success"));
				statusResponseDTO.setFolderlist(folderDtoList);
				return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		statusResponseDTO.setStatus(env.getProperty("failure"));
		statusResponseDTO.setMessage(env.getProperty("rootfolderlist.failed"));
		return new ResponseEntity<String>(new Gson().toJson(statusResponseDTO), HttpStatus.PARTIAL_CONTENT);
	}
}
