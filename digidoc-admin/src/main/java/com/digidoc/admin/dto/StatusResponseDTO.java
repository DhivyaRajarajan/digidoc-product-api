package com.digidoc.admin.dto;

import java.util.List;
import java.util.Set;

import org.springframework.core.io.ByteArrayResource;

public class StatusResponseDTO {
	
	public String status;
	public String message;
	private ByteArrayResource bar;
//	private byte[] documentContent;
	private String originalDocumentName;
	public List<FolderDto> folderlist;
	public List<UserRoleDto> userRoleDtoList;
	public List<MetaDataTagDTO> metalist;
	public MetaDataTagDTO metalistdto;
	public List<KeywordDto> userKeywordlist;
	public List<UserDto> userDtoList;
	public UserDto userDto;
	public List<DocumentDto> documentDtoList;
	public List<AliasDto> aliasList;
	public AliasDto aliasDto;
	public KeywordDto userKeyword;
	public List<UserFavoriteSearchDto> UserFavoriteSearchList;
	public List documentIdsList;
	public Set documentIdsForAvailSearchContent;
	private List<DocumentMetaDataTagInfoDto> docMetaValues;


	public Set getDocumentIdsForAvailSearchContent() {
		return documentIdsForAvailSearchContent;
	}
	public void setDocumentIdsForAvailSearchContent(Set documentIdsForAvailSearchContent) {
		this.documentIdsForAvailSearchContent = documentIdsForAvailSearchContent;
	}
	public List<DocumentDto> getDocumentDtoList() {
		return documentDtoList;
	}
	public void setDocumentDtoList(List<DocumentDto> documentDtoList) {
		this.documentDtoList = documentDtoList;
	}
	public UserDto getUserDto() {
		return userDto;
	}
	public void setUserDto(UserDto userDto) {
		this.userDto = userDto;
	}
	public List<UserDto> getUserDtoList() {
		return userDtoList;
	}
	public void setUserDtoList(List<UserDto> userDtoList) {
		this.userDtoList = userDtoList;
	}
	public MetaDataTagDTO getMetalistdto() {
		return metalistdto;
	}
	public void setMetalistdto(MetaDataTagDTO metalistdto) {
		this.metalistdto = metalistdto;
	}
	public List<UserRoleDto> getUserRoleDtoList() {
		return userRoleDtoList;
	}
	public void setUserRoleDtoList(List<UserRoleDto> userRoleDtoList) {
		this.userRoleDtoList = userRoleDtoList;
	}
	public List<MetaDataTagDTO> getMetalist() {
		return metalist;
	}
	public void setMetalist(List<MetaDataTagDTO> metalist) {
		this.metalist = metalist;
	}
	public List<FolderDto> getFolderlist() {
		return folderlist;
	}
	public void setFolderlist(List<FolderDto> folderlist) {
		this.folderlist = folderlist;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setKeywordList(List<KeywordDto> keyWordDtoList) {
		this.userKeywordlist = keyWordDtoList;
	}
	public List<AliasDto> getAliasList() {
		return aliasList;
	}

	public void setAliasList(List<AliasDto> aliasList) {
		this.aliasList = aliasList;
	}
	public AliasDto getAliasDto() {
		return aliasDto;
	}

	public void setAliasDto(AliasDto aliasDto) {
		this.aliasDto = aliasDto;
	}
	public KeywordDto getUserKeyword() {
		return userKeyword;
	}

	public void setUserKeyword(KeywordDto userKeyword) {
		this.userKeyword = userKeyword;
	}
	
	public List<UserFavoriteSearchDto> getUserFavoriteSearchList() {
		return UserFavoriteSearchList;
	}

	public void setUserFavoriteSearchList(List<UserFavoriteSearchDto> userFavoriteSearchList) {
		UserFavoriteSearchList = userFavoriteSearchList;
	}
	public List getdocumentIdsList() {
		return documentIdsList;
	}
	public void setdocumentIdsList(List documentIdsList) {
		this.documentIdsList = documentIdsList;
	}
	/*public byte[] getDocumentContent() {
		return documentContent;
	}
	public void setDocumentContent(byte[] documentContent) {
		this.documentContent = documentContent;
	}*/
	public String getOriginalDocumentName() {
		return originalDocumentName;
	}
	public void setOriginalDocumentName(String originalDocumentName) {
		this.originalDocumentName = originalDocumentName;
	}
	public ByteArrayResource getBar() {
		return bar;
	}
	public void setBar(ByteArrayResource bar) {
		this.bar = bar;
	}
	public List<DocumentMetaDataTagInfoDto> getDocMetaValues() {
		return docMetaValues;
	}
	public void setDocMetaValues(List<DocumentMetaDataTagInfoDto> docMetaValues) {
		this.docMetaValues = docMetaValues;
	}
	

}
