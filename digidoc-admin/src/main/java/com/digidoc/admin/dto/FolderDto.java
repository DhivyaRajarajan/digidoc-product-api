package com.digidoc.admin.dto;

import java.util.List;

public class FolderDto {
	private Long id;

	private String folderName;
	
	private Long folder;
	
	private List<FolderDto> subFolders;

	public Long getId() {
		return id;
	}

	public List<FolderDto> getSubFolders() {
		return subFolders;
	}

	public void setSubFolders(List<FolderDto> subFolders) {
		this.subFolders = subFolders;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public Long getFolder() {
		return folder;
	}

	public void setFolder(Long folder) {
		this.folder = folder;
	}

}
