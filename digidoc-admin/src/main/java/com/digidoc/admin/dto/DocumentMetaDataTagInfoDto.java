package com.digidoc.admin.dto;

public class DocumentMetaDataTagInfoDto {
	
	Long id;
	Long documentId;
	Long metaDataTagId;
	Long metaDataTagValueId;
	String value;
	String docLableValues;
	public String getDocLableValues() {
		return docLableValues;
	}
	public void setDocLableValues(String docLableValues) {
		this.docLableValues = docLableValues;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public Long getMetaDataTagId() {
		return metaDataTagId;
	}
	public void setMetaDataTagId(Long metaDataTagId) {
		this.metaDataTagId = metaDataTagId;
	}
	public Long getMetaDataTagValueId() {
		return metaDataTagValueId;
	}
	public void setMetaDataTagValueId(Long metaDataTagValueId) {
		this.metaDataTagValueId = metaDataTagValueId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
	
	
	
	

}
