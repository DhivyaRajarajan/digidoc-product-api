package com.digidoc.admin.dto;

import java.util.List;

public class LoginDTO {
	
	private Long id;

	private String userName;
	
	private String emailId;
	
	private String password;
	
	public String status;
	
	public String message;
	
	public String role;
	
	public boolean login;
	
	
	public List<UserRoleInfoDTO> UserRolesinfo;
	
	
	public List<UserRoleDto> UserRoles;
	
	
	
	

	public List<UserRoleInfoDTO> getUserRolesinfo() {
		return UserRolesinfo;
	}

	public void setUserRolesinfo(List<UserRoleInfoDTO> userRolesinfo) {
		UserRolesinfo = userRolesinfo;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isLogin() {
		return login;
	}

	public void setLogin(boolean login) {
		this.login = login;
	}

	public List<UserRoleDto> getUserRoles() {
		return UserRoles;
	}

	public void setUserRoles(List<UserRoleDto> userRoles) {
		UserRoles = userRoles;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
