package com.digidoc.admin.dto;

public class MetaDataTagValueDTO {
	
	
	private Long id;
	private boolean iseligibleOcr;
	private String metadataTagValue;
	private Long metadata_tagvalue_id;
	
	public Long getId() {
		return id;
	}
	public boolean isIseligibleOcr() {
		return iseligibleOcr;
	}
	public String getMetadataTagValue() {
		return metadataTagValue;
	}
	public Long getMetadata_tagvalue_id() {
		return metadata_tagvalue_id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setIseligibleOcr(boolean iseligibleOcr) {
		this.iseligibleOcr = iseligibleOcr;
	}
	public void setMetadataTagValue(String metadataTagValue) {
		this.metadataTagValue = metadataTagValue;
	}
	public void setMetadata_tagvalue_id(Long metadata_tagvalue_id) {
		this.metadata_tagvalue_id = metadata_tagvalue_id;
	}
	
	

}
