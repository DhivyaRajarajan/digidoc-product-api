package com.digidoc.admin.dto;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class DocumentDto {

	private Long id;
	private Long userId;
	private Long folderId;
	private Long documentUniqueId;
	private String labelName;
	private Long documentPath;
	private String documentName;
	private String originalDocumentName;
	private String documentType;
	private  byte[]  documentContent;
	private Long pageCount;
	private List<MetaDataTagDTO> tagArray;
	private Long parentdocid;
	private String searchContent;
	private Boolean isEligibleOcr;
	private Long versId;
	private String documentColur;
	
	
	
	
	public String getDocumentColur() {
		return documentColur;
	}
	public void setDocumentColur(String documentColur) {
		this.documentColur = documentColur;
	}
	public String getSearchContent() {
		return searchContent;
	}
	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}
	
	public String getOriginalDocumentName() {
		return originalDocumentName;
	}
	public void setOriginalDocumentName(String originalDocumentName) {
		this.originalDocumentName = originalDocumentName;
	}
	public Long getParentdocid() {
		return parentdocid;
	}
	public void setParentdocid(Long parentdocid) {
		this.parentdocid = parentdocid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getFolderId() {
		return folderId;
	}
	public void setFolderId(Long folderId) {
		this.folderId = folderId;
	}
	public Long getDocumentUniqueId() {
		return documentUniqueId;
	}
	public void setDocumentUniqueId(Long documentUniqueId) {
		this.documentUniqueId = documentUniqueId;
	}
	public String getLabelName() {
		return labelName;
	}
	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}
	public Long getDocumentPath() {
		return documentPath;
	}
	public void setDocumentPath(Long documentPath) {
		this.documentPath = documentPath;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public byte[] getDocumentContent() {
		return documentContent;
	}
	public void setDocumentContent(byte[] documentContent) {
		this.documentContent = documentContent;
	}
	public Long getPageCount() {
		return pageCount;
	}
	public void setPageCount(Long pageCount) {
		this.pageCount = pageCount;
	}
	public List<MetaDataTagDTO> getTagArray() {
		return tagArray;
	}
	public void setTagArray(List<MetaDataTagDTO> tagArray) {
		this.tagArray = tagArray;
	}
	public Boolean getIsEligibleOcr() {
		return isEligibleOcr;
	}
	public void setIsEligibleOcr(Boolean isEligibleOcr) {
		this.isEligibleOcr = isEligibleOcr;
	}
	public Long getVersId() {
		return versId;
	}
	public void setVersId(Long versId) {
		this.versId = versId;
	}
}
