package com.digidoc.admin.dto;

public class NetworkDto {
	
	private Long id;
	private String networkPath;
	private String userName;
	private String password;
	private String pollTime;
	private Long folderid;
	private Long userId;
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNetworkPath() {
		return networkPath;
	}
	public void setNetworkPath(String networkPath) {
		this.networkPath = networkPath;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPollTime() {
		return pollTime;
	}
	public void setPollTime(String pollTime) {
		this.pollTime = pollTime;
	}
	public Long getFolderid() {
		return folderid;
	}
	public void setFolderid(Long folderid) {
		this.folderid = folderid;
	}
	
	
	
}
