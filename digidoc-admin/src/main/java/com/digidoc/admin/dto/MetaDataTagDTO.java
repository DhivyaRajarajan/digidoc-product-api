package com.digidoc.admin.dto;

import java.util.List;

public class MetaDataTagDTO {
	
	private Long id;
	private String name;
	private String typeVal;
	private boolean isRadio;
	private boolean isSelect;
	private boolean isFree_text;
	private boolean ismandatory;
	private boolean isvisible;
	private boolean iseligibleOcr;
	private String metaDataTagValue;
	private Long metaDataTagValueId;
	private List<MetaDataTagValueDTO> MetaDatatagValueDto;
	private boolean colorEnable;
	private String colorValue;
	private Long colorId;
	private Long valueid;
	
	


	
	
	
	public Long getValueid() {
		return valueid;
	}
	public void setValueid(Long valueid) {
		this.valueid = valueid;
	}
	public String getColorValue() {
		return colorValue;
	}
	public void setColorValue(String colorValue) {
		this.colorValue = colorValue;
	}
	public Long getColorId() {
		return colorId;
	}
	public void setColorId(Long colorId) {
		this.colorId = colorId;
	}
	public boolean isColorEnable() {
		return colorEnable;
	}
	public void setColorEnable(boolean colorEnable) {
		this.colorEnable = colorEnable;
	}
	public boolean isRadio() {
		return isRadio;
	}
	public void setRadio(boolean isRadio) {
		this.isRadio = isRadio;
	}
	public boolean isSelect() {
		return isSelect;
	}
	public void setSelect(boolean isSelect) {
		this.isSelect = isSelect;
	}
	public boolean isFree_text() {
		return isFree_text;
	}
	public void setFree_text(boolean isFree_text) {
		this.isFree_text = isFree_text;
	}
	public boolean isIsmandatory() {
		return ismandatory;
	}
	public void setIsmandatory(boolean ismandatory) {
		this.ismandatory = ismandatory;
	}
	public boolean isIsvisible() {
		return isvisible;
	}
	public void setIsvisible(boolean isvisible) {
		this.isvisible = isvisible;
	}
	
	public boolean isIseligibleOcr() {
		return iseligibleOcr;
	}
	public void setIseligibleOcr(boolean iseligibleOcr) {
		this.iseligibleOcr = iseligibleOcr;
	}
	public String getMetaDataTagValue() {
		return metaDataTagValue;
	}
	public void setMetaDataTagValue(String metaDataTagValue) {
		this.metaDataTagValue = metaDataTagValue;
	}
	public Long getMetaDataTagValueId() {
		return metaDataTagValueId;
	}
	public void setMetaDataTagValueId(Long metaDataTagValueId) {
		this.metaDataTagValueId = metaDataTagValueId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	public String getTypeVal() {
		return typeVal;
	}
	public void setTypeVal(String typeVal) {
		this.typeVal = typeVal;
	}
	
	public List<MetaDataTagValueDTO> getMetaDatatagValueDto() {
		return MetaDatatagValueDto;
	}
	public void setMetaDatatagValueDto(List<MetaDataTagValueDTO> metaDatatagValueDto) {
		MetaDatatagValueDto = metaDatatagValueDto;
	}
	
	
	
	
	
	
	

}
