package com.digidoc.admin.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.digidoc.dataaccess.model.Document;
import com.digidoc.dataaccess.model.Role;

public class UserDto {

	
	private Long id;
	private String email;
	private String mobileNo;
	private String password;
	private String userName;
	private String firstName;
	private String lastName;
	private List<String> userRoleDto;
//	private UserRole userRole;
	private List<UserRoleInfoDTO> userRoleinfoDto;
	private Long documentsId;
	private Long userRoleId;
	private String  userRole;
	
	
	
	

	


	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public List<UserRoleInfoDTO> getUserRoleinfoDto() {
		return userRoleinfoDto;
	}

	public void setUserRoleinfoDto(List<UserRoleInfoDTO> userRoleinfoDto) {
		this.userRoleinfoDto = userRoleinfoDto;
	}

	public List<String> getUserRoleDto() {
		return userRoleDto;
	}

	public void setUserRoleDto(List<String> userRoleDto) {
		this.userRoleDto = userRoleDto;
	}

	public Long getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	public Long getDocumentsId() {
		return documentsId;
	}

	public void setDocumentsId(Long documentsId) {
		this.documentsId = documentsId;
	}
	
	
}
