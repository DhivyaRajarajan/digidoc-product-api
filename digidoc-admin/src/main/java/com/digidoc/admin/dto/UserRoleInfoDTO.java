package com.digidoc.admin.dto;



public class UserRoleInfoDTO {

	
	
	private Long id;

	
	private String userid;

	
	private String roleid;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public String getRoleid() {
		return roleid;
	}


	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}
	
	
	

}
