package com.digidoc.admin.dto;

public class KeywordDto {

	private Long id;

	private String keyword;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keywords) {
		this.keyword = keywords;
	}

}
