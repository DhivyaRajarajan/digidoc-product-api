package com.digidoc.admin.dto;

import java.util.List;

import com.digidoc.dataaccess.model.RolePermissionInfo;
import com.digidoc.dataaccess.model.User;

public class UserRoleDto {
	
	private Long id;
	private String roleName;
//	private List<RolePermissionInfo> rolePermissionInfos;
//	private List<User> users;
	
	private Long rolePermissionInfoId;
	private Long userId;
	public Long getId() {
		return id;
	}
	public String getRoleName() {
		return roleName;
	}
	public Long getRolePermissionInfoId() {
		return rolePermissionInfoId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public void setRolePermissionInfoId(Long rolePermissionInfoId) {
		this.rolePermissionInfoId = rolePermissionInfoId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
}
