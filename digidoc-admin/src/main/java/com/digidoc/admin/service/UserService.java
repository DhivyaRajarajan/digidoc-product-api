package com.digidoc.admin.service;

import java.util.List;

import com.digidoc.admin.dto.UserDto;

public interface UserService {
	boolean isUserEmailExist(UserDto userDto);
	boolean isUserNameExist(UserDto userDto);
	boolean saveUser(UserDto userDto);
	List<UserDto> geAllUser(); 
	UserDto editUser(Long id);
	Boolean deleteUser(Long id);
}
