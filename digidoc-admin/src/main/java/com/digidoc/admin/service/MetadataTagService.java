
package com.digidoc.admin.service;


import java.util.List;

import com.digidoc.admin.dto.MetaDataTagDTO;

public interface MetadataTagService {
	
	boolean addMetaDataInfo(MetaDataTagDTO metatag);
	List<MetaDataTagDTO> getAllMetaDataTagList();
	boolean addMetaDatavalue(MetaDataTagDTO metatag);
	MetaDataTagDTO getMetaDataTag(Long id);
	List<MetaDataTagDTO> getMetaTagValueByTagId(Long tagId);
	List<MetaDataTagDTO> getAllMetaDataTagValueList(Long id);
	MetaDataTagDTO getMetaDataTagValue(Long id);
	Boolean isMetaTagNameExist(String metadataValue);
	Boolean isMetaTagValueNameExist(String metadataTagValue);
	List<MetaDataTagDTO> getMetaDataTagWithValue();
	boolean deletemetatagname(Long id);
	boolean deletemetatagvalue(Long id);

}
