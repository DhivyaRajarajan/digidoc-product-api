package com.digidoc.admin.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.LoginDTO;

public interface KeywordsService {

	boolean isKeyWordIdExist(KeywordDto keywordDto);

	List<KeywordDto> geAllKeyWords();

	Boolean update(KeywordDto keywordsDto);

	Boolean delete(KeywordDto keywordsDto);

	KeywordDto adddKeyWord(KeywordDto keywordDto);

}
