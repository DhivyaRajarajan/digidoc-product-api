package com.digidoc.admin.service;

import java.util.List;

import com.digidoc.admin.dto.FolderDto;
import com.digidoc.admin.dto.NetworkDto;
import com.digidoc.dataaccess.model.Folder;
import com.digidoc.dataaccess.model.NetworkShare;

public interface FolderService {

	String addRootFolder(Folder folder);

	List<Folder> listAllRootFolder();

	public String isValidInput(FolderDto folderDto);
	
	
	

	public List<Folder> findByFolder(Folder folder);

	
}
