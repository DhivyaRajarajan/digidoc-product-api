package com.digidoc.admin.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.DocumentLabelsDto;
import com.digidoc.admin.dto.DocumentMetaDataTagInfoDto;
import com.digidoc.admin.dto.UserFavoriteSearchDto;


public interface UserFavoriteSearchService {

	boolean addUserFavoriteSearchText(UserFavoriteSearchDto userFavoriteSearchDto);
	List<UserFavoriteSearchDto> getAllUserFavoriteSearchText(Long id);
	List<DocumentMetaDataTagInfoDto> getDocumentsUsingMetaDataTagValue(String value);
	List<DocumentLabelsDto> getDocumentsUsingLabel(String value);

	

}
