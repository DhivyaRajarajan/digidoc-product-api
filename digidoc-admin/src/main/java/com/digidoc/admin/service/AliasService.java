package com.digidoc.admin.service;

import java.util.List;

import com.digidoc.admin.dto.AliasDto;
import com.digidoc.admin.dto.KeywordDto;
import com.digidoc.admin.dto.MetaDataTagDTO;

public interface AliasService {

	Boolean addAlias(AliasDto aliasDto);

	List<AliasDto> getAllAliasForKeyWord(Long id);

	AliasDto isAliasIdExist(Long id);

	Boolean delete(Long id);

	Boolean isKeyWordHasAliases(KeywordDto keywordDto);

}
