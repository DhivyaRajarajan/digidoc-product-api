package com.digidoc.admin.service;

import javax.servlet.http.HttpServletRequest;


import com.digidoc.admin.dto.LoginDTO;
import com.digidoc.dataaccess.model.User;

public interface LogInService {

	LoginDTO isUsernameAndPasswordExist(LoginDTO loginDTO);

	User findEmailid(String emailId);

}
