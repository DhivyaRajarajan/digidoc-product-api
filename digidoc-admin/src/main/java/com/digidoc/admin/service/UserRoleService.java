package com.digidoc.admin.service;

import java.util.List;

import com.digidoc.admin.dto.UserRoleDto;

public interface UserRoleService {
	
	List<UserRoleDto> geAllUserRoles();
}
